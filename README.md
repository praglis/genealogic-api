# GeneaLogic API

GeneaLogic API is the API module of an open-source GeneaLogic application that also includes [GeneaLogic Client](https://gitlab.com/praglis/genealogic-client). GeneaLogic is a genealogical application created to aid genealogists with their work.
The application was created by Paweł Raglis within the engineer's thesis 'Family tree maker' from the Faculty of Computer Science on Białystok University of Technology and published as an open-source project on 14.02.2021.

This project is written in the Spring Boot framework and uses the PostgreSQL database. To run this project locally you need to install [PostgreSQL](https://www.postgresql.org), [Maven](https://maven.apache.org/), and JDK 11 or higher.

## Setting up a database

[Download](https://www.postgresql.org/download/) PostgreSQL installer, run it, and follow installer instructions. After successful installation, create a user and a database. To do that you can use any database administration tool, such as the pgAdmin, which is usually installed together with PostgreSQL.

## Maven

To run the REST API module, you need to [download](https://maven.apache.org/download.cgi), install, and configure Maven — Java projects management tool. Follow instructions from [the Maven project website](https://maven.apache.org/).

## Running the module

After installation and configuration of PostgreSQL and Maven, you can run the GeneaLogic API module. To achieve that, you need to open the project from this repo using IDE capable of handling Java and Maven projects (e. g. IntelliJ, Eclipse). 'Open as Maven project' (or similar) option is preferred because it needs only pointing out the _pom.xml_ file. Then the IDE will download the required dependencies, which are declared in the _pom.xml_. Before launching the GeneaLogic API module, you need to set the username, password, database name, and connection URL in the _application.properties_ file. Provide the same credentials which were used to create user and database after PostgreSQL installation. Launch GeneaLogic API as Spring Boot project in your IDE.

The REST API module should be working properly now. To obtain a fully working GeneaLogic application, you need to launch the other module — [GeneaLogic Client](https://gitlab.com/praglis/genealogic-client) at the same time.
