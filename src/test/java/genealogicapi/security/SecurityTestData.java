package genealogicapi.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityTestData {
    public static SecurityContext createSecurityContext(UserDetails customUser) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        Authentication auth =
                new UsernamePasswordAuthenticationToken(customUser, customUser.getPassword(), customUser.getAuthorities());
        context.setAuthentication(auth);
        return context;
    }
}
