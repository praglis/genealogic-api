package genealogicapi.security;

import genealogicapi.domain.user.UserDto;
import genealogicapi.domain.user.UserService;
import genealogicapi.domain.user.UserTestData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class SecurityServiceImplTest {
    @InjectMocks
    private SecurityServiceImpl securityService;
    @Mock
    private UserService userService;

    private UserDetails userDetails;
    private UserDto userDto;

    @BeforeEach
    void setUp() {
        userDetails = UserTestData.buildUserDetails();
        userDto = UserTestData.buildUserDto();
        SecurityContextHolder.setContext(SecurityTestData.createSecurityContext(userDetails));
    }

    @Test
    void FindLoggedInUsername_UserLogged_UsernameReturned() {
        assertEquals(UserTestData.testUsername, securityService.findLoggedInUsername());
    }

    @Test
    void FindLoggedInUsername_UserNotLogged_NullReturned() {
        SecurityContextHolder.clearContext();
        assertNull(securityService.findLoggedInUsername());
    }

    @Test
    void FindLoggedInUser_UserLogged_UserDtoReturned() {
        given(userService.getByUsername(UserTestData.testUsername)).willReturn(userDto);

        assertEquals(userDto, securityService.findLoggedInUser());
    }

    @Test
    void CheckIfUserIsLoggedIn_Username_ExceptionNotThrown() {
        assertDoesNotThrow(() -> securityService.checkIfUserIsLoggedIn(UserTestData.testUsername));
    }

    @Test
    void CheckIfUserIsLoggedIn_NotLoggedUsername_ExceptionThrown() {
        SecurityContextHolder.clearContext();
        assertThrows(NoOwnershipException.class, () -> securityService.checkIfUserIsLoggedIn(UserTestData.testUsername));
    }

    @Test
    void CheckIfUserIsLoggedIn_Id_DtoReturned() {
        given(userService.getById(UserTestData.testId)).willReturn(userDto);

        assertEquals(userDto, securityService.checkIfUserIsLoggedIn(UserTestData.testId));
    }

    @Test
    void CheckIfUserIsLoggedIn_WrongId_ExceptionThrown() {
        given(userService.getById(UserTestData.testId2)).willReturn(UserTestData.buildUserDto2());

        assertThrows(NoOwnershipException.class, () -> securityService.checkIfUserIsLoggedIn(UserTestData.testId2));
    }
}
