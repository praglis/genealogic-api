package genealogicapi.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@Component
public class AuthorizedTestRequestFactory {
    private final ObjectWriter objectWriter;
    private HttpHeaders headersWithToken;

    public AuthorizedTestRequestFactory(ObjectWriter objectWriter) {
        this.objectWriter = objectWriter;
    }

    public void prepareAuthorizationHeader(String jwtToken) {
        headersWithToken = new HttpHeaders();
        headersWithToken.setBearerAuth(jwtToken);
    }

    public <T> MockHttpServletRequestBuilder authorizedPost(String url, T request) throws JsonProcessingException {
        return unauthorizedPost(url, request)
                .headers(headersWithToken);
    }

    public <T> MockHttpServletRequestBuilder unauthorizedPost(String url, T request) throws JsonProcessingException {
        return MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(request));
    }

    public MockHttpServletRequestBuilder authorizedGet(String url) {
        return MockMvcRequestBuilders.get(url)
                .headers(headersWithToken);
    }

    public MockHttpServletRequestBuilder unauthorizedGet(String url) {
        return MockMvcRequestBuilders.get(url);
    }

    public <T> MockHttpServletRequestBuilder authorizedPut(String url, T request) throws JsonProcessingException {
        return unauthorizedPut(url, request)
                .headers(headersWithToken);
    }

    public <T> MockHttpServletRequestBuilder unauthorizedPut(String url, T request) throws JsonProcessingException {
        return MockMvcRequestBuilders.put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(request));
    }

    public MockHttpServletRequestBuilder authorizedDelete(String url) {
        return unauthorizedDelete(url)
                .headers(headersWithToken);
    }

    public MockHttpServletRequestBuilder unauthorizedDelete(String url) {
        return MockMvcRequestBuilders.delete(url);
    }
}
