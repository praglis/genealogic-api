package genealogicapi.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ObjectWriterConfig {
    @Bean
    public ObjectWriter objectWriter() {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writer().withDefaultPrettyPrinter();
    }
}
