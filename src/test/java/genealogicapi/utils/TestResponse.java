package genealogicapi.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@ToString
public class TestResponse {
    private final MockHttpServletResponse mockHttpServletResponse;

    public <R> R getBodyAs(Class<R> responseBodyClass) throws java.io.IOException {
        ObjectReader objectReader = new ObjectMapper().reader().forType(responseBodyClass);
        return objectReader.readValue(mockHttpServletResponse.getContentAsString());
    }

    public HttpStatus getStatus() {
        return HttpStatus.resolve(mockHttpServletResponse.getStatus());
    }


    public <R> Page<R> getBodyAsPage(Class<R> pageElementClass) throws Exception {
        TypeReference<TestPage<R>> typeReference = new TypeReference<>() {
        };

        ObjectReader reader = new ObjectMapper().readerFor(typeReference);
        TestPage<R> testPage = reader.readValue(mockHttpServletResponse.getContentAsString());

        List<R> elements = testPage.getContent().stream()
                .map(element -> new ObjectMapper().convertValue(element, pageElementClass))
                .collect(Collectors.toList());

        return new TestPage<>(elements, testPage.getNumber(), testPage.getNumberOfElements(),
                testPage.getTotalElements(), null);
    }
}
