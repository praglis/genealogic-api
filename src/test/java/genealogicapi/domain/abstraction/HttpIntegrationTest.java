package genealogicapi.domain.abstraction;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import genealogicapi.utils.AuthorizedTestRequestFactory;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

/*
 * If not specified, all methods in this class assume AUTHORIZED request by default.
 * */
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public abstract class HttpIntegrationTest<D extends AbstractDto> {
    ObjectWriter objectWriter;
    ObjectReader domainDtoReader;
    @Autowired
    AuthorizedTestRequestFactory testRequestFactory;
    @Autowired
    private MockMvc mockMvc;

    <RQ> MockHttpServletResponse performPost(@NonNull String path, RQ request) throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                        testRequestFactory.unauthorizedPost(path, request)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(request)))
                .andReturn();

        return mvcResult.getResponse();
    }

    MockHttpServletResponse performGet(@NonNull String path) throws Exception {
        return performGet(true, path);
    }

    MockHttpServletResponse performGet(@NonNull boolean isAuthorized, @NonNull String path) throws Exception {
        MvcResult mvcResult = isAuthorized ? performAuthorizedGet(path) : performUnauthorizedGet(path);
        return mvcResult.getResponse();
    }

    private MvcResult performAuthorizedGet(@NonNull String path) throws Exception {
        return mockMvc.perform(testRequestFactory.authorizedGet(path))
                .andReturn();
    }

    private MvcResult performUnauthorizedGet(@NonNull String path) throws Exception {
        return mockMvc.perform(testRequestFactory.unauthorizedGet(path))
                .andReturn();
    }

    MockHttpServletResponse performAuthorizedPut(@NonNull String path, @NonNull D request) throws Exception {
        return mockMvc.perform(testRequestFactory.authorizedPut(path, request))
                .andReturn()
                .getResponse();
    }

    MockHttpServletResponse performDelete(@NonNull boolean isAuthorized, @NonNull String path) throws Exception {
        ResultActions resultActions = isAuthorized ? performAuthorizedDelete(path) : performUnauthorizedDelete(path);
        return resultActions.andReturn().getResponse();
    }

    private ResultActions performAuthorizedDelete(@NonNull String path) throws Exception {
        return mockMvc.perform(testRequestFactory.authorizedDelete(path));
    }

    private ResultActions performUnauthorizedDelete(@NonNull String path) throws Exception {
        return mockMvc.perform(testRequestFactory.unauthorizedDelete(path));
    }
}
