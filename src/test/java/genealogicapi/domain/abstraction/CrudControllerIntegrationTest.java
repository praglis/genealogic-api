package genealogicapi.domain.abstraction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import genealogicapi.domain.user.AppUserRole;
import genealogicapi.domain.user.UserDto;
import genealogicapi.domain.user.mapping.UserMapper;
import genealogicapi.security.payload.JwtResponse;
import genealogicapi.security.payload.LoginRequest;
import genealogicapi.utils.TestResponse;
import lombok.NonNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static genealogicapi.domain.user.UserTestData.buildUserDtoWithRole;

/*
 * If not specified, all methods in this class assume AUTHORIZED request by default.
 * */
@ExtendWith(SpringExtension.class)
@Sql(value = {"/sql/truncate-all-data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
public abstract class CrudControllerIntegrationTest<D extends AbstractDto> extends HttpIntegrationTest<D> {

    protected static final String[] EXCLUDED_PROPS_FOR_ABSTRACT_DTO = {"id", "creationDate", "modificationDate"};
    private static final String AUTH_URL = "/users/auth";
    private final String domainUrl;
    private final Class<D> domainDtoClass;
    protected UserDto authUser;
    @Autowired
    private UserMapper userMapper;

    protected CrudControllerIntegrationTest(String domainUrl, Class<D> domainDtoClass) {
        this.domainUrl = domainUrl;
        this.domainDtoClass = domainDtoClass;
    }

    @BeforeAll
    public void setUpMappers() {
        ObjectMapper objectMapper = new ObjectMapper();
        this.objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        domainDtoReader = objectMapper.reader().forType(domainDtoClass);
    }

    @BeforeEach
    public void createAdminAndLogIn() throws Exception {
        UserDto userToCreate = buildUserDtoWithRole(AppUserRole.ROLE_ADMIN);
        this.authUser = createAndReturnObjectFromAbsolutePath(domainUrl, userToCreate, UserDto.class);

        TestResponse response = authenticateUser(userMapper.toLoginRequest(userToCreate));
        JwtResponse jwtResponse = response.getBodyAs(JwtResponse.class);

        testRequestFactory.prepareAuthorizationHeader(jwtResponse.getToken());
    }

    protected <RQ, RS> RS createAndReturnObjectFromAbsolutePath(@NonNull String absolutePath, RQ request,
                                                                @NonNull Class<RS> responseClass) throws Exception {
        MockHttpServletResponse response = performPost(absolutePath, request);

        ObjectReader nonDomainReader = new ObjectMapper().reader().forType(responseClass);
        return nonDomainReader.readValue(response.getContentAsString());
    }

    protected TestResponse authenticateUser(@NonNull LoginRequest loginRequest) throws Exception {
        return new TestResponse(performPost(AUTH_URL, loginRequest));
    }

    public List<D> createAndReturnMultipleResources(@NonNull List<D> entitiesToCreate) throws Exception {
        List<D> createdEntities = new ArrayList<>(entitiesToCreate.size());
        for (D entity : entitiesToCreate) {
            createdEntities.add(createAndReturnResource(entity));
        }
        return createdEntities;
    }

    protected D createAndReturnResource(D resourceToCreate) throws Exception {
        MockHttpServletResponse response = performPost(domainUrl, resourceToCreate);
        return domainDtoReader.readValue(response.getContentAsString());
    }

    public List<TestResponse> createMultipleResources(@NonNull List<D> entitiesToCreate) throws Exception {
        List<TestResponse> responses = new ArrayList<>(entitiesToCreate.size());
        for (D entity : entitiesToCreate) {
            responses.add(createResource(entity));
        }
        return responses;
    }

    protected TestResponse createResource(D requestBody) throws Exception {
        return new TestResponse(performPost(domainUrl, requestBody));

    }

    protected Page<D> getAndReturnPageOfAllResources() throws Exception {
        TestResponse response = getAllResources(true);

        return response.getBodyAsPage(domainDtoClass);
    }

    protected TestResponse getAllResources(@NonNull boolean isAuthorized) throws Exception {
        return new TestResponse(performGet(isAuthorized, domainUrl));
    }

    protected TestResponse getResourceById(@NonNull Long id) throws Exception {
        return getResourceById(true, id);
    }

    protected TestResponse getResourceById(@NonNull boolean isAuthorized, @NonNull Long id) throws Exception {
        MockHttpServletResponse response = performGet(
                isAuthorized,
                domainUrl + "/" + id
        );

        return new TestResponse(response);
    }

    protected TestResponse updateResource(@NonNull Long id, @NonNull D dto) throws Exception {
        return new TestResponse(performAuthorizedPut(domainUrl + "/" + id, dto));
    }

    protected TestResponse deleteResource(@NonNull Long id) throws Exception {
        return deleteResource(true, id);
    }

    protected TestResponse deleteResource(@NonNull boolean isAuthorized, @NonNull Long id) throws Exception {
        return new TestResponse(performDelete(isAuthorized, domainUrl + "/" + id));
    }
}
