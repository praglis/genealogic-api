package genealogicapi.domain.person;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.person.mapping.PersonMapper;
import genealogicapi.domain.place.PlaceMapper;
import genealogicapi.domain.tree.Tree;
import genealogicapi.domain.tree.TreeDto;
import genealogicapi.domain.tree.TreeMapper;
import genealogicapi.domain.tree.TreeService;
import genealogicapi.domain.user.mapping.UserMapper;
import genealogicapi.gedcom.structure.IndividualRecord;
import genealogicapi.security.SecurityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.Optional;

import static genealogicapi.domain.person.PersonTestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PersonServiceImplTest {

    @Spy
    private final AbstractMapper<PersonDto, PersonEntity> personMapper = Mappers.getMapper(PersonMapper.class);
    @Spy
    private final TreeMapper treeMapper = Mappers.getMapper(TreeMapper.class);
    @Spy
    private final PlaceMapper placeMapper = Mappers.getMapper(PlaceMapper.class);
    @Spy
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    @Mock
    protected PasswordEncoder passwordEncoder;
    @InjectMocks
    private PersonServiceImpl personService;
    @Mock
    private PersonRepository personRepository;
    @Mock
    private TreeService treeService;
    @Mock
    private SecurityService securityService;
    private PersonDto personDto1;
    private PersonDto personDto2;
    private PersonEntity person1;
    private IndividualRecord individualRecord;
    private List<PersonEntity> personList;
    private List<PersonDto> personDtoList;
    private List<IndividualRecord> individualRecordList;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(personMapper, "treeMapper", treeMapper);
        ReflectionTestUtils.setField(personMapper, "placeMapper", placeMapper);
        ReflectionTestUtils.setField(treeMapper, "userMapper", userMapper);
        ReflectionTestUtils.setField(userMapper, "passwordEncoder", passwordEncoder);

        personDto1 = buildPersonDto1();
        personDto2 = buildPersonDto2();
        person1 = buildPerson1();
        individualRecord = buildIndividualRecord1();
        personList = buildPersonList();
        personDtoList = buildPersonDtoList();
        individualRecordList = buildIndividualRecordList();
    }

    @Test
    void getById_id_DtoReturned() {
        given(personRepository.findById(personDto1.getId())).willReturn(Optional.of(person1));

        Long id = personDto1.getId();
        assertEquals(personDto1, personService.getById(id));
    }

    @Test
    void getById_wrongId_ExceptionThrown() {
        given(personRepository.findById(personDto2.getId())).willReturn(Optional.empty());

        Long id = personDto2.getId();
        assertThrows(EntityNotFoundException.class, () -> personService.getById(id));
    }

    @Test
    void getPersonsByTree_treeId_PersonsReturned() {
        given(treeService.getById(personDto1.getId())).willReturn(personDto1.getTree());
        given(personRepository.findByTreeOrderById(any(Tree.class), any(Pageable.class))).willReturn(new PageImpl<>(personList));

        Long treeId = personDto1.getTree().getId();
        Pageable pageable = PageRequest.of(0, 20);
        List<PersonDto> resultContent = personService.getPersonsByTree(treeId, pageable).getContent();
        assertEquals(personDtoList, resultContent);
    }

    @Test
    void getAllRecordsByTree_treeId_IndisReturned() {
        given(treeService.getById(personDto1.getId())).willReturn(personDto1.getTree());
        given(personRepository.findByTreeOrderById(any(Tree.class), any(Pageable.class))).willReturn(new PageImpl<>(personList));

        Long treeId = personDto1.getTree().getId();
        assertEquals(individualRecordList, personService.getAllRecordsByTree(treeId));
    }

    @Test
    void countByTree_treeDto_counted() {
        given(personRepository.countByTree(person1.getTree())).willReturn(2L);

        TreeDto treeDto = personDto1.getTree();
        assertEquals(2, personService.countByTree(treeDto));
    }

    @Test
    void CreateDto_Dto_DtoReturned() {
        given(treeService.getById(personDto1.getTree().getId())).willReturn(personDto1.getTree());
        given(personRepository.saveAndFlush(any(PersonEntity.class))).will(returnsFirstArg());
        given(treeService.updateCounts(personDto1.getTree().getId())).willReturn(personDto1.getTree());

        assertEquals(personDto1, personService.create(personDto1));
    }

    @Test
    void CreateRecord_Record_RecordReturned() {
        given(treeService.getById(personDto1.getTree().getId())).willReturn(personDto1.getTree());
        given(personRepository.saveAndFlush(any(PersonEntity.class))).will(returnsFirstArg());
        given(treeService.updateCounts(personDto1.getTree().getId())).willReturn(personDto1.getTree());

        assertEquals(individualRecord, personService.create(individualRecord));
    }

    @Test
    void update_dto_DtoReturned() {
        given(treeService.getById(personDto1.getTree().getId())).willReturn(personDto1.getTree());
        given(personRepository.findById(person1.getId())).willReturn(Optional.of(person1));
        given(personRepository.saveAndFlush(any(PersonEntity.class))).will(returnsFirstArg());
        given(treeService.updateCounts(personDto1.getTree().getId())).willReturn(personDto1.getTree());

        assertEquals(personDto1, personService.update(personDto1));
    }
}
