package genealogicapi.domain.person;

import genealogicapi.commons.file.EncodedFileDto;
import genealogicapi.domain.place.Place;
import genealogicapi.domain.place.PlaceDto;
import genealogicapi.domain.tree.TreeTestData;
import genealogicapi.gedcom.structure.IndividualRecord;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

public abstract class PersonTestData {
    public static final Long testId1 = 1L;
    public static final Long testId2 = 2L;
    public static final Long testId3 = 3L;
    public static final Long testId4 = 4L;
    private static final String testNamePrefix1 = "B.A.";
    private static final String testFirstName1 = "John";
    private static final String testMiddleName1 = "Jason";
    private static final String testNickname1 = "Sagittarius";
    private static final String testSurnamePrefix1 = "de";
    private static final String testLastName1 = "Hatfield";
    private static final String testMaidenName1 = null;
    private static final String testNameSuffix1 = "Sr.";
    private static final Sex testSex1 = Sex.M;
    private static final String testOccupation1 = "Postmaster";
    private static final String testBio1 = "Biography of 1st person";
    private static final String testNamePrefix2 = "Ph.D.";
    private static final String testFirstName2 = "Ruby";
    private static final String testMiddleName2 = "Ethel";
    private static final String testNickname2 = "Leo";
    private static final String testSurnamePrefix2 = "von";
    private static final String testLastName2 = "Smith";
    private static final String testMaidenName2 = "Oliver";
    private static final String testNameSuffix2 = "Jr.";
    private static final Sex testSex2 = Sex.F;
    private static final String testOccupation2 = "Water transportation master";
    private static final String testBio2 = "Biography of 2nd person";
    private static final String testNamePrefix3 = "B.S";
    private static final String testFirstName3 = "Floyd";
    private static final String testMiddleName3 = "Joshua";
    private static final String testNickname3 = "Equator";
    private static final String testSurnamePrefix3 = "van";
    private static final String testLastName3 = "Gilbert";
    private static final String testMaidenName3 = null;
    private static final String testNameSuffix3 = "I";
    private static final Sex testSex3 = Sex.U;
    private static final String testOccupation3 = "Farmer";
    private static final String testBio3 = "Biography of 3nd person";
    private static final String testNamePrefix4 = "M.S.";
    private static final String testFirstName4 = "Jennifer";
    private static final String testMiddleName4 = "J.";
    private static final String testNickname4 = "Libra";
    private static final String testSurnamePrefix4 = "de la";
    private static final String testLastName4 = "Ryan";
    private static final String testMaidenName4 = "Rios";
    private static final String testNameSuffix4 = "II";
    private static final Sex testSex4 = Sex.F;
    private static final String testOccupation4 = "Sales clerk";
    private static final String testBio4 = "Biography of 4nd person";
    private static final String defaultEncodedPhoto = Base64.getEncoder().encodeToString(new byte[0]);
    private static final String defaultPhotoNameSuffix = "-photo";

    public static PersonDto buildPersonDto1() {
        return PersonDto.builder()
                .id(testId1)
                .tree(TreeTestData.buildTreeDto())
                .namePrefix(testNamePrefix1)
                .firstName(testFirstName1)
                .middleName(testMiddleName1)
                .nickname(testNickname1)
                .surnamePrefix(testSurnamePrefix1)
                .lastName(testLastName1)
                .maidenName(testMaidenName1)
                .nameSuffix(testNameSuffix1)
                .sex(testSex1)
                .occupation(testOccupation1)
                .residency(new PlaceDto())
                .bio(testBio1)
                .photo(new EncodedFileDto(testId1 + defaultPhotoNameSuffix, defaultEncodedPhoto))
                .build();
    }

    public static PersonDto buildPersonDto2() {
        return PersonDto.builder()
                .id(testId2)
                .tree(TreeTestData.buildTreeDto())
                .namePrefix(testNamePrefix2)
                .firstName(testFirstName2)
                .middleName(testMiddleName2)
                .nickname(testNickname2)
                .surnamePrefix(testSurnamePrefix2)
                .lastName(testLastName2)
                .maidenName(testMaidenName2)
                .nameSuffix(testNameSuffix2)
                .sex(testSex2)
                .occupation(testOccupation2)
                .residency(new PlaceDto())
                .bio(testBio2)
                .photo(new EncodedFileDto(testId2 + defaultPhotoNameSuffix, defaultEncodedPhoto))
                .build();
    }

    public static PersonDto buildPersonDto3() {
        return PersonDto.builder()
                .id(testId3)
                .tree(TreeTestData.buildTreeDto())
                .namePrefix(testNamePrefix3)
                .firstName(testFirstName3)
                .middleName(testMiddleName3)
                .nickname(testNickname3)
                .surnamePrefix(testSurnamePrefix3)
                .lastName(testLastName3)
                .maidenName(testMaidenName3)
                .nameSuffix(testNameSuffix3)
                .sex(testSex3)
                .occupation(testOccupation3)
                .residency(new PlaceDto())
                .bio(testBio3)
                .photo(new EncodedFileDto(testId3 + defaultPhotoNameSuffix, defaultEncodedPhoto))
                .build();
    }

    public static PersonDto buildPersonDto4() {
        return PersonDto.builder()
                .id(testId4)
                .tree(TreeTestData.buildTreeDto())
                .namePrefix(testNamePrefix4)
                .firstName(testFirstName4)
                .middleName(testMiddleName4)
                .nickname(testNickname4)
                .surnamePrefix(testSurnamePrefix4)
                .lastName(testLastName4)
                .maidenName(testMaidenName4)
                .nameSuffix(testNameSuffix4)
                .sex(testSex4)
                .occupation(testOccupation4)
                .residency(new PlaceDto())
                .bio(testBio4)
                .photo(new EncodedFileDto(testId4 + defaultPhotoNameSuffix, defaultEncodedPhoto))
                .build();
    }

    public static PersonEntity buildPerson1() {
        return PersonEntity.builder()
                .id(testId1)
                .tree(TreeTestData.buildTree())
                .namePrefix(testNamePrefix1)
                .firstName(testFirstName1)
                .middleName(testMiddleName1)
                .nickname(testNickname1)
                .surnamePrefix(testSurnamePrefix1)
                .lastName(testLastName1)
                .maidenName(testMaidenName1)
                .nameSuffix(testNameSuffix1)
                .sex(testSex1)
                .occupation(testOccupation1)
                .residency(new Place())
                .bio(testBio1)
                .photo(new byte[0])
                .build();
    }

    protected static PersonEntity buildPerson2() {
        return PersonEntity.builder()
                .id(testId2)
                .tree(TreeTestData.buildTree())
                .namePrefix(testNamePrefix2)
                .firstName(testFirstName2)
                .middleName(testMiddleName2)
                .nickname(testNickname2)
                .surnamePrefix(testSurnamePrefix2)
                .lastName(testLastName2)
                .maidenName(testMaidenName2)
                .nameSuffix(testNameSuffix2)
                .sex(testSex2)
                .occupation(testOccupation2)
                .residency(new Place())
                .bio(testBio2)
                .photo(new byte[0])
                .build();
    }

    public static PersonEntity buildPerson3() {
        return PersonEntity.builder()
                .id(testId3)
                .tree(TreeTestData.buildTree())
                .namePrefix(testNamePrefix3)
                .firstName(testFirstName3)
                .middleName(testMiddleName3)
                .nickname(testNickname3)
                .surnamePrefix(testSurnamePrefix3)
                .lastName(testLastName3)
                .maidenName(testMaidenName3)
                .nameSuffix(testNameSuffix3)
                .sex(testSex3)
                .occupation(testOccupation3)
                .residency(new Place())
                .bio(testBio3)
                .photo(new byte[0])
                .build();
    }

    public static PersonEntity buildPerson4() {
        return PersonEntity.builder()
                .id(testId4)
                .tree(TreeTestData.buildTree())
                .namePrefix(testNamePrefix4)
                .firstName(testFirstName4)
                .middleName(testMiddleName4)
                .nickname(testNickname4)
                .surnamePrefix(testSurnamePrefix4)
                .lastName(testLastName4)
                .maidenName(testMaidenName4)
                .nameSuffix(testNameSuffix4)
                .sex(testSex4)
                .occupation(testOccupation4)
                .residency(new Place())
                .bio(testBio4)
                .photo(new byte[0])
                .build();
    }

    public static IndividualRecord buildIndividualRecord1() {
        return IndividualRecord.builder()
                .id(testId1)
                .tree(TreeTestData.buildTreeDto())
                .namePrefix(testNamePrefix1)
                .firstName(testFirstName1)
                .middleName(testMiddleName1)
                .nickname(testNickname1)
                .surnamePrefix(testSurnamePrefix1)
                .lastName(testLastName1)
                .maidenName(testMaidenName1)
                .nameSuffix(testNameSuffix1)
                .sex(testSex1)
                .occupation(testOccupation1)
                .residency(new PlaceDto())
                .bio(testBio1)
                .individualEvents(new ArrayList<>())
                .familyPointers(new HashMap<>())
                .build();
    }

    public static IndividualRecord buildIndividualRecord2() {
        return IndividualRecord.builder()
                .id(testId2)
                .tree(TreeTestData.buildTreeDto())
                .namePrefix(testNamePrefix2)
                .firstName(testFirstName2)
                .middleName(testMiddleName2)
                .nickname(testNickname2)
                .surnamePrefix(testSurnamePrefix2)
                .lastName(testLastName2)
                .maidenName(testMaidenName2)
                .nameSuffix(testNameSuffix2)
                .sex(testSex2)
                .occupation(testOccupation2)
                .residency(new PlaceDto())
                .bio(testBio2)
                .individualEvents(new ArrayList<>())
                .familyPointers(new HashMap<>())
                .build();
    }

    public static IndividualRecord buildIndividualRecord3() {
        return IndividualRecord.builder()
                .id(testId3)
                .tree(TreeTestData.buildTreeDto())
                .namePrefix(testNamePrefix3)
                .firstName(testFirstName3)
                .middleName(testMiddleName3)
                .nickname(testNickname3)
                .surnamePrefix(testSurnamePrefix3)
                .lastName(testLastName3)
                .maidenName(testMaidenName3)
                .nameSuffix(testNameSuffix3)
                .sex(testSex3)
                .occupation(testOccupation3)
                .residency(new PlaceDto())
                .bio(testBio3)
                .individualEvents(new ArrayList<>())
                .familyPointers(new HashMap<>())
                .build();
    }

    public static IndividualRecord buildIndividualRecord4() {
        return IndividualRecord.builder()
                .id(testId4)
                .tree(TreeTestData.buildTreeDto())
                .namePrefix(testNamePrefix4)
                .firstName(testFirstName4)
                .middleName(testMiddleName4)
                .nickname(testNickname4)
                .surnamePrefix(testSurnamePrefix4)
                .lastName(testLastName4)
                .maidenName(testMaidenName4)
                .nameSuffix(testNameSuffix4)
                .sex(testSex4)
                .occupation(testOccupation4)
                .residency(new PlaceDto())
                .bio(testBio4)
                .individualEvents(new ArrayList<>())
                .familyPointers(new HashMap<>())
                .build();
    }

    public static List<PersonDto> buildPersonDtoList() {
        return List.of(buildPersonDto1(), buildPersonDto2());
    }

    public static List<PersonEntity> buildPersonList() {
        return List.of(buildPerson1(), buildPerson2());
    }

    public static List<IndividualRecord> buildIndividualRecordList() {
        return List.of(buildIndividualRecord1(), buildIndividualRecord2());
    }

    public static PersonDto buildPersonDtoWithId(Long id) {
        switch (id.intValue()) {
            case 1:
            default:
                return buildPersonDto1();
            case 2:
                return buildPersonDto2();
            case 3:
                return buildPersonDto3();
            case 4:
                return buildPersonDto4();
        }
    }
}
