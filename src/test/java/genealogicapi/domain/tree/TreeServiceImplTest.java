package genealogicapi.domain.tree;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.family.FamilyService;
import genealogicapi.domain.person.PersonService;
import genealogicapi.domain.person.PersonTestData;
import genealogicapi.domain.user.UserEntity;
import genealogicapi.domain.user.UserService;
import genealogicapi.domain.user.UserTestData;
import genealogicapi.domain.user.mapping.UserMapper;
import genealogicapi.security.NoOwnershipException;
import genealogicapi.security.SecurityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.Optional;

import static genealogicapi.domain.tree.TreeTestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TreeServiceImplTest {

    @Spy
    private final AbstractMapper<TreeDto, Tree> treeMapper = Mappers.getMapper(TreeMapper.class);
    @Spy
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    @Mock
    protected PasswordEncoder passwordEncoder;
    @InjectMocks
    private TreeServiceImpl treeService;
    @Mock
    private TreeRepository treeRepository;
    @Mock
    private SecurityService securityService;
    @Mock
    private UserService userService;
    @Mock
    private FamilyService familyService;
    @Mock
    private PersonService personService;
    private TreeDto treeDto;
    private TreeDto treeDto2;
    private TreeDto treeDtoWithPersons;
    private Tree tree;
    private Tree tree2;
    private Tree treeWithPersons;
    private List<Tree> treeList;
    private List<TreeDto> treeDtoList;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(treeMapper, "userMapper", userMapper);
        ReflectionTestUtils.setField(userMapper, "passwordEncoder", passwordEncoder);
        treeDto = buildTreeDto();
        treeDto2 = buildTreeDto2();
        treeDtoWithPersons = buildTreeDtoWithPersons();
        tree = buildTree();
        tree2 = buildTree2();
        treeWithPersons = buildTreeWithPersons();
        treeList = buildTreeList();
        treeDtoList = buildTreeDtoList();
    }

    @Test
    void Create_Dto_DtoReturned() {
        given(securityService.checkIfUserIsLoggedIn(anyLong())).willReturn(treeDto.getOwner());
        given(treeRepository.saveAndFlush(any(Tree.class))).will(returnsFirstArg());
        given(userService.getById(anyLong())).willReturn(treeDto.getOwner());

        assertEquals(buildTreeDto(), treeService.create(buildTreeDto()));
    }

    @Test
    void Create_OwnerNotLogged_ExceptionThrown() {
        given(securityService.checkIfUserIsLoggedIn(anyLong())).willThrow(NoOwnershipException.class);
        assertThrows(NoOwnershipException.class, () -> treeService.create(treeDto));
    }

    @Test
    void GetById_Id_DtoReturned() {
        given(treeRepository.findById(anyLong())).willReturn(Optional.of(tree));

        Long id = treeDto.getId();
        assertEquals(treeDto, treeService.getById(id));

        verify(securityService).checkIfUserIsLoggedIn(tree.getOwnerUsername());
    }

    @Test
    void getTreesByOwner() {
        given(securityService.checkIfUserIsLoggedIn(anyLong())).willReturn(treeDto.getOwner());
        given(userService.getById(anyLong())).willReturn(treeDto.getOwner());
        given(treeRepository.findByOwner(any(UserEntity.class), any(Pageable.class))).willReturn(new PageImpl<>(treeList));

        Long ownerId = treeDto.getOwner().getId();
        Pageable pageable = PageRequest.of(0, 20);
        List<TreeDto> resultContent = treeService.getTreesByOwner(ownerId, pageable).getContent();
        assertEquals(treeDtoList, resultContent);
    }

    @Test
    void Update_Dto_DtoReturned() {
        given(securityService.checkIfUserIsLoggedIn(anyLong())).willReturn(buildTreeDto().getOwner());
        given(treeRepository.saveAndFlush(any(Tree.class))).will(returnsFirstArg());
        given(treeRepository.findById(anyLong())).willReturn(Optional.of(tree));

        assertEquals(buildTreeDto(), treeService.update(buildTreeDto()));
    }

    @Test
    void Update_OwnerNotPresent_ExceptionThrown() {
        given(securityService.checkIfUserIsLoggedIn(anyLong())).willThrow(NoOwnershipException.class);
        assertThrows(NoOwnershipException.class, () -> treeService.update(treeDto));
    }

    @Test
    void Delete_FromId_DtoReturned() {
        given(treeRepository.findById(anyLong())).willReturn(Optional.of(treeWithPersons));

        assertEquals(treeDtoWithPersons, treeService.delete(treeWithPersons.getId()));
        verify(securityService, atLeastOnce()).checkIfUserIsLoggedIn(tree.getOwnerUsername());
        int personCount = PersonTestData.buildPersonDtoList().size();
        verify(familyService, times(personCount)).deleteContainingPerson(anyLong());
        verify(personService, times(personCount)).delete(anyLong());
        verify(treeRepository).delete(treeWithPersons);
    }

    @Test
    void Delete_WrongId_ExceptionThrown() {
        given(treeRepository.findById(testId1)).willReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> treeService.delete(testId1));
        verify(securityService, never()).checkIfUserIsLoggedIn(tree.getOwnerUsername());
    }


    @Test
    void DeleteAllByOwner_TreesWithoutPersons_DtoListReturned() {
        given(userService.getById(UserTestData.testId)).willReturn(treeDto.getOwner());
        given(treeRepository.findByOwner(tree.getOwner(), Pageable.unpaged())).willReturn(new PageImpl<>(treeList));

        given(treeRepository.findById(anyLong())).will(
                invocation -> Optional.of(TreeTestData.buildTreeWithId(invocation.getArgument(0))));

        assertEquals(treeDtoList, treeService.deleteAllByOwner(UserTestData.testId));
        verify(securityService, times(treeDtoList.size())).checkIfUserIsLoggedIn(UserTestData.testUsername);
        verify(familyService, never()).deleteContainingPerson(anyLong());
        verify(personService, never()).delete(anyLong());
        verify(treeRepository).delete(tree);
    }

    @Test
    void DeleteAllByOwner_DifferentTrees_DtoListReturned() {
        given(userService.getById(UserTestData.testId)).willReturn(treeDto.getOwner());
        given(treeRepository.findByOwner(tree.getOwner(), Pageable.unpaged()))
                .willReturn(new PageImpl<>(List.of(treeWithPersons, tree2)));

        given(treeRepository.findById(anyLong())).will(
                invocation -> Optional.of(TreeTestData.buildTreeWithId(invocation.getArgument(0))));

        assertEquals(List.of(treeDtoWithPersons, treeDto2), treeService.deleteAllByOwner(UserTestData.testId));

        int personCount = treeWithPersons.getPersons().size();
        verify(securityService, times(treeList.size())).checkIfUserIsLoggedIn(UserTestData.testUsername);
        verify(familyService, times(personCount)).deleteContainingPerson(anyLong());
        verify(personService, times(personCount)).delete(anyLong());
        verify(treeRepository, times(2)).delete(any(Tree.class));
    }

    @Test
    void UpdateCounts_TreeId_ReturnedUpdatedTree() {
        given(treeRepository.findById(anyLong())).willReturn(Optional.of(tree));
        given(personService.countByTree(any(TreeDto.class))).willReturn(3L);
        given(treeRepository.saveAndFlush(any(Tree.class))).will(returnsFirstArg());

        Long updatedTreePersonsCount = treeService.updateCounts(treeDto.getId()).getPersonsCount();
        assertEquals(3L, updatedTreePersonsCount);

        verify(securityService).checkIfUserIsLoggedIn(tree.getOwnerUsername());
    }
}
