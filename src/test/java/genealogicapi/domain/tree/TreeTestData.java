package genealogicapi.domain.tree;

import genealogicapi.domain.person.PersonTestData;

import java.util.HashSet;
import java.util.List;

import static genealogicapi.domain.user.UserTestData.buildUserDtoWithoutPassword;
import static genealogicapi.domain.user.UserTestData.buildUserWithoutPassword;

public abstract class TreeTestData {
    public static final Long testId1 = 1L;
    public static final Long testId2 = 2L;
    private static final Long idWithPersons = 3L;
    private static final String testName1 = "test tree name";
    private static final String testName2 = "test tree name 2";

    public static TreeDto buildTreeDto() {
        return TreeDto.builder()
                .id(testId1)
                .name(testName1)
                .owner(buildUserDtoWithoutPassword())
                .personsCount(0L)
                .build();
    }

    public static TreeDto buildTreeDtoWithPersons() {
        TreeDto tree = buildTreeDto();
        tree.setId(idWithPersons);
        tree.setPersonsCount((long) PersonTestData.buildPersonList().size());
        return tree;
    }

    protected static TreeDto buildTreeDto2() {
        return TreeDto.builder()
                .id(testId2)
                .name(testName2)
                .owner(buildUserDtoWithoutPassword())
                .personsCount(0L)
                .build();
    }

    public static Tree buildTree() {
        return Tree.builder()
                .id(testId1)
                .name(testName1)
                .owner(buildUserWithoutPassword())
                .personsCount(0L)
                .build();
    }

    public static Tree buildTreeWithPersons() {
        Tree tree = buildTree();
        tree.setId(idWithPersons);
        tree.setPersons(new HashSet<>(PersonTestData.buildPersonList()));
        tree.setPersonsCount((long) PersonTestData.buildPersonList().size());
        return tree;
    }

    protected static Tree buildTree2() {
        return Tree.builder()
                .id(testId2)
                .name(testName2)
                .owner(buildUserWithoutPassword())
                .personsCount(0L)
                .build();
    }

    public static Tree buildTreeWithId(Long id) {
        switch (id.intValue()) {
            case 1:
            default:
                return buildTree();
            case 2:
                return buildTree2();
            case 3:
                return buildTreeWithPersons();
        }
    }

    public static List<TreeDto> buildTreeDtoList() {
        return List.of(buildTreeDto(), buildTreeDto2());
    }

    public static List<Tree> buildTreeList() {
        return List.of(buildTree(), buildTree2());
    }


}
