package genealogicapi.domain.family;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.person.PersonService;
import genealogicapi.domain.person.PersonTestData;
import genealogicapi.domain.person.mapping.PersonMapper;
import genealogicapi.domain.place.PlaceMapper;
import genealogicapi.domain.tree.Tree;
import genealogicapi.domain.tree.TreeMapper;
import genealogicapi.domain.tree.TreeService;
import genealogicapi.domain.tree.TreeTestData;
import genealogicapi.domain.user.mapping.UserMapper;
import genealogicapi.gedcom.structure.FamilyRecord;
import genealogicapi.security.SecurityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class FamilyServiceImplTest {
    @Spy
    private final AbstractMapper<FamilyDto, FamilyEntity> familyMapper = Mappers.getMapper(FamilyMapper.class);
    @Spy
    private final TreeMapper treeMapper = Mappers.getMapper(TreeMapper.class);
    @Spy
    private final PlaceMapper placeMapper = Mappers.getMapper(PlaceMapper.class);
    @Spy
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    @Spy
    private final PersonMapper personMapper = Mappers.getMapper(PersonMapper.class);
    @Mock
    protected PasswordEncoder passwordEncoder;
    @InjectMocks
    private FamilyServiceImpl familyService;
    @Mock
    private FamilyRepository familyRepository;
    @Mock
    private PersonService personService;
    @Mock
    private TreeService treeService;
    @Mock
    private SecurityService securityService;

    private FamilyDto familyDto1;
    private FamilyDto emptyFamilyDto;
    private FamilyEntity family1;
    private FamilyRecord familyRecord;
    private FamilyRecord emptyFamilyRecord;
    private List<FamilyEntity> familyList;
    private List<FamilyDto> familyDtoList;
    private List<FamilyRecord> familyRecordList;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(familyMapper, "personMapper", personMapper);
        ReflectionTestUtils.setField(personMapper, "treeMapper", treeMapper);
        ReflectionTestUtils.setField(personMapper, "placeMapper", placeMapper);
        ReflectionTestUtils.setField(treeMapper, "userMapper", userMapper);
        ReflectionTestUtils.setField(userMapper, "passwordEncoder", passwordEncoder);
        familyDto1 = FamilyTestData.buildFamilyDto1();
        emptyFamilyDto = FamilyTestData.buildEmptyFamilyDto();
        family1 = FamilyTestData.buildFamily1();
        familyRecord = FamilyTestData.buildFamilyRecord1();
        emptyFamilyRecord = FamilyTestData.buildEmptyFamilyRecord();
        familyList = FamilyTestData.buildFamilyList();
        familyDtoList = FamilyTestData.buildFamilyDtoList();
        familyRecordList = FamilyTestData.buildFamilyRecordList();
    }

    @Test
    void GetFamiliesByTree_treeId_DtoListReturned() {
        given(familyRepository.findAllByTree(any(Tree.class), any(Pageable.class))).willReturn(new PageImpl<>(familyList));
        given(treeService.getById(anyLong())).willReturn(TreeTestData.buildTreeDto());

        Long id = TreeTestData.buildTree().getId();
        List<FamilyDto> content = familyService.getFamiliesByTree(id, PageRequest.of(0, 20)).getContent();
        assertEquals(familyDtoList, content);
    }

    @Test
    void GetAllRecordsByTree_treeId_RecordListReturned() {
        given(familyRepository.findAllByTree(any(Tree.class), any(Pageable.class))).willReturn(new PageImpl<>(familyList));
        given(treeService.getById(anyLong())).willReturn(TreeTestData.buildTreeDto());

        Long id = TreeTestData.buildTree().getId();
        assertEquals(familyRecordList, familyService.getAllRecordsByTree(id));
    }

    @Test
    void GetFamiliesContainingPerson_personId_DtoPageReturned() {
        given(personService.getById(PersonTestData.testId1)).willReturn(PersonTestData.buildPersonDto1());
        given(familyRepository.findDistinctByHusbandOrWifeOrChildrenContaining(
                PersonTestData.buildPerson1(),
                PersonTestData.buildPerson1(),
                PersonTestData.buildPerson1(),
                PageRequest.of(0, 20)
        )).willReturn(new PageImpl<>(familyList));

        Page<FamilyDto> resultPage = familyService.getFamiliesContainingPerson(PersonTestData.testId1, PageRequest.of(0, 20));
        assertEquals(familyDtoList, resultPage.getContent());
    }

    @Test
    void GetFamiliesContainingChild_childId_DtoReturned() {
        given(personService.getById(PersonTestData.testId1)).willReturn(PersonTestData.buildPersonDto1());
        given(familyRepository.findAllByChildrenContaining(
                PersonTestData.buildPerson1(),
                PageRequest.of(0, 20)
        )).willReturn(new PageImpl<>(familyList));

        Page<FamilyDto> resultPage = familyService.getFamiliesContainingChild(PersonTestData.testId1, PageRequest.of(0, 20));
        assertEquals(familyDtoList, resultPage.getContent());
    }

    @Test
    void GetFamiliesContainingSpouse_spouseId_ListReturned() {
        given(personService.getById(PersonTestData.testId1)).willReturn(PersonTestData.buildPersonDto1());
        given(familyRepository.findAllByHusbandOrWife(
                PersonTestData.buildPerson1(),
                PersonTestData.buildPerson1(),
                PageRequest.of(0, 20)
        )).willReturn(new PageImpl<>(familyList));

        Page<FamilyDto> resultPage = familyService.getFamiliesContainingSpouse(PersonTestData.testId1, PageRequest.of(0, 20));
        assertEquals(familyDtoList, resultPage.getContent());
    }

    @Test
    void GetById_id_DtoReturned() {
        given(familyRepository.findById(FamilyTestData.testId1)).willReturn(Optional.of(family1));

        assertEquals(familyDto1, familyService.getById(FamilyTestData.testId1));
    }

    @Test
    void GetById_WrongId_ExceptionThrown() {
        given(familyRepository.findById(FamilyTestData.testId1)).willReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> familyService.getById(FamilyTestData.testId1));
    }

    @Test
    void Create_dto_DtoReturned() {
        given(personService.getById(anyLong()))
                .will(invocationOnMock -> PersonTestData.buildPersonDtoWithId(invocationOnMock.getArgument(0)));
        given(familyRepository.saveAndFlush(any(FamilyEntity.class))).will(returnsFirstArg());

        assertEquals(familyDto1, familyService.create(familyDto1));
    }

    @Test
    void Create_EmptyFamily_ExceptionThrown() {
        assertThrows(EmptyFamilyException.class, () -> familyService.create(emptyFamilyDto));
    }

    @Test
    void Create_Record_RecordReturned() {
        given(personService.getById(anyLong()))
                .will(invocationOnMock -> PersonTestData.buildPersonDtoWithId(invocationOnMock.getArgument(0)));
        given(familyRepository.saveAndFlush(any(FamilyEntity.class))).will(returnsFirstArg());

        Assertions.assertEquals(familyRecord, familyService.create(familyRecord));
    }

    @Test
    void Create_EmptyRecord_ExceptionThrown() {
        assertThrows(EmptyFamilyException.class, () -> familyService.create(emptyFamilyRecord));
    }

    @Test
    void Update_Dto_DtoReturned() {
        given(familyRepository.findById(FamilyTestData.testId1)).willReturn(Optional.of(family1));
        given(personService.getById(anyLong()))
                .will(invocationOnMock -> PersonTestData.buildPersonDtoWithId(invocationOnMock.getArgument(0)));
        given(familyRepository.saveAndFlush(any(FamilyEntity.class))).will(returnsFirstArg());

        assertEquals(familyDto1, familyService.update(familyDto1));
    }

    @Test
    void Update_EmptyDto_ExceptionThrown() {
        given(familyRepository.findById(FamilyTestData.emptyId)).willReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> familyService.update(emptyFamilyDto));
    }

    @Test
    void Delete_Dto_DtoReturned() {
        given(familyRepository.findById(FamilyTestData.testId1)).willReturn(Optional.of(family1));

        assertEquals(familyDto1, familyService.delete(FamilyTestData.testId1));
    }

    @Test
    void Delete_WrongId_ExceptionThrown() {
        given(familyRepository.findById(FamilyTestData.emptyId)).willReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> familyService.delete(FamilyTestData.emptyId));
    }

    @Test
    void DeleteContainingPerson_PersonId_DtoListReturned() {
        given(personService.getById(PersonTestData.testId1)).willReturn(PersonTestData.buildPersonDto1());
        PersonEntity personEntity = PersonTestData.buildPerson1();
        given(familyRepository.deleteAllByHusbandOrWifeOrChildrenContaining(personEntity, personEntity, personEntity)).willReturn(familyList);

        assertEquals(familyDtoList, familyService.deleteContainingPerson(PersonTestData.testId1));
    }
}
