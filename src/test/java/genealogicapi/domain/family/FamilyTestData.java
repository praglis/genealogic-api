package genealogicapi.domain.family;

import genealogicapi.domain.person.PersonTestData;
import genealogicapi.gedcom.structure.FamilyRecord;

import java.util.ArrayList;
import java.util.List;

public abstract class FamilyTestData {
    public static final Long testId1 = 1L;
    private static final String testBio1 = "Biography of 1st family";

    public static final Long testId2 = 2L;
    private static final String testBio2 = "Biography of 2nd family";

    public static final Long emptyId = 3L;
    private static final String emptyBio = "Biography of empty family";

    public static FamilyDto buildFamilyDto1() {
        return FamilyDto.builder()
                .id(testId1)
                .husband(PersonTestData.buildPersonDto3())
                .wife(PersonTestData.buildPersonDto4())
                .children(PersonTestData.buildPersonDtoList())
                .details(testBio1)
                .build();
    }


    protected static FamilyDto buildFamilyDto2() {
        return FamilyDto.builder()
                .id(testId2)
                .husband(PersonTestData.buildPersonDto3())
                .wife(PersonTestData.buildPersonDto4())
                .children(PersonTestData.buildPersonDtoList())
                .details(testBio2)
                .build();
    }

    protected static FamilyDto buildEmptyFamilyDto() {
        return FamilyDto.builder()
                .id(emptyId)
                .details(emptyBio)
                .build();
    }

    public static FamilyEntity buildFamily1() {
        return FamilyEntity.builder()
                .id(testId1)
                .husband(PersonTestData.buildPerson3())
                .wife(PersonTestData.buildPerson4())
                .children(PersonTestData.buildPersonList())
                .details(testBio1)
                .build();
    }

    protected static FamilyEntity buildFamily2() {
        return FamilyEntity.builder()
                .id(testId2)
                .husband(PersonTestData.buildPerson3())
                .wife(PersonTestData.buildPerson4())
                .children(PersonTestData.buildPersonList())
                .details(testBio2)
                .build();
    }

    public static FamilyRecord buildFamilyRecord1() {
        return FamilyRecord.builder()
                .husband(PersonTestData.buildIndividualRecord3())
                .wife(PersonTestData.buildIndividualRecord4())
                .children(PersonTestData.buildIndividualRecordList())
                .details(testBio1)
                .familyEvents(new ArrayList<>())
                .build();
    }

    public static FamilyRecord buildFamilyRecord2() {
        return FamilyRecord.builder()
                .husband(PersonTestData.buildIndividualRecord3())
                .wife(PersonTestData.buildIndividualRecord4())
                .children(PersonTestData.buildIndividualRecordList())
                .details(testBio2)
                .familyEvents(new ArrayList<>())
                .build();
    }

    public static FamilyRecord buildEmptyFamilyRecord() {
        return FamilyRecord.builder()
                .details(emptyBio)
                .familyEvents(new ArrayList<>())
                .build();
    }

    public static List<FamilyDto> buildFamilyDtoList() {
        return List.of(buildFamilyDto1(), buildFamilyDto2());
    }

    public static List<FamilyEntity> buildFamilyList() {
        return List.of(buildFamily1(), buildFamily2());
    }

    public static List<FamilyRecord> buildFamilyRecordList() {
        return List.of(buildFamilyRecord1(), buildFamilyRecord2());
    }

}
