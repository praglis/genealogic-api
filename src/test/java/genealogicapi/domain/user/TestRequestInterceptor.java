package genealogicapi.domain.user;

import lombok.Setter;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TestRequestInterceptor implements ClientHttpRequestInterceptor {
    @Setter
    private String jwtToken = "";

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        request.getHeaders().setBearerAuth(jwtToken);

        return execution.execute(request, body);
    }
}
