package genealogicapi.domain.user;

import genealogicapi.domain.abstraction.CrudControllerIntegrationTest;
import genealogicapi.security.payload.JwtResponse;
import genealogicapi.security.payload.LoginRequest;
import genealogicapi.utils.TestResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class UserControllerIntegrationTest extends CrudControllerIntegrationTest<UserDto> {
    private static final String DOMAIN_URL = "/users";
    private static final Class<UserDto> DOMAIN_DTO_CLASS = UserDto.class;
    private static final String[] EXCLUDED_PROPS_UNIQUE_TO_USER_DTO = new String[]{"password"};
    private static final String[] EXCLUDED_PROPS_FOR_USER_DTO = ArrayUtils.addAll(EXCLUDED_PROPS_FOR_ABSTRACT_DTO, EXCLUDED_PROPS_UNIQUE_TO_USER_DTO);

    protected UserControllerIntegrationTest() {
        super(DOMAIN_URL, DOMAIN_DTO_CLASS);
    }

    @Test
    void Create_Created() throws Exception {
        UserDto userToCreate = UserTestData.buildRandomUserDto();

        TestResponse response = createResource(userToCreate);
        UserDto createdUser = response.getBodyAs(UserDto.class);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED);
        assertThat(createdUser).isEqualToIgnoringGivenFields(userToCreate, EXCLUDED_PROPS_FOR_USER_DTO);
    }

    @Test
    void Create_ExistingUsername_409Returned() throws Exception {
        UserDto existingUser = createAndReturnResource(UserTestData.buildRandomUserDto());
        UserDto userToCreate = UserTestData.buildUserDtoWithUsername(existingUser.getUsername());

        TestResponse response = createResource(userToCreate);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CONFLICT);
    }

    @Test
    void GetAllUsers_ReturnedAll() throws Exception {
        List<UserDto> createdUsers = createAndReturnMultipleResources(UserTestData.buildRandomUserDtos(10));
        createdUsers.add(authUser);

        List<UserDto> gotUsers = getAndReturnPageOfAllResources().getContent();

        assertThat(gotUsers)
                .usingRecursiveComparison()
                .ignoringCollectionOrder()
                .ignoringFields(EXCLUDED_PROPS_FOR_USER_DTO)
                .isEqualTo(createdUsers);
    }

    @Test
    void GetAllUsers_Unauthorized_403Returned() throws Exception {
        createMultipleResources(UserTestData.buildRandomUserDtos(10));

        TestResponse response = getAllResources(false);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    void GetById_EntityReturned() throws Exception {
        UserDto createdUser = createAndReturnResource(UserTestData.buildRandomUserDto());

        TestResponse response = getResourceById(createdUser.getId());
        UserDto gotUser = response.getBodyAs(UserDto.class);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK);
        assertThat(gotUser).isEqualTo(createdUser);
    }

    @Test
    void GetById_NotExistentId_404Returned() throws Exception {
        UserDto createdUser = createAndReturnResource(UserTestData.buildRandomUserDto());

        TestResponse response = getResourceById(createdUser.getId() + 1); //adding 1 to look for not existent ID

        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void Update_CorrectData_Updated() throws Exception {
        UserDto createdUser = createAndReturnResource(UserTestData.buildRandomUserDto());
        UserDto userToUpdate = UserTestData.buildUserDtoWithId(createdUser.getId());

        TestResponse testResponse = updateResource(createdUser.getId(), userToUpdate);
        UserDto updatedUser = testResponse.getBodyAs(UserDto.class);

        assertThat(testResponse.getStatus()).isEqualTo(HttpStatus.OK);
        assertThat(updatedUser).isEqualToIgnoringGivenFields(userToUpdate, EXCLUDED_PROPS_FOR_USER_DTO);
    }

    @Test
    void Update_IdMismatch_400Returned() throws Exception {
        UserDto createdUser = createAndReturnResource(UserTestData.buildRandomUserDto());
        UserDto userToUpdate = UserTestData.buildUserDtoWithId(createdUser.getId());

        TestResponse response = updateResource(
                createdUser.getId() + 1, // adding 1 to modify ID, so it does not match in ID in body
                userToUpdate
        );

        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void Delete_CorrectId_Deleted() throws Exception {
        UserDto existingUser = createAndReturnResource(UserTestData.buildRandomUserDto());

        TestResponse response = deleteResource(existingUser.getId());
        UserDto deletedUser = response.getBodyAs(UserDto.class);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK);
        assertThat(deletedUser).isEqualToIgnoringGivenFields(existingUser, EXCLUDED_PROPS_FOR_USER_DTO);
    }

    @Test
    void Delete_NonExistentID_404Returned() throws Exception {
        UserDto existingUser = createAndReturnResource(UserTestData.buildRandomUserDto());

        TestResponse response = deleteResource(
                true, existingUser.getId() + 1);// adding 1 to modify ID, so it does not match in ID in body

        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);

    }

    @Test
    void AuthenticateUser_CorrectCredentials_Authenticated() throws Exception {
        String password = UserTestData.testPassword;
        UserDto existingUser = createAndReturnResource(UserTestData.buildUserDtoWithPassword(password));

        TestResponse authResponse = authenticateUser(
                LoginRequest.builder()
                        .email(existingUser.getEmail())
                        .password(password)
                        .build()
        );

        assertThat(authResponse.getStatus()).isEqualTo(HttpStatus.OK);
        assertThat(authResponse.getBodyAs(JwtResponse.class).getToken()).isNotNull();
    }

    @Test
    void AuthenticateUser_IncorrectCredentials_401Returned() throws Exception {
        String password = UserTestData.testPassword;
        UserDto existingUser = createAndReturnResource(UserTestData.buildRandomUserDto());

        TestResponse authResponse = authenticateUser(
                LoginRequest.builder()
                        .email(existingUser.getEmail() + "incorrectEmail.com")
                        .password(password + "incorrectPassword")
                        .build()
        );

        assertThat(authResponse.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
    }
}
