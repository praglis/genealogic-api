package genealogicapi.domain.user;

import genealogicapi.security.UserDetailsImpl;
import org.instancio.Instancio;
import org.instancio.Model;
import org.instancio.Select;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.stream.Collectors;

public class UserTestData {
    public static final Long testId = 1L;
    public static final Long testId2 = 2L;
    public static final String testUsername = "Ration";
    public static final String testUsername2 = "Leo";
    public static final String testPassword = "testPassword";
    private static final String testPassword2 = "testPassword2";
    private static final String testEmail = "MarkRoger@mail.com";
    private static final String testEmail2 = "JenniferCotton@mail.com";
    private static final String testFirstName = "Mark";
    private static final String testFirstName2 = "Jennifer";
    private static final String testLastName = "Roger";
    private static final String testLastName2 = "Cotton";

    public static UserDto buildUserDto() {
        return UserDto.builder()
                .id(testId)
                .email(testEmail)
                .firstName(testFirstName)
                .lastName(testLastName)
                .password(testPassword)
                .role(AppUserRole.ROLE_USER)
                .username(testUsername)
                .build();
    }

    public static UserDto buildUserDto2() {
        return UserDto.builder()
                .id(testId2)
                .email(testEmail2)
                .firstName(testFirstName2)
                .lastName(testLastName2)
                .password(testPassword2)
                .role(AppUserRole.ROLE_USER)
                .username(testUsername2)
                .build();
    }

    public static UserDto buildUserDtoWithoutPassword() {
        return UserDto.builder()
                .id(testId)
                .email(testEmail)
                .firstName(testFirstName)
                .lastName(testLastName)
                .role(AppUserRole.ROLE_USER)
                .username(testUsername)
                .build();
    }

    public static UserEntity buildUser() {
        return UserEntity.builder()
                .id(testId)
                .email(testEmail)
                .firstName(testFirstName)
                .lastName(testLastName)
                .password(testPassword)
                .role(AppUserRole.ROLE_USER)
                .username(testUsername)
                .build();
    }

    public static UserEntity buildUserWithoutPassword() {
        return UserEntity.builder()
                .id(testId)
                .email(testEmail)
                .firstName(testFirstName)
                .lastName(testLastName)
                .role(AppUserRole.ROLE_USER)
                .username(testUsername)
                .build();
    }

    public static UserDetails buildUserDetails() {
        return UserDetailsImpl.builder()
                .id(testId)
                .email(testEmail)
                .firstName(testFirstName)
                .lastName(testLastName)
                .username(testUsername)
                .password(testPassword)
                .authorities(AppUserRole.ROLE_USER.getValues().stream()
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList()))
                .build();
    }

    public static List<UserDto> buildRandomUserDtos(int count) {
        return Instancio.ofList(prepareRandomizedUserDtoModel()).size(count).create();
    }

    public static UserDto buildRandomUserDto() {
        return Instancio.of(prepareRandomizedUserDtoModel()).create();
    }

    public static UserDto buildUserDtoWithRole(AppUserRole role) {
        return Instancio.of(prepareRandomizedUserDtoModel())
                .set(Select.field(UserDto::getRole), role)
                .create();
    }

    public static UserDto buildUserDtoWithId(Long id) {
        return Instancio.of(prepareRandomizedUserDtoModel())
                .set(Select.field(UserDto::getId), id)
                .create();
    }

    public static UserDto buildUserDtoWithUsername(String username) {
        return Instancio.of(prepareRandomizedUserDtoModel())
                .set(Select.field(UserDto::getUsername), username)
                .create();
    }


    public static UserDto buildUserDtoWithPassword(String password) {
        return Instancio.of(prepareRandomizedUserDtoModel())
                .set(Select.field(UserDto::getPassword), password)
                .create();
    }

    private static Model<UserDto> prepareRandomizedUserDtoModel() {
        return Instancio.of(UserDto.class)
                .withNullable(Select.field(UserDto::getModificationDate))
                .withNullable(Select.field(UserDto::getCreationDate))
                .withNullable(Select.field(UserDto::getLastName))
                .withNullable(Select.field(UserDto::getFirstName))
                .generate(Select.field(UserDto::getEmail), gen -> gen.text().pattern("#a#a#a#a#a#a@example.com"))
                .generate(Select.field(UserDto::getPassword), gen -> gen.string().minLength(8).maxLength(80))
                .generate(Select.field(UserDto::getRole), gen -> gen.enumOf(AppUserRole.class))
                .toModel();
    }
}
