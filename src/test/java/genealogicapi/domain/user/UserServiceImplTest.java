package genealogicapi.domain.user;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.tree.TreeService;
import genealogicapi.domain.user.mapping.UserMapper;
import genealogicapi.security.payload.LoginRequest;
import info.solidsoft.mockito.java8.api.WithBDDMockito;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.Optional;

import static genealogicapi.domain.user.UserTestData.buildUser;
import static genealogicapi.domain.user.UserTestData.buildUserDto;
import static genealogicapi.domain.user.UserTestData.buildUserDtoWithoutPassword;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class UserServiceImplTest implements WithAssertions, WithBDDMockito {

    @Spy
    private final AbstractMapper<UserDto, UserEntity> userMapper = Mappers.getMapper(UserMapper.class);

    //@Autowired
    @Mock
    protected PasswordEncoder passwordEncoder;
    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private TreeService treeService;


    private LoginRequest loginRequest;
    private UserDto userDto;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(userMapper, "passwordEncoder", passwordEncoder);
        userDto = buildUserDto();
        loginRequest = ((UserMapper) userMapper).toLoginRequest(userDto);
    }

    @Test
    void should_create() {
        given(userRepository.existsByUsernameLike(anyString())).willReturn(false);
        given(userRepository.existsByEmailLike(anyString())).willReturn(false);
        given(userRepository.saveAndFlush(any(UserEntity.class))).will(returnsFirstArg());

        assertEquals(buildUserDtoWithoutPassword(), userService.create(userDto));
    }

    @Test
    void should_not_create() {
        given(userRepository.existsByUsernameLike(anyString())).willReturn(false);
        given(userRepository.existsByEmailLike(anyString())).willReturn(true);

        assertThrows(ValueAlreadyInUseException.class, () -> userService.create(userDto));
    }

    @Test
    void should_getByUsername() {
        given(userRepository.findByUsername(anyString())).willReturn(Optional.of(buildUser()));
        assertEquals(buildUserDto(), userService.getByUsername(buildUserDto().getUsername()));
    }

    @Test
    void should_not_getByUsername() {
        given(userRepository.findByUsername(anyString())).willReturn(Optional.empty());
        String username = userDto.getUsername();
        assertThrows(EntityNotFoundException.class, () -> userService.getByUsername(username));
    }

    @Test
    void should_update() {
        given(userRepository.existsByEmailLikeAndIdNot(anyString(), anyLong())).willReturn(false);
        given(userRepository.existsByUsernameLikeAndIdNot(anyString(), anyLong())).willReturn(false);
        given(userRepository.saveAndFlush(any(UserEntity.class))).will(returnsFirstArg());
        given(userRepository.findById(anyLong())).willReturn(Optional.of(buildUser()));

        assertEquals(buildUserDtoWithoutPassword(), userService.update(buildUserDto()));
    }

    @Test
    void should_not_update() {
        given(userRepository.existsByEmailLikeAndIdNot(anyString(), anyLong())).willReturn(false);
        given(userRepository.existsByUsernameLikeAndIdNot(anyString(), anyLong())).willReturn(true);

        assertThrows(ValueAlreadyInUseException.class, () -> userService.update(userDto));
    }

    @Test
    void should_not_authenticateUser() {
        given(userRepository.findByEmail(anyString())).willReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> userService.authenticateUser(loginRequest));
    }


    @Test
    void should_delete() {
        given(treeService.deleteAllByOwner(anyLong())).willReturn(Collections.emptyList());
        given(userRepository.findById(anyLong())).willReturn(Optional.of(buildUser()));

        assertEquals(buildUserDto(), userService.delete(buildUserDto().getId()));
    }
}
