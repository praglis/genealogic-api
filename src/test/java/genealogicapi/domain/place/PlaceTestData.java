package genealogicapi.domain.place;

public class PlaceTestData {
    public static final String TEST_PLACE_NAME = "Test Place Name";

    public static PlaceDto buildValidPlaceDto() {
        PlaceDto placeDto = new PlaceDto();
        placeDto.setName(TEST_PLACE_NAME);
        return placeDto;
    }
}
