package genealogicapi.domain.date;

import java.time.LocalDate;

public class DateTestData {

    public static final int TEST_YEAR = 2025;

    public static DateDto buildValidDateDto() {
        return DateDto.builder()
                .value(LocalDate.of(TEST_YEAR, 1, 1))
                .valueAccuracy(DateAccuracy.YEAR)
                .estimation(DateEstimation.EXACT)
                .build();
    }
}
