package genealogicapi.gedcom.structure;

import genealogicapi.domain.date.DateTestData;
import genealogicapi.domain.event.EventType;
import genealogicapi.domain.place.PlaceTestData;
import genealogicapi.gedcom.SupportedGedcomVersion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static genealogicapi.domain.date.DateTestData.TEST_YEAR;

public abstract class EventRecordTestData {
    public static final String TEST_SUBTYPE = "Event subtype";
    public static final String TEST_CAUSE = "Test event cause";
    public static final String TEST_NAME = "Birth of Test Person";
    public static final String SHORT_TEST_DETAILS = "Test event details";
    public static final int BASE_LEVEL = 1;
    private static final String YES_DESCRIPTOR = "Y";

    public static EventRecord buildValidEventRecord() {
        EventRecord eventRecord = initEventRecord();

        eventRecord.setName(TEST_NAME);
        eventRecord.setNameCustom(true);
        eventRecord.setType(EventType.BIRT);
        eventRecord.setSubtype(TEST_SUBTYPE);
        eventRecord.setStartDate(DateTestData.buildValidDateDto());
        eventRecord.setPlace(PlaceTestData.buildValidPlaceDto());
        eventRecord.setCause(TEST_CAUSE);
        eventRecord.setDetails(SHORT_TEST_DETAILS);

        return eventRecord;
    }


    public static EventRecord buildGenericEventRecordWithCustomNameOnly() {
        EventRecord eventRecord = initEventRecord();

        eventRecord.setType(null);
        eventRecord.setNameCustom(true);
        eventRecord.setName(TEST_NAME);
        eventRecord.setDetails("");

        return eventRecord;
    }

    public static EventRecord buildDeathRecordWithoutAnyData() {
        EventRecord eventRecord = initEventRecord();

        eventRecord.setType(EventType.DEAT);

        return eventRecord;
    }

    private static EventRecord initEventRecord() {
        EventRecord eventRecord = new EventRecord();
        eventRecord.lines = new ArrayList<>();
        eventRecord.setVersion(SupportedGedcomVersion.V555);
        return eventRecord;
    }

    public static List<GedcomLine> buildValidGedcomLinesContainingEventRecord() {
        return List.of(
                new GedcomLine(BASE_LEVEL, GedcomTag.BIRT),
                new GedcomLine(BASE_LEVEL + 1, GedcomTag.TYPE, TEST_SUBTYPE),
                new GedcomLine(BASE_LEVEL + 1, GedcomTag.DATE, String.valueOf(TEST_YEAR)),
                new GedcomLine(BASE_LEVEL + 1, GedcomTag.PLAC, PlaceTestData.TEST_PLACE_NAME),
                new GedcomLine(BASE_LEVEL + 1, GedcomTag.CAUS, TEST_CAUSE),
                new GedcomLine(BASE_LEVEL + 1, GedcomTag.NOTE, SHORT_TEST_DETAILS)
        );
    }

    public static List<GedcomLine> buildGedcomLineContainingGenericEventRecordWithCustomDescriptor() {
        return Collections.singletonList(
                new GedcomLine(BASE_LEVEL, GedcomTag.EVEN, TEST_NAME)
        );
    }

    public static List<GedcomLine> buildGedcomLineContainingDeathRecordWithYesDescriptor() {
        return Collections.singletonList(
                new GedcomLine(BASE_LEVEL, GedcomTag.DEAT, YES_DESCRIPTOR)
        );
    }
}
