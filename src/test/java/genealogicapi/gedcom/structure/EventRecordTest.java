package genealogicapi.gedcom.structure;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static genealogicapi.gedcom.structure.EventRecordTestData.*;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class EventRecordTest {

    @Test
    void AddDataToLines_ValidEventRecord_EventRecordAddedToLines() {
        EventRecord eventRecord = buildValidEventRecord();

        eventRecord.addDataToLines(BASE_LEVEL);

        assertThat(eventRecord.getLines()).containsAll(buildValidGedcomLinesContainingEventRecord());
    }


    @Test
    void AddDataToLines_GenericEventRecordWithCustomNameOnly_OneLineRecordAddedToLines() {
        EventRecord eventRecord = buildGenericEventRecordWithCustomNameOnly();

        eventRecord.addDataToLines(BASE_LEVEL);

        assertThat(eventRecord.getLines()).containsAll(buildGedcomLineContainingGenericEventRecordWithCustomDescriptor());
    }

    @Test
    void AddDataToLines_DeathRecordWithoutAnyData_OneLineRecordAddedToLinesWithYesDescriptor() {
        EventRecord eventRecord = buildDeathRecordWithoutAnyData();

        eventRecord.addDataToLines(BASE_LEVEL);

        assertThat(eventRecord.getLines()).containsAll(buildGedcomLineContainingDeathRecordWithYesDescriptor());
    }


}
