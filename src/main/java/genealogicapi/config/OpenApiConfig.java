package genealogicapi.config;

import genealogicapi.domain.abstraction.AbstractEntity;
import genealogicapi.domain.event.role.EventRole;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.SpringDocUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Value("${genea-logic.api.name}")
    private String appName;

    @Value("${genea-logic.api.description}")
    private String appDescription;

    @Bean
    public OpenAPI openApi() {
        final String securitySchemeName = "Authorization";
        SpringDocUtils.getConfig().addAnnotationsToIgnore(EventRole.class, AbstractEntity.class);
        return new OpenAPI()
                .info(apiInfo())
                .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
                .components(
                        new Components()
                                .addSecuritySchemes(securitySchemeName,
                                        new SecurityScheme()
                                                .name(securitySchemeName)
                                                .type(SecurityScheme.Type.HTTP)
                                                .scheme("Bearer")
                                                .bearerFormat("JWT")
                                                .in(SecurityScheme.In.HEADER))
                );
    }


    private Info apiInfo() {
        return new Info()
                .title(appName + " API")
                .version("1.0.0")
                .description(String.format("API for %s - %s", appName, appDescription))
                .contact(new Contact()
                        .name("Paweł Raglis")
                        .email("paw.raglis@gmail.com"));
    }
}
