package genealogicapi.gedcom.exporting.exception;

public class GedcomExportException extends RuntimeException {
    public GedcomExportException(String message) {
        super("GEDCOM export exception: " + message);
    }
}
