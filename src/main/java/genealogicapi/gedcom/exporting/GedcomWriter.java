package genealogicapi.gedcom.exporting;

import genealogicapi.domain.event.participant.EventParticipantDto;
import genealogicapi.domain.person.PersonDto;
import genealogicapi.domain.user.UserDto;
import genealogicapi.gedcom.SupportedGedcomVersion;
import genealogicapi.gedcom.exporting.exception.GedcomExportException;
import genealogicapi.gedcom.structure.*;
import lombok.NonNull;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static genealogicapi.gedcom.GedcomServiceImpl.UTF8_BOM;

public class GedcomWriter {
    private final SupportedGedcomVersion version;
    private final UserDto user;
    private final List<IndividualRecord> individuals;
    private final List<EventRecord> events;
    private final List<FamilyRecord> families;
    private final String appName;
    private BufferedWriter writer;

    public GedcomWriter(SupportedGedcomVersion version,
                        UserDto user,
                        List<IndividualRecord> persons,
                        List<EventRecord> events,
                        List<FamilyRecord> families, String appName) {
        this.version = version;
        this.user = user;
        this.individuals = new ArrayList<>(persons);
        this.events = new ArrayList<>(events);
        this.families = new ArrayList<>(families);
        this.appName = appName;
    }

    public void generateGedcom(OutputStream file) throws IOException {
        writer = new BufferedWriter(new OutputStreamWriter(file, StandardCharsets.UTF_8));

        writeHeaderAndSubmitterRecord();
        writeRecords();
        writeTrailerRecord();

        writer.close();
    }

    private void writeTrailerRecord() throws IOException {
        writeLine(new GedcomLine(0, GedcomTag.TRLR));
    }

    private void writeRecords() throws IOException {
        setXrefIdsForIndividuals();
        setXrefIdsForFamilies();

        assignEventsToIndividualsAndFamilies();

        for (IndividualRecord ir : individuals) {
            ir.setVersion(version);
            ir.addDataToLines(0);
            for (GedcomLine gl : ir.getLines()) {
                writeLine(gl);
            }
        }

        for (FamilyRecord fr : families) {
            fr.setVersion(version);
            fr.addDataToLines(0);
            for (GedcomLine gl : fr.getLines()) {
                writeLine(gl);
            }
        }
    }

    private void assignEventsToIndividualsAndFamilies() {
        for (EventRecord er : events) {
            if (er.getParticipants().size() == 1 && (er.getType() == null || er.getType().isIndividual())) {
                individuals.stream()
                        .filter(ir -> ir.getId().equals(er.getParticipants().iterator().next().getPerson().getId()))
                        .findAny()
                        .orElseThrow(() -> new GedcomExportException("Event participant for event not found"))
                        .addEvent(er);
            } else if (er.getParticipants().size() > 1 && (er.getType() == null || !er.getType().isIndividual())) {
                Iterator<EventParticipantDto> participantsIterator = er.getParticipants().iterator();
                PersonDto spouse1 = participantsIterator.next().getPerson();
                PersonDto spouse2 = participantsIterator.next().getPerson();
                families.stream()
                        .filter(fr -> fr.spousesContainId(spouse1.getId()) && fr.spousesContainId(spouse2.getId()))
                        .findFirst()
                        .orElseThrow(() -> new GedcomExportException("Family for event not found"))
                        .addEvent(er);
            }
        }
    }

    private void setXrefIdsForFamilies() {
        for (int i = 0; i < families.size(); i++) {
            FamilyRecord fr = families.get(i);
            fr.setXrefId("F" + (i + 1));
            IndividualRecord husband;
            IndividualRecord wife;
            List<IndividualRecord> children;

            if (fr.hasHusband()) {
                husband = findIndividual(fr.getHusband().getId());
                husband.addFamilyPointer(fr.getXrefId(), GedcomTag.FAMS);
                fr.setHusband(husband);
            }
            if (fr.hasWife()) {
                wife = findIndividual(fr.getWife().getId());
                wife.addFamilyPointer(fr.getXrefId(), GedcomTag.FAMS);
                fr.setWife(wife);
            }
            if (fr.hasAtLeastOneChild()) {
                children = new ArrayList<>();
                for (IndividualRecord child : fr.getChildren()) {
                    children.add(findIndividual(child.getId()));
                }
                for (IndividualRecord child : children) {
                    child.addFamilyPointer(fr.getXrefId(), GedcomTag.FAMC);
                }
                fr.setChildren(children);
            }
            families.set(i, fr);
        }
    }

    private void setXrefIdsForIndividuals() {
        for (int i = 0; i < individuals.size(); i++) {
            IndividualRecord ir = individuals.get(i);
            ir.setXrefId("I" + (i + 1));
            individuals.set(i, ir);
        }
    }

    private IndividualRecord findIndividual(@NonNull Long id) {
        return individuals.stream()
                .filter(ir -> ir.getId().equals(id))
                .findFirst().orElseThrow(() -> new GedcomExportException("Individual not found"));
    }

    private void writeHeaderAndSubmitterRecord() throws IOException {
        writer.write(UTF8_BOM);
        writeLine(new GedcomLine(0, GedcomTag.HEAD));
        writeLine(new GedcomLine(1, GedcomTag.GEDC));
        writeLine(new GedcomLine(2, GedcomTag.VERS, version.getVersion()));
        writeLine(new GedcomLine(2, GedcomTag.FORM, "LINEAGE-LINKED"));
        if (version == SupportedGedcomVersion.V555) {
            writeLine(new GedcomLine(3, GedcomTag.VERS, version.getVersion()));
        }

        writeLine(new GedcomLine(1, GedcomTag.CHAR, "UTF-8"));
        writeLine(new GedcomLine(1, "SR", GedcomTag.SUBM, true));
        writeLine(new GedcomLine(1, GedcomTag.SOUR, appName));


        writeLine(new GedcomLine(0, "SR", GedcomTag.SUBM));
        writeLine(new GedcomLine(1, GedcomTag.NAME, user.getUsername()));
        writeLine(new GedcomLine(1, GedcomTag.EMAIL, user.getEmail()));
    }

    private void writeLine(GedcomLine gedcomLine) throws IOException {
        writer.write(gedcomLine.toString());
        writer.newLine();
    }
}
