package genealogicapi.gedcom.structure;

public interface ContainingEvents {
    void addEvent(EventRecord eventRecord);
}
