package genealogicapi.gedcom.structure;

import genealogicapi.domain.date.DateAccuracy;
import genealogicapi.domain.date.DateDto;
import genealogicapi.domain.date.DateEstimation;
import genealogicapi.domain.date.GedcomMonth;
import genealogicapi.domain.event.EventType;
import genealogicapi.domain.event.participant.EventParticipantDto;
import genealogicapi.domain.place.PlaceDto;
import genealogicapi.domain.user.UserDto;
import genealogicapi.gedcom.SupportedGedcomVersion;
import genealogicapi.gedcom.importing.exception.EmptyRecordException;
import genealogicapi.gedcom.importing.exception.IncorrectGedcomRecord;
import genealogicapi.gedcom.importing.exception.MissingTagException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class EventRecord extends GedcomRecord {

    public static final String YES_DESCRIPTOR = "Y";
    private Long id;
    private String name;
    private boolean isNameCustom = false;

    private EventType type;
    private String subtype;

    private PlaceDto place;
    private DateDto startDate;
    private DateDto endDate;

    private String cause;
    private String details;

    private UserDto owner;
    private Set<EventParticipantDto> participants;

    public EventRecord(final GedcomRecord eventStructure, final @NonNull SupportedGedcomVersion version) {
        super(version, eventStructure);

        GedcomLine firstLine = this.lines.get(0);
        this.type = EventType.valueOf(firstLine.getTag().name());

        if (hasTagWithItem(GedcomTag.TYPE)) {
            this.subtype = findItemByTagOrElseDefault(GedcomTag.TYPE, null);
        }

        if (this.type == EventType.EVEN) {
            this.name = firstLine.getItem();
            this.isNameCustom = true;
        }

        GedcomRecord placeRecord = new GedcomRecord(findRecord(GedcomTag.PLAC), findRecord(GedcomTag.ADDR), version);
        place = readPlaceAndAddressRecords(placeRecord);

        if (hasTag(GedcomTag.DATE)) startDate = readDateRecord(findRecord(GedcomTag.DATE));

        cause = findItemByTagOrElseDefault(GedcomTag.CAUS, null);

        if (hasTag(GedcomTag.NOTE)) {
            details = new LongTextRecord(findRecord(GedcomTag.NOTE)).getLongText();
        }
    }

    @Override
    public void addDataToLines(int level) {
        GedcomTag eventTypeTag;
        if (type != null && GedcomTag.contains(type.name())) eventTypeTag = GedcomTag.valueOf(type.name());
        else eventTypeTag = GedcomTag.EVEN;

        if (eventTypeTag == GedcomTag.EVEN && this.isNameCustom) {
            lines.add(new GedcomLine(level, eventTypeTag, this.name));
        } else {
            lines.add(new GedcomLine(level, eventTypeTag));
        }

        addToLinesIfNotNullOrBlank(this.subtype, level + 1, GedcomTag.TYPE);
        addDateToLines(startDate, level + 1);
        addPlaceAndAddressToGedcomLines(place, level + 1);
        addToLinesIfNotNullOrBlank(cause, level + 1, GedcomTag.CAUS);

        if (StringUtils.isNotBlank(details)) {
            LongTextRecord longTextRecord = new LongTextRecord(level + 1, GedcomTag.NOTE, details, version);
            lines.addAll(longTextRecord.getLines());
        }

        if (lines.size() == 1) {
            if (eventTypeTag != GedcomTag.BIRT && eventTypeTag != GedcomTag.EVEN) {
                lines.set(0, new GedcomLine(level, eventTypeTag, YES_DESCRIPTOR));
            } else if (eventTypeTag == GedcomTag.BIRT || !lines.get(0).hasItem()) {
                lines.remove(0);
            }
        }
    }


    private void addDateToLines(DateDto date, int level) {
        if (startDate == null) return;

        StringBuilder dateItemBuilder = new StringBuilder(date.getEstimation().getValue());
        if (date.getEstimation() != DateEstimation.UNKNOWN) {
            dateItemBuilder.append(writeDateRecord(date.getValue(), date.getValueAccuracy()));
        } else {
            dateItemBuilder = new StringBuilder(String.format("(%s)", date.getPhrase()));
        }

        if (date.getEstimation() == DateEstimation.BETWEEN) {
            dateItemBuilder.append(" AND ").append(writeDateRecord(date.getMaxValue(), date.getMaxValueAccuracy()));
        }

        lines.add(new GedcomLine(level, GedcomTag.DATE, dateItemBuilder.toString()));
    }

    private String writeDateRecord(LocalDate localDate, DateAccuracy valueAccuracy) {
        String dateString = String.valueOf(localDate.getYear());
        if (valueAccuracy != DateAccuracy.YEAR) {
            dateString = GedcomMonth.ofJavaMonth(localDate.getMonth()).name().concat(" " + dateString);
        }
        if (valueAccuracy == DateAccuracy.DAY) {
            dateString = String.valueOf(localDate.getDayOfMonth()).concat(" " + dateString);
        }
        return dateString;
    }

    private DateDto readDateRecord(GedcomRecord dateRecord) {
        if (dateRecord.isEmpty())
            throw new MissingTagException(GedcomTag.DATE, "Every date record must start with DATE tag.");
        GedcomLine dateLine = dateRecord.getLines().get(0);
        if (!dateLine.hasItem()) throw new EmptyRecordException(GedcomTag.DATE);

        List<String> datePieces = new ArrayList<>(Arrays.asList(dateLine.getItem().split(" ")));

        DateDto date = new DateDto();

        if (RecordDateEstimation.contains(datePieces.get(0))) {
            date.setEstimation(DateEstimation.of(RecordDateEstimation.valueOf(datePieces.get(0))));
            datePieces.remove(0);
        } else if (datePieces.get(0).startsWith("(") || datePieces.get(0).startsWith("FROM")) {
            date.setEstimation(DateEstimation.UNKNOWN);
            date.setPhrase(dateLine.getItem().replace("(", "").replace(")", ""));
        } else date.setEstimation(DateEstimation.EXACT);

        if (date.getEstimation() == DateEstimation.BETWEEN) {
            int andPosition = datePieces.indexOf("AND");

            List<String> dateValue = datePieces.subList(0, andPosition);
            date.setValue(getDateValueFromPieces(dateValue));
            date.setValueAccuracy(DateAccuracy.values()[dateValue.size() - 1]);

            List<String> maxDateValue = datePieces.subList(andPosition + 1, datePieces.size());
            date.setMaxValue(getDateValueFromPieces(maxDateValue));
            date.setMaxValueAccuracy(DateAccuracy.values()[maxDateValue.size() - 1]);

        } else if (date.getEstimation() != DateEstimation.UNKNOWN) {
            date.setValue(getDateValueFromPieces(datePieces));
            date.setValueAccuracy(DateAccuracy.values()[datePieces.size() - 1]);
        }

        return date;
    }

    public LocalDate getDateValueFromPieces(List<String> datePieces) {
        if (datePieces.size() == 1) {
            return Year.of(Integer.parseInt(datePieces.get(0))).atDay(1);
        } else if (datePieces.size() == 2) {
            return YearMonth.of(
                    Integer.parseInt(datePieces.get(1)),
                    GedcomMonth.toJavaMonth(datePieces.get(0))
            ).atDay(1);
        } else if (datePieces.size() == 3) {
            return LocalDate.of(
                    Integer.parseInt(datePieces.get(2)),
                    GedcomMonth.toJavaMonth(datePieces.get(1)),
                    Integer.parseInt(datePieces.get(0))
            );
        } else
            throw new IncorrectGedcomRecord(GedcomTag.DATE,
                    String.format("Incorrect record value: '%s'", String.join(" ", datePieces)));
    }

    public enum RecordDateEstimation {
        EXACT,
        ABT,//ABT
        CAL,//ABT
        EST,//ABT
        BEF,
        AFT,
        BET;

        public static boolean contains(String name) {
            for (RecordDateEstimation dae : RecordDateEstimation.values()) {
                if (dae.name().equals(name)) {
                    return true;
                }
            }
            return false;
        }
    }
}
