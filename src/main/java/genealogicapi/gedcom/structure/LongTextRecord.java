package genealogicapi.gedcom.structure;

import genealogicapi.gedcom.SupportedGedcomVersion;
import lombok.Getter;

import java.util.Arrays;
import java.util.stream.Collectors;

public class LongTextRecord extends GedcomRecord {
    public static final int MAX_LINE_LENGTH = 248;
    @Getter
    private String longText;

    public LongTextRecord(int baseLevel, GedcomTag baseTag, String wholeText, SupportedGedcomVersion version) {
        super(version);

        lines = Arrays.stream(wholeText.split("\\R"))
                .map(s -> new GedcomLine(baseLevel + 1, GedcomTag.CONT, s))
                .collect(Collectors.toList());

        lines.set(0, new GedcomLine(baseLevel, baseTag, lines.get(0).getItem()));

        for (int i = 0; i < lines.size(); i++) {
            GedcomLine gedcomLine = lines.get(i);
            if (gedcomLine.getItem().length() > MAX_LINE_LENGTH) {
                lines.set(i, new GedcomLine(baseLevel + 1, gedcomLine.getTag(), gedcomLine.getItem().substring(0, MAX_LINE_LENGTH)));
                lines.add(i + 1, new GedcomLine(baseLevel + 1, GedcomTag.CONC, gedcomLine.getItem().substring(MAX_LINE_LENGTH)));
            }
        }
    }

    public LongTextRecord(GedcomRecord gedcomRecord) {
        super(gedcomRecord.getVersion(), gedcomRecord);

        longText = lines.get(0).getItem();

        for (GedcomLine gedcomLine : lines) {
            if (gedcomLine.getTag() == GedcomTag.CONT) {
                String item = gedcomLine.hasItem() ? gedcomLine.getItem() : "";
                longText = longText.concat(System.lineSeparator() + item);
            } else if (gedcomLine.getTag() == GedcomTag.CONC) longText = longText.concat(gedcomLine.getItem());
        }
    }
}
