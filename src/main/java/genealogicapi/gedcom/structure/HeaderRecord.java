package genealogicapi.gedcom.structure;

import genealogicapi.gedcom.SupportedGedcomVersion;
import genealogicapi.gedcom.importing.exception.GedcomImportException;
import genealogicapi.gedcom.importing.exception.MissingTagException;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class HeaderRecord extends TopLevelRecord {

    public HeaderRecord(String firstLine, Scanner gs) {
        super(firstLine, gs, null);

        if (lines.get(0).getTag() != GedcomTag.HEAD) {
            throw new MissingTagException(GedcomTag.HEAD, "GEDCOM file should begin with header record.");
        }

        checkIfContainsSubtag(GedcomTag.SOUR, GedcomTag.HEAD);
        checkIfContainsSubtag(GedcomTag.CHAR, GedcomTag.HEAD);
        GedcomLine versionLine = checkIfContainsSubtag(GedcomTag.VERS, GedcomTag.GEDC);
        if (versionLine.getItem().equals(SupportedGedcomVersion.V551.getVersion()))
            checkIfContainsSubtag(GedcomTag.SUBM, GedcomTag.HEAD);
        GedcomLine formLine = checkIfContainsSubtag(GedcomTag.FORM, GedcomTag.GEDC);

        if (Arrays.stream(SupportedGedcomVersion.values())
                .map(SupportedGedcomVersion::getVersion)
                .noneMatch(s -> s.equals(versionLine.getItem())))
            throw new GedcomImportException("Other GEDCOM versions than " +
                    Arrays.stream(SupportedGedcomVersion.values())
                            .map(SupportedGedcomVersion::getVersion)
                            .collect(Collectors.joining(" or "))
                    + " are not supported.");

        for (SupportedGedcomVersion version : SupportedGedcomVersion.values()) {
            if (version.getVersion().equals(versionLine.getItem())) this.version = version;
        }

        if (!"LINEAGE-LINKED".equals(formLine.getItem())) throw new GedcomImportException(
                "Unrecognised GEDCOM form. Only LINEAGE-LINKED forms are supported.");

    }

    private GedcomLine checkIfContainsSubtag(GedcomTag childTag, GedcomTag parentTag) {
        GedcomLine parentLine = lines.stream()
                .filter(gedcomLine -> gedcomLine.getTag() == parentTag)
                .findFirst().orElseThrow(() -> {
                    throw new MissingTagException(parentTag, "Every header record must contain " + parentTag.name() + " tag");
                });

        int parentIndex = lines.indexOf(parentLine);

        while (++parentIndex < lines.size() && lines.get(parentIndex).getLevel() > parentLine.getLevel()) {
            if (lines.get(parentIndex).getTag() == childTag) return lines.get(parentIndex);
        }
        throw new MissingTagException(childTag, parentTag.name() + " tag in header must contain " + childTag.name());
    }
}
