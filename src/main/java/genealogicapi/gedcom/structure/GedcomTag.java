package genealogicapi.gedcom.structure;

public enum GedcomTag {
    ABBR, ADDR, ADOP, ADR1, ADR2, ADR3, AGE, AGNC, BAPM, BIRT, BURI, CALN, CAUS, CHAN, CHAR, CHIL, CHR, CITY, CONC,
    CONF, CONT, CORP, CTRY, DATA, DATE, DEAT, DEST, DIV, EMAIL, EMIG, ENGA, EVEN, FAM, FAMC, FAMS, FILE, FORM, GEDC,
    GIVN, GRAD, HEAD, HUSB, IMMI, INDI, LANG, LATI, LONG, MAP, MARR, NAME, NATU, NICK, NOTE, NPFX, NSFX, OBJE, OCCU,
    PAGE, PEDI, PHON, PLAC, POST, RELI, REPO, RESI, RETI, RFN, RIN, SEX, SOUR, SPFX, STAE, SUBM, SUBN, SURN, TIME,
    TITL, TRLR, TYPE, USER_DEFINED, VERS, WIFE, WWW;

    // IMMI and EMIG are MIGR in EventType
    // ^ - no longer true

    public static boolean contains(String name) {
        for (GedcomTag gt : GedcomTag.values()) {
            if (gt.name().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean isAddressTag() {
        return this == ADDR || this == ADR1 || this == ADR2 || this == ADR3;
    }
}
