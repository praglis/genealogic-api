package genealogicapi.gedcom.structure;

import genealogicapi.domain.event.EventType;
import genealogicapi.domain.family.Family;
import genealogicapi.gedcom.SupportedGedcomVersion;
import genealogicapi.gedcom.importing.exception.MissingPointerException;
import genealogicapi.gedcom.importing.exception.MissingTagException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Builder
@Slf4j
public class FamilyRecord extends TopLevelRecord implements Family<IndividualRecord>, ContainingEvents {

    private IndividualRecord husband;
    private IndividualRecord wife;
    private List<IndividualRecord> children;
    private String details;
    private List<EventRecord> familyEvents;

    public FamilyRecord(String firstStringLine, Scanner gs, SupportedGedcomVersion version) {
        super(firstStringLine, gs, version);

        GedcomLine firstParsedLine = lines.get(0);

        if (firstParsedLine.getTag() != GedcomTag.FAM) {
            throw new MissingTagException(GedcomTag.FAM, "Each family record must begin with FAM tag.");
        }

        if (firstParsedLine.getXref() == null) throw new MissingPointerException("family");
        xrefId = firstParsedLine.getXref();

        Optional<String> husbandXref = findXref(GedcomTag.HUSB);
        husband = husbandXref.map(IndividualRecord::new).orElseGet(IndividualRecord::empty);

        Optional<String> wifeXref = findXref(GedcomTag.WIFE);
        wife = wifeXref.map(IndividualRecord::new).orElseGet(IndividualRecord::empty);

        children = findAllXrefs(GedcomTag.CHIL).stream()
                .map(IndividualRecord::new)
                .collect(Collectors.toList());

        if (hasTag(GedcomTag.NOTE)) {
            LongTextRecord note = new LongTextRecord(findRecord(GedcomTag.NOTE));
            details = note.getLongText();
        }

        familyEvents = new ArrayList<>();
        for (EventType et : EventType.values()) {
            if (et.isIndividual()) continue;
            GedcomTag tag = GedcomTag.valueOf(et.name());
            if (hasTag(tag)) {
                familyEvents.add(new EventRecord(findRecord(tag), version));
            }
        }

        log.info("LOADED FAMILY RECORD {}", lines.get(0).getXref());
    }

    @Override
    public Set<IndividualRecord> getSpouses() {
        Set<IndividualRecord> spouses = new HashSet<>();
        if (hasHusband()) spouses.add(husband);
        if (hasWife()) spouses.add(wife);
        return spouses;
    }

    @Override
    public boolean spousesContainId(@NonNull Long id) {
        return getSpouses().stream()
                .anyMatch(individualRecord -> individualRecord.getId().equals(id));
    }

    @Override
    public boolean hasHusband() {
        return this.husband != null && this.husband.isPresent();
    }

    @Override
    public boolean hasWife() {
        return this.wife != null && this.wife.isPresent();
    }

    @Override
    public boolean hasAtLeastOneChild() {
        return this.children != null && !this.children.isEmpty();
    }

    @Override
    public void addEvent(EventRecord eventRecord) {
        familyEvents.add(eventRecord);
    }

    @Override
    public void addDataToLines(int level) {
        lines.add(new GedcomLine(level, xrefId, GedcomTag.FAM));

        for (EventRecord familyEvent : familyEvents) {
            familyEvent.setVersion(version);
            familyEvent.addDataToLines(level + 1);
            lines.addAll(familyEvent.getLines());
        }

        if (hasHusband()) lines.add(new GedcomLine(level + 1, husband.getXrefId(), GedcomTag.HUSB, true));
        if (hasWife()) lines.add(new GedcomLine(level + 1, wife.getXrefId(), GedcomTag.WIFE, true));
        if (hasAtLeastOneChild()) {
            for (IndividualRecord child : children) {
                lines.add(new GedcomLine(level + 1, child.getXrefId(), GedcomTag.CHIL, true));
            }
        }

        if (StringUtils.isNotBlank(details)) {
            LongTextRecord longTextRecord = new LongTextRecord(level + 1, GedcomTag.NOTE, details, version);
            lines.addAll(longTextRecord.getLines());
        }
    }
}
