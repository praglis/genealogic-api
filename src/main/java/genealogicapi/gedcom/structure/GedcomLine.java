package genealogicapi.gedcom.structure;

import genealogicapi.gedcom.importing.exception.IllegalLevelNumber;
import genealogicapi.gedcom.importing.exception.MissingTagException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@EqualsAndHashCode
public class GedcomLine {
    private static final String CROSS_REFERENCE_REGEX = "@[\\w]+@";
    private static final String ESCAPE_SEQUENCE_REGEX = "@#[\\w ]+@ ";
    private final int level;
    private final GedcomTag tag;
    private String xref;
    private String item;
    private boolean isXrefPointer = false;
    private String escapeSequence;

    public GedcomLine(String line) {
        Scanner scanner = new Scanner(line);
        level = readLevel(scanner);

        String xrefOrTag = lookForXrefId(scanner);
        tag = readTag(line, xrefOrTag);

        if (scanner.hasNext()) {
            scanner.useDelimiter("\\A");
            String restOfLine = scanner.next();

            restOfLine = lookForEscapeSequence(restOfLine);
            lookForLineValue(restOfLine);
        }
    }

    public GedcomLine(int level, String xref, GedcomTag tag) {
        this.level = level;
        this.tag = tag;
        this.xref = xref;
    }

    public GedcomLine(int level, String xref, GedcomTag tag, boolean isXrefPointer) {
        this.level = level;
        this.tag = tag;
        this.xref = xref;
        this.isXrefPointer = isXrefPointer;
    }

    public GedcomLine(int level, GedcomTag tag, String item) {
        this.level = level;
        this.tag = tag;
        this.item = item;
    }

    public GedcomLine(int level, GedcomTag tag) {
        this.level = level;
        this.tag = tag;
    }

    private void lookForLineValue(String restOfLine) {
        if (StringUtils.isNotBlank(restOfLine)) {
            Pattern xrefPattern = Pattern.compile(CROSS_REFERENCE_REGEX);
            Matcher xrefMatcher = xrefPattern.matcher(restOfLine);

            if (xrefMatcher.find()) {
                xref = xrefMatcher.group().strip().substring(1, restOfLine.length() - 2);
                isXrefPointer = true;
            } else item = restOfLine.substring(1);
        }
    }

    private String lookForXrefId(Scanner scanner) {
        String xrefOrTag = scanner.next();
        if (xrefOrTag.startsWith("@") && xrefOrTag.endsWith("@")) {
            xref = xrefOrTag.strip().substring(1, xrefOrTag.length() - 1);
            xrefOrTag = scanner.next();
        }
        return xrefOrTag;
    }

    private GedcomTag readTag(String line, String xrefOrTag) {
        final GedcomTag gedcomTag;
        if (GedcomTag.contains(xrefOrTag)) gedcomTag = GedcomTag.valueOf(xrefOrTag);
        else if (xrefOrTag.matches("_\\w{3,30}")) gedcomTag = GedcomTag.USER_DEFINED;
        else throw new MissingTagException(
                    "Every GEDCOM line should contain GEDCOM tag. No tag detected in line: '" + line + "'");
        return gedcomTag;
    }

    private String lookForEscapeSequence(String restOfLine) {
        Pattern escapePattern = Pattern.compile(ESCAPE_SEQUENCE_REGEX);
        Matcher escapeMatcher = escapePattern.matcher(restOfLine);
        if (escapeMatcher.find()) {
            escapeSequence = escapeMatcher.group();
            escapeSequence = escapeSequence.substring(2, escapeSequence.length() - 3);
            restOfLine = restOfLine.replaceAll(escapePattern.pattern(), "");
        }
        return restOfLine;
    }

    private int readLevel(Scanner scanner) {
        int lvl;
        try {
            lvl = scanner.nextInt();
        } catch (InputMismatchException nfe) {
            throw new IllegalLevelNumber("Level must be a number.");
        }
        return lvl;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(String.valueOf(level));
        if (xref != null && !isXrefPointer) stringBuilder.append(" @").append(xref).append("@");
        stringBuilder.append(" ").append(tag);
        if (StringUtils.isNotBlank(item)) stringBuilder.append(" ").append(item);
        else if (xref != null && isXrefPointer) stringBuilder.append(" @").append(xref).append("@");
        return stringBuilder.toString();
    }

    public boolean hasItem() {
        return StringUtils.isNotBlank(item);
    }
}
