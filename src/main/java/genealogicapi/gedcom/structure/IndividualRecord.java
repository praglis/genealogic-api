package genealogicapi.gedcom.structure;

import genealogicapi.domain.event.EventType;
import genealogicapi.domain.person.Person;
import genealogicapi.domain.person.Sex;
import genealogicapi.domain.place.PlaceDto;
import genealogicapi.domain.tree.TreeDto;
import genealogicapi.gedcom.SupportedGedcomVersion;
import genealogicapi.gedcom.importing.exception.EmptyRecordException;
import genealogicapi.gedcom.importing.exception.MissingPointerException;
import genealogicapi.gedcom.importing.exception.MissingTagException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IndividualRecord extends TopLevelRecord implements Person, ContainingEvents {

    private boolean isEmpty = true;

    private String namePrefix;
    private String firstName;
    private String middleName;
    private String nickname;
    private String surnamePrefix;
    private String lastName;
    private String maidenName;
    private String nameSuffix;

    private Sex sex;
    private List<EventRecord> individualEvents;
    private String occupation;
    private PlaceDto residency;
    private String bio;
    private Map<String, GedcomTag> familyPointers = new HashMap<>();

    private TreeDto tree;
    private Long id;
    private Boolean isNameCustom = false;

    public IndividualRecord(String firstStringLine, Scanner gs, SupportedGedcomVersion version) {
        super(firstStringLine, gs, version);

        GedcomLine firstParsedLine = lines.get(0);

        if (firstParsedLine.getTag() != GedcomTag.INDI) {
            throw new MissingTagException(GedcomTag.INDI, "Each individual record must begin with INDI tag.");
        }

        if (firstParsedLine.getXref() == null) throw new MissingPointerException("individual");
        xrefId = firstParsedLine.getXref();

        readNameRecord(findRecord(GedcomTag.NAME));
        sex = Sex.valueOf(findItemByTagOrElseDefault(GedcomTag.SEX, Sex.U.name()));
        occupation = findItemByTagOrElseDefault(GedcomTag.OCCU, null);

        GedcomRecord residencyRecord = findRecord(GedcomTag.RESI);
        residency = readPlaceAndAddressRecords(new GedcomRecord(
                residencyRecord.findRecord(GedcomTag.PLAC),
                residencyRecord.findRecord(GedcomTag.ADDR),
                version));

        if (hasTag(GedcomTag.NOTE)) {
            LongTextRecord note = new LongTextRecord(findRecord(GedcomTag.NOTE));
            bio = note.getLongText();
        }

        individualEvents = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            GedcomLine gedcomLine = lines.get(i);
            GedcomTag lineTag = gedcomLine.getTag();

            if (EventType.contains(lineTag) && EventType.valueOf(lineTag.name()).isIndividual()) {
                individualEvents.add(new EventRecord(getRecord(i), version));
            }
        }
        isEmpty = false;
    }

    public IndividualRecord(String xrefId) {
        super();
        this.xrefId = xrefId;
        isEmpty = false;
    }

    public static IndividualRecord empty() {
        IndividualRecord emptyRecord = new IndividualRecord();
        emptyRecord.isEmpty = true;
        return emptyRecord;
    }

    @Override
    public boolean isPresent() {
        return !isEmpty;
    }

    @Override
    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public void addEvent(EventRecord eventRecord) {
        individualEvents.add(eventRecord);
    }

    private void readNameRecord(GedcomRecord gedcomRecord) {
        namePrefix = findItemByTagOrElseDefault(GedcomTag.NPFX, gedcomRecord.getLines(), null);
        nickname = findItemByTagOrElseDefault(GedcomTag.NICK, gedcomRecord.getLines(), null);
        surnamePrefix = findItemByTagOrElseDefault(GedcomTag.SPFX, gedcomRecord.getLines(), null);
        nameSuffix = findItemByTagOrElseDefault(GedcomTag.NSFX, gedcomRecord.getLines(), null);

        String surnames;
        if (gedcomRecord.hasTagWithItem(GedcomTag.SURN)) {
            surnames = findItemByTagOrElseDefault(GedcomTag.SURN, gedcomRecord.getLines(), null);
        } else {
            surnames = findItemByTagOrElseEmptyString(GedcomTag.NAME, gedcomRecord.getLines())
                    .replaceAll("^([^/]*/)", "")
                    .replaceAll("/[^/]*$", "");
        }

        if (StringUtils.isBlank(surnames)) {
            throw new EmptyRecordException(GedcomTag.NAME);
        }

        String[] splitSurnames = splitStringIntoTwo(surnames);
        lastName = splitSurnames[0];
        maidenName = splitSurnames[1];

        Optional<GedcomLine> givenRecordOptional = findGedcomLine(GedcomTag.GIVN, gedcomRecord.getLines());
        if (givenRecordOptional.isPresent() && givenRecordOptional.get().hasItem()) {
            GedcomLine givenRecord = givenRecordOptional.get();
            String[] splitGivenNames = splitStringIntoTwo(givenRecord.getItem());
            firstName = splitGivenNames[0];
            middleName = splitGivenNames[1];
        }
    }

    private String[] splitStringIntoTwo(String surnames) {
        String[] names = surnames.split(" ");
        String firstTarget = "";
        String secondTarget = null;
        if (names.length > 1) {
            secondTarget = "";
            for (int i = 0; i < names.length; i++) {
                if (i < Math.ceil(names.length / 2.0)) firstTarget = firstTarget.concat(names[i] + " ");
                else secondTarget = secondTarget.concat(names[i] + " ");
            }
            firstTarget = firstTarget.strip();
            secondTarget = secondTarget.strip();
        } else {
            firstTarget = names[0];
        }
        return new String[]{firstTarget, secondTarget};
    }

    @Override
    public void addDataToLines(int level) {
        lines.add(new GedcomLine(level, xrefId, GedcomTag.INDI));

        addNameRecordToLines(level + 1);

        lines.add(new GedcomLine(level + 1, GedcomTag.SEX, sex.name()));

        for (EventRecord individualEvent : individualEvents) {
            individualEvent.setVersion(version);
            individualEvent.addDataToLines(level + 1);
            lines.addAll(individualEvent.getLines());
        }

        addToLinesIfNotNullOrBlank(occupation, level + 1, GedcomTag.OCCU);
        addResidencyRecordToLines(level + 1);

        for (Map.Entry<String, GedcomTag> entry : familyPointers.entrySet()) {
            lines.add(new GedcomLine(level + 1, entry.getKey(), entry.getValue(), true));
        }

        if (StringUtils.isNotBlank(bio)) {
            LongTextRecord longTextRecord = new LongTextRecord(level + 1, GedcomTag.NOTE, bio, version);
            lines.addAll(longTextRecord.getLines());
        }
    }

    public void addFamilyPointer(String xrefId, GedcomTag roleInFamily) {
        familyPointers.put(xrefId, roleInFamily);
    }

    private void addResidencyRecordToLines(int level) {
        if (residency != null) {
            lines.add(new GedcomLine(level, GedcomTag.RESI));
            addPlaceAndAddressToGedcomLines(residency, level + 1);
        }
    }

    private void addNameRecordToLines(int level) {
        lines.add(new GedcomLine(level, GedcomTag.NAME, getFullName()));

        addToLinesIfNotNullOrBlank(namePrefix, level + 1, GedcomTag.NPFX);

        String names = "";
        if (firstName != null) names = firstName;
        if (middleName != null) names = names.concat(" " + middleName);
        addToLinesIfNotNullOrBlank(names, level + 1, GedcomTag.GIVN);

        addToLinesIfNotNullOrBlank(nickname, level + 1, GedcomTag.NICK);
        addToLinesIfNotNullOrBlank(surnamePrefix, level + 1, GedcomTag.SPFX);
        String surname = lastName;
        if (maidenName != null) {
            surname = surname.concat(" " + maidenName);
        }
        lines.add(new GedcomLine(level + 1, GedcomTag.SURN, surname));
        addToLinesIfNotNullOrBlank(nameSuffix, level + 1, GedcomTag.NSFX);
    }

    @Override
    public String getFullName() {
        return StringUtils.chop(String.format("%s%s%s%s%s%s%s%s",
                namePrefix != null ? namePrefix + " " : "",
                firstName != null ? firstName + " " : "",
                middleName != null ? middleName + " " : "",
                nickname != null ? nickname + " " : "",
                surnamePrefix != null ? surnamePrefix + " " : "",
                lastName != null ? "/" + lastName + "/ " : "",
                maidenName != null ? maidenName + " " : "",
                nameSuffix != null ? nameSuffix + " " : ""));
    }


}
