package genealogicapi.gedcom.structure;

import genealogicapi.domain.place.PlaceDto;
import genealogicapi.gedcom.SupportedGedcomVersion;
import genealogicapi.gedcom.importing.exception.IncorrectGedcomRecord;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public class GedcomRecord {
    private static final int MAX_ADDRESS_LINE_LENGTH = 60;
    @Setter
    protected SupportedGedcomVersion version;

    protected List<GedcomLine> lines;

    public GedcomRecord(SupportedGedcomVersion version, GedcomRecord gedcomRecord) {
        this.version = version;
        this.lines = gedcomRecord.getLines();
    }

    public GedcomRecord(GedcomRecord struct1, GedcomRecord struct2, SupportedGedcomVersion version) {
        this.version = version;
        lines = new LinkedList<>();
        lines.addAll(struct1.getLines());
        lines.addAll(struct2.getLines());
    }

    protected GedcomRecord() {
        lines = new LinkedList<>();
    }

    protected GedcomRecord(SupportedGedcomVersion version) {
        this.version = version;
        lines = new LinkedList<>();
    }

    public static GedcomRecord empty() {
        return new GedcomRecord();
    }

    public boolean isEmpty() {
        return this.lines.isEmpty();
    }

    public boolean isPresent() {
        return !this.lines.isEmpty();
    }

    protected Optional<GedcomLine> findGedcomLine(GedcomTag tag, List<GedcomLine> srcLines) {
        List<GedcomLine> foundLines = findAllGedcomLines(tag, srcLines);
        return foundLines.isEmpty() ? Optional.empty() : Optional.of(foundLines.get(0));
    }

    protected Optional<GedcomLine> findGedcomLine(GedcomTag tag) {
        return findGedcomLine(tag, lines);
    }

    protected List<GedcomLine> findAllGedcomLines(GedcomTag tag, List<GedcomLine> lines) {
        return lines.stream()
                .filter(gedcomLine -> gedcomLine.getTag() == tag)
                .collect(Collectors.toList());
    }

    protected String findItemByTagOrElseDefault(@NonNull final GedcomTag tag, final String defaultItem) {
        return findItemByTagOrElseDefault(tag, lines, defaultItem);
    }

    protected String findItemByTagOrElseEmptyString(@NonNull final GedcomTag tag, @NonNull final List<GedcomLine> lines) {
        return findItemByTagOrElseDefault(tag, lines, "");
    }

    protected String findItemByTagOrElseDefault(@NonNull final GedcomTag tag, @NonNull final List<GedcomLine> linesToSearch,
                                                final String defaultItem) {
        Optional<GedcomLine> gedcomLineOptional = findGedcomLine(tag, linesToSearch);
        return gedcomLineOptional.isPresent() && gedcomLineOptional.get().hasItem()
                ? gedcomLineOptional.get().getItem() : defaultItem;
    }

    protected boolean hasTag(GedcomTag tag) {
        return lines.stream()
                .anyMatch(gedcomLine -> gedcomLine.getTag() == tag);
    }

    protected boolean hasTagWithItem(GedcomTag tag) {
        return lines.stream()
                .anyMatch(gedcomLine -> gedcomLine.getTag() == tag
                        && gedcomLine.hasItem());
    }

    protected PlaceDto readPlaceAndAddressRecords(GedcomRecord gedcomRecord) {
        if (gedcomRecord.isEmpty()) return null;
        PlaceDto placeDto = new PlaceDto();

        placeDto.setName(readPlaceRecord(gedcomRecord));

        GedcomRecord addressRecord = gedcomRecord.findRecord(GedcomTag.ADDR);
        if (!addressRecord.lines.isEmpty()) {
            String dtoAddress = readAddress(addressRecord);

            if (StringUtils.isNotBlank(dtoAddress)) {
                placeDto.setAddress(dtoAddress);
            }
            placeDto.setCity(gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.CITY, gedcomRecord.getLines()));
            placeDto.setState(gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.STAE, gedcomRecord.getLines()));
            placeDto.setPostalCode(gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.POST, gedcomRecord.getLines()));
            placeDto.setCountry(gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.CTRY, gedcomRecord.getLines()));
        }
        return placeDto;
    }

    private String readAddress(GedcomRecord addressRecord) {
        String dtoAddress = "";
        for (GedcomLine addressLine : addressRecord.lines) {
            if ((addressLine.getTag().isAddressTag() || addressLine.getTag() == GedcomTag.CONT)
                    && addressLine.hasItem()) {
                dtoAddress = dtoAddress.concat(System.lineSeparator() + addressLine.getItem());
            } else if (addressLine.getTag() == GedcomTag.CONC) {
                dtoAddress = dtoAddress.concat(addressLine.getItem());
            }
        }
        return dtoAddress.strip();
    }

    private String readPlaceRecord(GedcomRecord gedcomRecord) {
        String placeName;
        if (gedcomRecord.hasTagWithItem(GedcomTag.PLAC))
            placeName = gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.PLAC, gedcomRecord.getLines());
        else if (version == SupportedGedcomVersion.V551 && gedcomRecord.hasTagWithItem(GedcomTag.ADDR))
            placeName = gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.ADDR, gedcomRecord.getLines());
        else if (gedcomRecord.hasTagWithItem(GedcomTag.ADR1))
            placeName = gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.ADR1, gedcomRecord.getLines());
        else if (gedcomRecord.hasTagWithItem(GedcomTag.ADR2))
            placeName = gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.ADR2, gedcomRecord.getLines());
        else if (gedcomRecord.hasTagWithItem(GedcomTag.ADR3))
            placeName = gedcomRecord.findItemByTagOrElseEmptyString(GedcomTag.ADR3, gedcomRecord.getLines());
        else throw new IncorrectGedcomRecord(GedcomTag.PLAC, "missing place name.");
        return placeName;
    }

    protected GedcomRecord findRecord(GedcomTag tag) {
        Optional<GedcomLine> firstLineOptional = findGedcomLine(tag);
        return firstLineOptional.map(this::buildGedcomRecord).orElseGet(GedcomRecord::empty);
    }

    private GedcomRecord buildGedcomRecord(GedcomLine first) {
        if (first == null) return new GedcomRecord(version);
        int index = lines.indexOf(first);
        List<GedcomLine> gedcomRecord = new ArrayList<>(List.of(first));

        while (++index < lines.size() && lines.get(index).getLevel() > first.getLevel()) {
            gedcomRecord.add(lines.get(index));
        }
        return new GedcomRecord(version, gedcomRecord);
    }

    protected GedcomRecord getRecord(int lineNumber) {
        GedcomLine first = lines.get(lineNumber);
        return buildGedcomRecord(first);
    }

    protected String[] splitAddress(String address) {
        List<String> addressLines = Arrays.asList(address.split("\\r?\\n"));
        for (int i = 0; i < addressLines.size(); i++) {
            String line = addressLines.get(i);
            if (line.length() > MAX_ADDRESS_LINE_LENGTH) {
                addressLines.set(i, line.substring(0, 60));
                addressLines.add(i + 1, line.substring(60));
            }
        }
        return addressLines.toArray(new String[0]);
    }

    protected void addToLinesIfNotNullOrBlank(String item, int level, GedcomTag tag) {
        if (StringUtils.isNotBlank(item)) lines.add(new GedcomLine(level, tag, item));
    }

    protected void addPlaceAndAddressToGedcomLines(PlaceDto placeDto, int level) {
        if (placeDto == null) return;

        lines.add(new GedcomLine(level, GedcomTag.PLAC, placeDto.getName()));
        if (placeDto.hasAnyAddress()) {

            if (placeDto.getAddress() != null) {
                String[] addressLines = splitAddress(placeDto.getAddress());
                addAddressLinesToRecordLines(level, addressLines);
            } else {
                lines.add(new GedcomLine(level, GedcomTag.ADDR));
            }

            addToLinesIfNotNullOrBlank(placeDto.getCity(), 1 + level, GedcomTag.CITY);
            addToLinesIfNotNullOrBlank(placeDto.getState(), 1 + level, GedcomTag.STAE);
            addToLinesIfNotNullOrBlank(placeDto.getPostalCode(), 1 + level, GedcomTag.POST);
            addToLinesIfNotNullOrBlank(placeDto.getCountry(), 1 + level, GedcomTag.CTRY);
        }
    }

    private void addAddressLinesToRecordLines(int level, String[] addressLines) {
        List<GedcomTag> addressLinesTags = new ArrayList<>(List.of(GedcomTag.ADR1, GedcomTag.ADR2, GedcomTag.ADR3));
        int lineNumber;
        if (version == SupportedGedcomVersion.V555) {
            lines.add(new GedcomLine(level, GedcomTag.ADDR));
            lineNumber = 0;
        } else {
            lines.add(new GedcomLine(level, GedcomTag.ADDR, addressLines[0]));
            lineNumber = 1;
        }

        for (int tagNumber = 0;
             lineNumber < addressLines.length && tagNumber < addressLinesTags.size();
             lineNumber++, tagNumber++) {
            addToLinesIfNotNullOrBlank(addressLines[lineNumber], 1 + level, addressLinesTags.get(tagNumber));
        }
    }

    public void addDataToLines(int level) {
        // todo #38 refactor
    }
}
