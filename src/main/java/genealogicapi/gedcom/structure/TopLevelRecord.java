package genealogicapi.gedcom.structure;

import genealogicapi.gedcom.SupportedGedcomVersion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
@Slf4j
public class TopLevelRecord extends GedcomRecord {

    @Setter
    protected String xrefId;
    protected String nextRecordLine;

    public TopLevelRecord(String firstLine, Scanner gs, SupportedGedcomVersion version) {
        super(version);
        lines = new LinkedList<>();
        lines.add(new GedcomLine(firstLine));
        String nextLine;

        while (gs.hasNext()) {
            nextLine = gs.nextLine();
            if (nextLine.startsWith("0")) {
                nextRecordLine = nextLine;
                return;
            }
            lines.add(new GedcomLine(nextLine));
        }
        log.debug("LOADED TOP-LEVEL RECORD");
    }

    public TopLevelRecord(SupportedGedcomVersion version) {
        super(version);
    }

    protected Optional<String> findXref(GedcomTag tag) {
        Optional<GedcomLine> gedcomLineOptional = findGedcomLine(tag);
        return gedcomLineOptional.map(GedcomLine::getXref);
    }

    protected List<String> findAllXrefs(GedcomTag tag) {
        return findAllGedcomLines(tag, lines).stream().map(GedcomLine::getXref).collect(Collectors.toList());
    }
}


