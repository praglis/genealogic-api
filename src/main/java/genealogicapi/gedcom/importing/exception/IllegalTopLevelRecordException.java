package genealogicapi.gedcom.importing.exception;

import genealogicapi.gedcom.structure.GedcomTag;

public class IllegalTopLevelRecordException extends GedcomImportException {
    public IllegalTopLevelRecordException(GedcomTag gedcomTag) {
        super(String.format("illegal top level record: %s.", gedcomTag.name()));
    }

    public IllegalTopLevelRecordException(GedcomTag gedcomTag, String additionalMessage) {
        super(String.format("illegal top level record: %s. %s", gedcomTag.name(), additionalMessage));
    }
}
