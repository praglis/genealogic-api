package genealogicapi.gedcom.importing.exception;

public class MissingPointerException extends GedcomImportException {
    public MissingPointerException(String pointerType) {
        super("Missing pointer for " + pointerType + " record");
    }
}
