package genealogicapi.gedcom.importing.exception;

import genealogicapi.gedcom.structure.GedcomTag;

public class MissingTagException extends GedcomImportException {
    public MissingTagException(GedcomTag tag) {
        super(String.format("Missing '%s' tag.", tag.name()));
    }

    public MissingTagException(GedcomTag tag, String additionalMessage) {
        super(String.format("Missing '%s' tag. %s", tag.name(), additionalMessage));
    }

    public MissingTagException(String message) {
        super(message);
    }
}
