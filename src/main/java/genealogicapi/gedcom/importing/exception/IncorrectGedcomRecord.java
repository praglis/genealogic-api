package genealogicapi.gedcom.importing.exception;

import genealogicapi.gedcom.structure.GedcomTag;

public class IncorrectGedcomRecord extends GedcomImportException {
    public IncorrectGedcomRecord(GedcomTag recordMainTag, String message) {
        super(String.format("Incorrect %s record: %s", recordMainTag, message));
    }
}
