package genealogicapi.gedcom.importing.exception;

import genealogicapi.gedcom.structure.GedcomTag;

public class EmptyRecordException extends IncorrectGedcomRecord {
    public EmptyRecordException(GedcomTag recordMainTag) {
        super(recordMainTag, "this record must contain any value.");
    }
}
