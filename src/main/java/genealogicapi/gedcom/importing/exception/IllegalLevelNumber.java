package genealogicapi.gedcom.importing.exception;

public class IllegalLevelNumber extends GedcomImportException {
    public IllegalLevelNumber(String message) {
        super(message);
    }
}
