package genealogicapi.gedcom.importing.exception;

public class GedcomImportException extends RuntimeException {
    public GedcomImportException(String message) {
        super("GEDCOM import exception: " + message);
    }
}
