package genealogicapi.gedcom.importing;

import genealogicapi.domain.tree.TreeDto;
import genealogicapi.domain.user.UserDto;
import genealogicapi.gedcom.GedcomServiceImpl;
import genealogicapi.gedcom.SupportedGedcomVersion;
import genealogicapi.gedcom.importing.exception.GedcomImportException;
import genealogicapi.gedcom.importing.exception.IllegalLevelNumber;
import genealogicapi.gedcom.importing.exception.IllegalTopLevelRecordException;
import genealogicapi.gedcom.importing.exception.MissingTagException;
import genealogicapi.gedcom.structure.*;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Data
public class GedcomReader {

    private static final String CHARSET_RECORD_REGEX = "CHAR .*\\w";
    private final UserDto owner;
    private final MultipartFile file;
    private final List<IndividualRecord> indiRecords = new ArrayList<>();
    private final List<FamilyRecord> famRecords = new ArrayList<>();
    private final List<TopLevelRecord> otherRecords = new ArrayList<>();
    private HeaderRecord header;
    private SupportedGedcomVersion version;

    private static String removeUTF8BOM(String s) {
        if (s.startsWith(GedcomServiceImpl.UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    public List<FamilyRecord> fillFamiliesWithIndividuals() {
        for (int i = 0; i < famRecords.size(); i++) {
            FamilyRecord familyRecord = famRecords.get(i);
            familyRecord.setHusband(findRecord(familyRecord.getHusband().getXrefId(), indiRecords));
            familyRecord.setWife(findRecord(familyRecord.getWife().getXrefId(), indiRecords));
            familyRecord.setChildren(
                    familyRecord.getChildren().stream()
                            .map(individualRecord -> findRecord(individualRecord.getXrefId(), indiRecords))
                            .collect(Collectors.toList())
            );
            famRecords.set(i, familyRecord);
        }
        return famRecords;
    }

    private IndividualRecord findRecord(String pointer, List<IndividualRecord> indiRecords) {
        if (pointer == null) return null;
        return indiRecords.stream()
                .filter(individualRecord -> individualRecord.getXrefId().equals(pointer))
                .findFirst()
                .orElseThrow(() -> new GedcomImportException("Encountered a cross reference pointer (@" + pointer
                        + "@) which is pointing to non-existing individual."));
    }

    private String parseFileToString(MultipartFile file, Charset charset) {
        try {
            return new String(file.getBytes(), charset);
        } catch (IOException e) {
            e.printStackTrace();
            throw new GedcomImportException(e.getMessage());
        }
    }

    private String preProcessGedcom(MultipartFile file) {
        String gedcom = parseFileToString(file, StandardCharsets.UTF_8);

        Pattern pattern = Pattern.compile(CHARSET_RECORD_REGEX);
        Matcher matcher = pattern.matcher(gedcom);

        if (matcher.find()) {
            String charsetLine = gedcom.substring(matcher.start(), matcher.end());
            if (charsetLine.contains("UNICODE")) gedcom = parseFileToString(file, StandardCharsets.UTF_16);
            else if (charsetLine.contains("ASCII")) gedcom = parseFileToString(file, StandardCharsets.US_ASCII);
        } else throw new MissingTagException(GedcomTag.CHAR);

        gedcom = removeUTF8BOM(gedcom);
        return gedcom;
    }

    private void readRecords(String gedcom) {
        Scanner scanner = new Scanner(gedcom);
        String nextLine = scanner.nextLine();
        boolean encounteredTrailer = false;
        boolean encounteredSubmitter = false;

        while (scanner.hasNext()) {
            if (!nextLine.startsWith("0")) throw new IllegalLevelNumber("GEDCOM file must start with 0 level.");

            GedcomLine nextGedcomLine = new GedcomLine(nextLine);
            switch (nextGedcomLine.getTag()) { //todo refactor this switch #67
                case HEAD:
                    header = new HeaderRecord(nextLine, scanner);
                    this.version = header.getVersion();
                    nextLine = header.getNextRecordLine();
                    break;
                case INDI:
                    indiRecords.add(new IndividualRecord(nextLine, scanner, version));
                    nextLine = indiRecords.get(indiRecords.size() - 1).getNextRecordLine();
                    break;
                case FAM:
                    famRecords.add(new FamilyRecord(nextLine, scanner, version));
                    nextLine = famRecords.get(famRecords.size() - 1).getNextRecordLine();
                    break;
                case TRLR:
                    encounteredTrailer = true;
                    break;
                case SUBM:
                    if (!encounteredSubmitter) encounteredSubmitter = true;
                    else {
                        throw new IllegalTopLevelRecordException(GedcomTag.SUBM, "Only one submitter record per GEDCOM file allowed.");
                    }
                case SOUR:
                case OBJE:
                case NOTE:
                case REPO:
                    TopLevelRecord submitterRecord = new TopLevelRecord(nextLine, scanner, version);
                    nextLine = submitterRecord.getNextRecordLine();
                    break;
                default:
                    throw new IllegalTopLevelRecordException(nextGedcomLine.getTag());
            }
            if (new GedcomLine(nextLine).getTag() == GedcomTag.TRLR) {
                encounteredTrailer = true;
            }
        }
        if (!encounteredTrailer)
            throw new MissingTagException(GedcomTag.TRLR, "Every GEDCOM file must end with trailer record.");
    }

    public void readGedcom() {
        String gedcom = preProcessGedcom(file);
        readRecords(gedcom);
    }

    public TreeDto prepareTreeToSave() {
        TreeDto tree = new TreeDto();
        tree.setName(file.getOriginalFilename());
        tree.setOwner(owner);
        return tree;
    }
}
