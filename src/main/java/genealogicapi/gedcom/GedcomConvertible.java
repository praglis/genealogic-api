package genealogicapi.gedcom;

import genealogicapi.domain.abstraction.AbstractDto;
import genealogicapi.domain.abstraction.AbstractService;
import genealogicapi.gedcom.structure.GedcomRecord;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface GedcomConvertible<R extends GedcomRecord, D extends AbstractDto> extends AbstractService<D> {
    @PreAuthorize("hasRole('USER')")
    R create(R gedcomRecord);

    @PreAuthorize("hasRole('USER')")
    List<R> getAllRecordsByTree(Long treeId);
}
