package genealogicapi.gedcom;

public enum SupportedGedcomVersion {
    V551("5.5.1"), V555("5.5.5");

    private final String version;

    SupportedGedcomVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
