package genealogicapi.gedcom;

import genealogicapi.commons.file.NotEncodedFileDto;
import genealogicapi.domain.event.EventService;
import genealogicapi.domain.event.EventType;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.family.FamilyService;
import genealogicapi.domain.person.PersonService;
import genealogicapi.domain.tree.TreeDto;
import genealogicapi.domain.tree.TreeService;
import genealogicapi.domain.user.UserDto;
import genealogicapi.domain.user.UserService;
import genealogicapi.gedcom.exporting.GedcomWriter;
import genealogicapi.gedcom.exporting.exception.GedcomExportException;
import genealogicapi.gedcom.importing.GedcomReader;
import genealogicapi.gedcom.structure.EventRecord;
import genealogicapi.gedcom.structure.FamilyRecord;
import genealogicapi.gedcom.structure.IndividualRecord;
import genealogicapi.gedcom.structure.TopLevelRecord;
import genealogicapi.security.SecurityService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;


@Service
public class GedcomServiceImpl implements GedcomService {
    public static final String UTF8_BOM = "\uFEFF";

    private final SecurityService securityService;
    private final UserService userService;
    private final TreeService treeService;
    private final PersonService personService;
    private final FamilyService familyService;
    private final EventService eventService;

    @Value("${genea-logic.api.name}")
    private String appName;

    public GedcomServiceImpl(SecurityService securityService,
                             UserService userService,
                             TreeService treeService,
                             PersonService personService,
                             FamilyService familyService,
                             EventService eventService) {
        this.securityService = securityService;
        this.userService = userService;
        this.treeService = treeService;
        this.personService = personService;
        this.familyService = familyService;
        this.eventService = eventService;
    }

    private void createFamilyWithEvents(TopLevelRecord topLevelRecord) {
        FamilyRecord familyRecord = familyService.create((FamilyRecord) topLevelRecord);
        familyRecord.setXrefId(topLevelRecord.getXrefId());

        UserDto owner = userService.getByUsername(securityService.findLoggedInUsername());
        if (!((FamilyRecord) topLevelRecord).getFamilyEvents().isEmpty()) {
            for (EventRecord es : ((FamilyRecord) topLevelRecord).getFamilyEvents()) {
                es.setOwner(owner);
                eventService.createWithParticipants(es, familyRecord.getSpouses());
            }
        }
    }

    private void createIndividualWithEvents(TreeDto createdTree, List<IndividualRecord> records, int i,
                                            IndividualRecord recordFromGedcom) {
        recordFromGedcom.setTree(createdTree);
        IndividualRecord createdRecord = personService.create(recordFromGedcom);
        createdRecord.setXrefId(recordFromGedcom.getXrefId());

        UserDto owner = userService.getByUsername(securityService.findLoggedInUsername());
        if (!recordFromGedcom.getIndividualEvents().isEmpty()) {
            for (EventRecord es : recordFromGedcom.getIndividualEvents()) {
                es.setOwner(owner);
                eventService.createWithParticipants(es, Collections.singleton(createdRecord));
            }
        }

        if (recordFromGedcom.getIndividualEvents().stream().noneMatch(er -> er.getType() == EventType.BIRT)) {
            EventDto defaultBirthEvent = new EventDto();
            defaultBirthEvent.setOwner(owner);
            defaultBirthEvent.setType(EventType.BIRT);
            eventService.createWithParticipants(defaultBirthEvent, Collections.singleton(createdRecord));
        }
        records.set(i, createdRecord);
    }

    @Override
    @Transactional
    public Long importGedcom(MultipartFile file) {

        GedcomReader gedcomReader = new GedcomReader(
                userService.getByUsername(securityService.findLoggedInUsername()),
                file
        );

        gedcomReader.readGedcom();

        TreeDto createdTree = treeService.create(gedcomReader.prepareTreeToSave()); //todo #69

        List<IndividualRecord> indiRecords = gedcomReader.getIndiRecords();

        for (int i = 0; i < indiRecords.size(); i++) {
            IndividualRecord individualRecord = indiRecords.get(i);
            createIndividualWithEvents(createdTree, indiRecords, i, individualRecord);
        }

        List<FamilyRecord> famRecords = gedcomReader.fillFamiliesWithIndividuals();

        for (FamilyRecord familyRecord : famRecords) {
            createFamilyWithEvents(familyRecord);
        }

        return createdTree.getId();
    }

    @Override
    public NotEncodedFileDto exportGedcom(Long treeId, SupportedGedcomVersion version) {
        TreeDto tree = treeService.getById(treeId);
        String fileName = (tree.getName() + ".ged").replace(",", "_");
        ByteArrayOutputStream baos;

        try {
            GedcomWriter gedcomWriter = new GedcomWriter(
                    version,
                    securityService.findLoggedInUser(),
                    personService.getAllRecordsByTree(treeId),
                    eventService.getAllRecordsByTree(treeId),
                    familyService.getAllRecordsByTree(treeId),
                    appName);

            baos = new ByteArrayOutputStream();
            gedcomWriter.generateGedcom(baos);

        } catch (IOException ioException) {
            throw new GedcomExportException(ioException.getMessage());
        }
        return new NotEncodedFileDto(fileName, baos.toByteArray());
    }

}
