package genealogicapi.gedcom;

import genealogicapi.domain.abstraction.AbstractDto;
import genealogicapi.domain.abstraction.AbstractEntity;
import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.gedcom.structure.GedcomRecord;

public interface GedcomRecordMapper<D extends AbstractDto, E extends AbstractEntity, R extends GedcomRecord>
        extends AbstractMapper<D, E> {

    D toDto(R gedcomRecord);

    R toRecord(D dto);
}
