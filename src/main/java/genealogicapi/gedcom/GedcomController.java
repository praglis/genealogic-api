package genealogicapi.gedcom;

import genealogicapi.commons.file.NotEncodedFileDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

@Tag(name = "GEDCOM", description = "Allows import and export of GEDCOM files in 5.5.1 and 5.5.5 versions.")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequiredArgsConstructor
@RequestMapping("/gedcom")
public class GedcomController {
    private final GedcomService gedcomService;

    @Operation(summary = "Allows importing GEDCOM file.")
    @ApiResponse(responseCode = "400", description = "Returned when uploaded file is not correct GEDCOM file.")
    @PostMapping
    public ResponseEntity<Void> importGedcom(@RequestParam("file") MultipartFile file) {
        Long ownerId = gedcomService.importGedcom(file);
        return ResponseEntity.created(URI.create("/trees/owner/" + ownerId)).build();
    }

    @Operation(summary = "Generates GEDCOM file in requested version from tree of requested ID.")
    @GetMapping("/{version}/tree/{id}")
    public ResponseEntity<byte[]> exportGedcom(@PathVariable SupportedGedcomVersion version, @PathVariable Long id) {
        NotEncodedFileDto gedcomFile = gedcomService.exportGedcom(id, version);

        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentLength(gedcomFile.getFile().length);
        respHeaders.setContentType(new MediaType("text", "plain"));
        respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + gedcomFile.getFileName());
        return new ResponseEntity<>(gedcomFile.getFile(), respHeaders, HttpStatus.OK);
    }
}
