package genealogicapi.gedcom;

import genealogicapi.commons.file.NotEncodedFileDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;

public interface GedcomService {
    @PreAuthorize("hasRole('USER')")
    Long importGedcom(MultipartFile file);

    @PreAuthorize("hasRole('USER')")
    NotEncodedFileDto exportGedcom(Long treeId, SupportedGedcomVersion version);
}
