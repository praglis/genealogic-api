package genealogicapi.domain.event.participant;

import genealogicapi.commons.validation.ValidId;
import genealogicapi.domain.abstraction.AbstractDto;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.event.role.EventRoleDto;
import genealogicapi.domain.person.PersonDto;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class EventParticipantDto extends AbstractDto {
    @ValidId
    private EventDto event;

    @ValidId
    private PersonDto person;

    private EventRoleDto role;
}
