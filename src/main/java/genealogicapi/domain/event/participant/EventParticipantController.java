package genealogicapi.domain.event.participant;

import genealogicapi.domain.abstraction.AbstractController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Event participant", description = "Operations related to event participants.")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequestMapping("/event-participants")
public class EventParticipantController extends AbstractController<EventParticipantDto> {

    private final EventParticipantService eventParticipantService;

    public EventParticipantController(EventParticipantService eventParticipantService) {
        super(eventParticipantService);
        this.eventParticipantService = eventParticipantService;
    }

    //    @Hidden
    @Operation(summary = "Returns page containing participants of the requested event.")
    @GetMapping("/event/{id}")
    public Page<EventParticipantDto> getEventParticipantsByEvent(@PathVariable Long id, Pageable pageable) {
        return eventParticipantService.getByEvent(id, pageable);
    }

    //    @Hidden
    @Operation(summary = "Returns page containing event participant objects of the requested person.")
    @GetMapping("/person/{id}")
    public Page<EventParticipantDto> getEventParticipantsByPerson(@PathVariable Long id, Pageable pageable) {
        return eventParticipantService.getByPerson(id, pageable);
    }
}
