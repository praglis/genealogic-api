package genealogicapi.domain.event.participant;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractServiceImpl;
import genealogicapi.domain.event.Event;
import genealogicapi.domain.event.EventMapper;
import genealogicapi.domain.event.EventService;
import genealogicapi.domain.event.EventType;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.event.role.EventRoleDto;
import genealogicapi.domain.event.role.EventRoleMapper;
import genealogicapi.domain.event.role.EventRoleService;
import genealogicapi.domain.person.PersonDto;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.person.PersonService;
import genealogicapi.domain.person.mapping.PersonMapper;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventParticipantServiceImpl extends AbstractServiceImpl<EventParticipantDto, EventParticipant> implements EventParticipantService {

    private final EventParticipantRepository eventParticipantRepository;
    private final EventParticipantMapper eventParticipantMapper;
    private final EventService eventService;
    private final EventMapper eventMapper;
    private final PersonService personService;
    private final PersonMapper personMapper;
    private final EventRoleService eventRoleService;
    private final EventRoleMapper eventRoleMapper;

    public EventParticipantServiceImpl(EventParticipantRepository eventParticipantRepository,//todo #66
                                       EventParticipantMapper eventParticipantMapper,
                                       EventService eventService,
                                       EventMapper eventMapper,
                                       PersonService personService,
                                       PersonMapper personMapper,
                                       EventRoleService eventRoleService,
                                       EventRoleMapper eventRoleMapper) {
        super(eventParticipantRepository, eventParticipantMapper);
        this.eventParticipantRepository = eventParticipantRepository;
        this.eventParticipantMapper = eventParticipantMapper;
        this.eventService = eventService;
        this.eventMapper = eventMapper;
        this.personService = personService;
        this.personMapper = personMapper;
        this.eventRoleService = eventRoleService;
        this.eventRoleMapper = eventRoleMapper;
    }


    @Override
    @Transactional(readOnly = true)
    public Page<EventParticipantDto> getByEvent(Long eventId, Pageable pageable) {
        EventDto eventDto = eventService.getById(eventId);
        return eventParticipantRepository.findByEvent(eventMapper.toEntity(eventDto), pageable)
                .map(eventParticipantMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventParticipantDto> getByPerson(Long personId, Pageable pageable) {
        PersonDto personDto = personService.getById(personId);
        return eventParticipantRepository.findByPerson(personMapper.toEntity(personDto), pageable)
                .map(eventParticipantMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public EventParticipantDto getById(Long id) {
        EventParticipantDto eventParticipantDto = super.getById(id);
        personService.getById(eventParticipantDto.getPerson().getId());
        eventService.getById(eventParticipantDto.getEvent().getId());
        return eventParticipantDto;
    }

    @Override
    @Transactional
    public List<EventParticipantDto> getMainParticipants(@NonNull final Long eventId) {
        EventDto eventDto = eventService.getById(eventId);
        Event event = eventMapper.toEntity(eventDto);
        EventRoleDto eventRoleDto = eventRoleService.getMainByEventType(event.getType());


        if (eventDto.getType().mayHaveMultipleParticipants()) {
            return eventParticipantRepository.findByEventAndRole(event, eventRoleMapper.toEntity(eventRoleDto))
                    .stream().map(eventParticipantMapper::toDto).collect(Collectors.toList());
        } else {
            return eventParticipantRepository.findByEvent(event, PageRequest.of(0, 1))
                    .stream().map(eventParticipantMapper::toDto).collect(Collectors.toList());
        }
    }

    @Override
    @Transactional
    public EventParticipantDto getByPersonAndEventType(Long personId, EventType eventType) {
        EventParticipant ep = eventParticipantRepository.findByPersonAndEventType(
                        personMapper.toEntity(personService.getById(personId)),
                        eventType)
                .orElseThrow(() -> new EntityNotFoundException("Requested event participant does not exist"));
        return eventParticipantMapper.toDto(ep);
    }

    @Override
    @Transactional
    public boolean existsByPersonAndRole(Long personId, EventType type) {
        PersonEntity personEntity = personMapper.toEntity(personService.getById(personId));
        return eventParticipantRepository.existsByPersonAndEventType(personEntity, type);
    }

    @Override
    @Transactional
    public EventParticipantDto create(EventParticipantDto epd) {
        PersonDto personDto = personService.getById(epd.getPerson().getId());
        epd.setPerson(personDto);
        EventDto eventDto = eventService.getById(epd.getEvent().getId());
        epd.setEvent(eventDto);
        EventParticipantDto createdParticipant = super.create(epd);
        eventService.update(eventDto);
        return createdParticipant;
    }

    @Override
    @Transactional
    public EventParticipantDto update(EventParticipantDto participant) {
        PersonDto personDto = personService.getById(participant.getPerson().getId());
        getById(participant.getId());
        participant.setPerson(personDto);
        EventDto eventDto = eventService.getById(participant.getEvent().getId());
        participant.setEvent(eventDto);
        EventParticipantDto updatedParticipant = super.update(participant);
        eventService.update(eventDto);
        return updatedParticipant;
    }

    @Override
    @Transactional
    public EventParticipantDto delete(Long id) {
        getById(id);
        return super.delete(id);
    }

    @Override
    @Transactional
    public List<EventParticipantDto> deleteAllByPerson(PersonDto person) {
        List<EventParticipantDto> deletedParticipants = eventParticipantRepository
                .deleteAllByPerson(personMapper.toEntity(person)).stream()
                .map(eventParticipantMapper::toDto)
                .collect(Collectors.toList());

        deletedParticipants.forEach(epd -> {
            if (!eventParticipantRepository.existsByEvent(eventMapper.toEntity(epd.getEvent())))
                eventService.delete(epd.getEvent().getId());
        });

        return deletedParticipants;
    }
}
