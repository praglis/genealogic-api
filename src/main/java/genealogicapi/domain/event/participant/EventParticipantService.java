package genealogicapi.domain.event.participant;

import genealogicapi.domain.abstraction.AbstractService;
import genealogicapi.domain.event.EventType;
import genealogicapi.domain.person.PersonDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface EventParticipantService extends AbstractService<EventParticipantDto> {
    @Override
    @PreAuthorize("hasRole('USER')")
    EventParticipantDto create(EventParticipantDto dto);

    @PreAuthorize("hasRole('USER')")
    Page<EventParticipantDto> getByEvent(Long eventId, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    Page<EventParticipantDto> getByPerson(Long personId, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    List<EventParticipantDto> getMainParticipants(Long eventId);

    @PreAuthorize("hasRole('USER')")
    EventParticipantDto getByPersonAndEventType(Long personId, EventType eventType);

    @PreAuthorize("hasRole('USER')")
    boolean existsByPersonAndRole(Long personId, EventType type);

    @PreAuthorize("hasRole('USER')")
    List<EventParticipantDto> deleteAllByPerson(PersonDto person);
}
