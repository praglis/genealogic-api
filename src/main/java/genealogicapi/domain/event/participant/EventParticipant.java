package genealogicapi.domain.event.participant;

import genealogicapi.domain.abstraction.AbstractEntity;
import genealogicapi.domain.event.Event;
import genealogicapi.domain.event.role.EventRole;
import genealogicapi.domain.person.PersonEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true) // todo #89
@RequiredArgsConstructor
@Entity
@Table
public class EventParticipant extends AbstractEntity implements Serializable {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "epa_event")
    private Event event;

    @ManyToOne
    @JoinColumn(name = "epa_person")
    private PersonEntity person;

    @ManyToOne
    @JoinColumn(name = "epa_role")
    private EventRole role;
}
