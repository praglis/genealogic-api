package genealogicapi.domain.event.participant;

import genealogicapi.domain.event.Event;
import genealogicapi.domain.event.role.EventRole;
import genealogicapi.domain.person.PersonEntity;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@RequiredArgsConstructor
public class EventParticipantPK implements Serializable {
    private final Event event;
    private final PersonEntity person;
    private final EventRole role;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null || obj.getClass() != this.getClass())
            return false;

        EventParticipantPK eventParticipant = (EventParticipantPK) obj;

        return (eventParticipant.event.getId().equals(this.event.getId())
                && eventParticipant.person.getId().equals(this.person.getId())
                && eventParticipant.role.getId().equals(this.role.getId()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (event == null ? 0 : event.hashCode());
        hash = 23 * hash + (person == null ? 0 : person.hashCode());
        hash = 23 * hash + (role == null ? 0 : role.hashCode());
        return hash;
    }
}
