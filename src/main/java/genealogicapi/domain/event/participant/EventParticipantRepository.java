package genealogicapi.domain.event.participant;

import genealogicapi.domain.abstraction.AbstractRepository;
import genealogicapi.domain.event.Event;
import genealogicapi.domain.event.EventType;
import genealogicapi.domain.event.role.EventRole;
import genealogicapi.domain.person.PersonEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface EventParticipantRepository extends AbstractRepository<EventParticipant> {
    Page<EventParticipant> findByEvent(Event event, Pageable pageable);

    Page<EventParticipant> findByPerson(PersonEntity person, Pageable pageable);

    Optional<EventParticipant> findByPersonAndEventType(PersonEntity person, EventType eventType);

    List<EventParticipant> findByEventAndRole(Event event, EventRole role);

    boolean existsByPersonAndEventType(PersonEntity person, EventType eventType);

    List<EventParticipant> deleteAllByPerson(PersonEntity person);

    boolean existsByEvent(Event event);
}
