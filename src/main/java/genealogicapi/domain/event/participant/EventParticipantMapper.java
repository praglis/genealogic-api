package genealogicapi.domain.event.participant;

import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.event.role.EventRoleMapper;
import genealogicapi.domain.person.mapping.PersonMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {EventRoleMapper.class, PersonMapper.class})
public interface EventParticipantMapper extends AbstractMapper<EventParticipantDto, EventParticipant> {
    @Override
    @Mapping(target = "event.participants", ignore = true)
    EventParticipantDto toDto(EventParticipant entity);
}
