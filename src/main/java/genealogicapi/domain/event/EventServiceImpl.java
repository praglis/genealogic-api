package genealogicapi.domain.event;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractServiceImpl;
import genealogicapi.domain.event.dto.BasicEventsDto;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.event.participant.EventParticipantDto;
import genealogicapi.domain.event.participant.EventParticipantService;
import genealogicapi.domain.event.role.EventRoleService;
import genealogicapi.domain.person.PersonDto;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.person.PersonService;
import genealogicapi.domain.person.mapping.PersonMapper;
import genealogicapi.domain.tree.Tree;
import genealogicapi.domain.tree.TreeDto;
import genealogicapi.domain.tree.TreeMapper;
import genealogicapi.domain.tree.TreeService;
import genealogicapi.domain.user.UserService;
import genealogicapi.gedcom.structure.EventRecord;
import genealogicapi.gedcom.structure.IndividualRecord;
import genealogicapi.security.SecurityService;
import lombok.NonNull;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@NonNull
public class EventServiceImpl extends AbstractServiceImpl<EventDto, Event> implements EventService {

    private final EventRepository eventRepository;
    private final EventMapper eventMapper;
    private final SecurityService securityService;
    private final EventParticipantService eventParticipantService;
    private final UserService userService;
    private final PersonService personService;
    private final PersonMapper personMapper;
    private final TreeMapper treeMapper;
    private final TreeService treeService;
    private final EventRoleService eventRoleService;

    public EventServiceImpl(EventRepository eventRepository, //todo #66 (this service might be too big)
                            EventMapper eventMapper,
                            SecurityService securityService,
                            UserService userService,
                            @Lazy EventParticipantService eventParticipantService,
                            TreeService treeService,
                            PersonService personService,
                            EventRoleService eventRoleService,
                            PersonMapper personMapper,
                            TreeMapper treeMapper
    ) {
        super(eventRepository, eventMapper);
        this.eventRepository = eventRepository;
        this.eventMapper = eventMapper;
        this.securityService = securityService;
        this.userService = userService;
        this.eventParticipantService = eventParticipantService;
        this.personMapper = personMapper;
        this.treeMapper = treeMapper;
        this.treeService = treeService;
        this.personService = personService;
        this.eventRoleService = eventRoleService;
    }

    @Override
    @Transactional
    public EventDto create(final EventDto eventToCreate) {
        eventToCreate.setOwner(userService.getByUsername(securityService.findLoggedInUsername()));

        if (eventToCreate.getStartDate() != null && eventToCreate.getStartDate().isEmpty())
            eventToCreate.setStartDate(null);

        EventDto createdEvent = super.create(eventToCreate);
        if (Boolean.FALSE.equals(eventToCreate.isNameCustom())) {
            createdEvent.setName(generateEventName(createdEvent));
            createdEvent = this.update(createdEvent);
        }

        if (eventToCreate.getParticipants() != null) {
            for (EventParticipantDto participant : eventToCreate.getParticipants()) {
                participant.setEvent(createdEvent);
                eventParticipantService.create(participant);
            }
        }
        return createdEvent;
    }

    @Override
    @Transactional
    public EventRecord create(EventRecord eventRecord) {
        return eventMapper.toRecord(create(eventMapper.toDto(eventRecord)));
    }

    @Override
    @Transactional
    public void createWithParticipants(@NonNull final EventDto createdEvent,
                                       @NonNull final Set<IndividualRecord> individualRecords) {

        for (IndividualRecord individualRecord : individualRecords) {
            EventParticipantDto eventParticipantDto = new EventParticipantDto(
                    createdEvent,
                    personMapper.toDto(individualRecord),
                    eventRoleService.getMainByEventType(createdEvent.getType()));
            eventParticipantService.create(eventParticipantDto);
        }
    }

    @Override
    @Transactional
    public void createWithParticipants(@NonNull final EventRecord eventRecord,
                                       @NonNull final Set<IndividualRecord> individualRecords) {
        EventDto createdEvent = eventMapper.toDto(create(eventRecord));

        createWithParticipants(createdEvent, individualRecords);
    }

    @Override
    public String generateEventName(EventDto event) {
        String eventTypeName = event.getType().getValue();
        List<EventParticipantDto> mainParticipants = eventParticipantService.getMainParticipants(event.getId());
        if (mainParticipants.isEmpty()) return eventTypeName;

        String eventName = eventTypeName + " of";
        eventName = eventName.concat(" " + mainParticipants.get(0).getPerson().getShortName());
        for (int i = 1; i < mainParticipants.size(); i++) {
            EventParticipantDto eventParticipantDto = mainParticipants.get(i);
            eventName = eventName.concat(" and " + eventParticipantDto.getPerson().getShortName());
        }
        if (eventName.length() > 90) return eventName.substring(0, 90);
        return eventName;
    }

    @Override
    public EventDto getById(@NonNull final Long id) {
        EventDto eventDto = super.getById(id);
        securityService.checkIfUserIsLoggedIn(eventDto.getOwnerUsername());
        return eventDto;
    }

    @Override
    @Transactional
    public EventDto update(EventDto dto) {
        getById(dto.getId());
        if (Boolean.FALSE.equals(dto.isNameCustom())) dto.setName(generateEventName(dto));
        if (dto.getStartDate() != null && dto.getStartDate().isEmpty()) dto.setStartDate(null);
        return super.update(dto);
    }

    @Override
    @Transactional
    public EventDto delete(Long id) {
        Event event = findById(id);
        securityService.checkIfUserIsLoggedIn(event.getOwnerUsername());
        return deleteEntity(event);
    }

    @Override
    public Page<EventDto> getPersonEvents(Long personId, Pageable pageable) {
        PersonDto personDto = personService.getById(personId);
        return eventRepository.findByPerson(personMapper.toEntity(personDto), pageable).map(eventMapper::toDto);
    }

    @Override
    public Page<EventDto[]> getLifespansByTree(Long id, Pageable pageable) {
        List<EventDto[]> lifespans = new ArrayList<>();
        List<PersonDto> persons = personService.getPersonsByTree(id, pageable).getContent();
        for (PersonDto pd : persons) {
            lifespans.add(getPersonLifespan(pd.getId()));
        }
        return new PageImpl<>(lifespans);
    }

    @Override
    public Page<EventDto> getEventsByTreeOrderByDate(TreeDto treeDto, Pageable pageable) {
        return eventRepository.findByTreeOrderByDate(treeMapper.toEntity(treeDto), pageable).map(eventMapper::toDto);
    }

    @Override
    public Page<EventDto> getEventsByPersonOrderByDate(PersonDto personDto, Pageable pageable) {
        return eventRepository.findByPersonOrderByDate(personMapper.toEntity(personDto), pageable).map(eventMapper::toDto);
    }

    @Override
    public EventDto[] getPersonLifespan(Long personId) {
        PersonDto personDto = personService.getById(personId);
        PersonEntity personEntity = personMapper.toEntity(personDto);

        Event birth = eventRepository.findByPersonAndByType(personEntity, EventType.BIRT).orElseThrow(
                () -> new EntityNotFoundException("Birth event for person with id = " + personId + " was not found"));
        Event death = eventRepository.findByPersonAndByType(personEntity, EventType.DEAT).orElse(null);

        EventDto[] lifespan = new EventDto[2];
        lifespan[0] = eventMapper.toDto(birth);
        lifespan[1] = eventMapper.toDto(death);

        return lifespan;
    }

    @Override
    public BasicEventsDto getBasicEventsByPerson(Long personId) {
        BasicEventsDto basicEvents = new BasicEventsDto();
        basicEvents.setBirth(getByPersonAndType(personId, EventType.BIRT));
        if (eventParticipantService.existsByPersonAndRole(personId, EventType.DEAT)) {
            basicEvents.setDeath(getByPersonAndType(personId, EventType.DEAT));
        }
        if (eventParticipantService.existsByPersonAndRole(personId, EventType.BURI)) {
            basicEvents.setBurial(getByPersonAndType(personId, EventType.BURI));
        }
        return basicEvents;
    }

    private EventDto getByPersonAndType(Long personId, EventType type) {
        return eventParticipantService.getByPersonAndEventType(personId, type).getEvent();
    }

    @Override
    public Page<EventDto> getEventsByTree(Long id, Pageable pageable) {
        Tree tree = treeMapper.toEntity(treeService.getById(id));
        return eventRepository.findByTree(tree, pageable).map(eventMapper::toDto);
    }

    @Override
    public List<EventRecord> getAllRecordsByTree(Long treeId) {
        return getEventsByTree(treeId, Pageable.unpaged()).map(eventMapper::toRecord).getContent();
    }
}
