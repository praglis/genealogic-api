package genealogicapi.domain.event;

import com.fasterxml.jackson.annotation.JsonBackReference;
import genealogicapi.commons.Ownable;
import genealogicapi.domain.abstraction.AbstractEntity;
import genealogicapi.domain.date.DateEntity;
import genealogicapi.domain.event.participant.EventParticipant;
import genealogicapi.domain.place.Place;
import genealogicapi.domain.user.UserEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = "participants") // todo #89
@ToString(exclude = "participants")
@Entity
@Table
public class Event extends AbstractEntity implements Serializable, Ownable {
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "owner", nullable = false)
    private UserEntity owner;

    @Column(name = "name", length = 90)
    private String name;

    @Column(name = "name_is_custom")
    private Boolean isNameCustom;

    @Column(name = "type")
    private EventType type;

    @Column(name = "subtype", length = 90)
    private String subtype;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "place")
    private Place place;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "date_start")
    private DateEntity startDate;

    @Column(name = "cause", length = 90)
    private String cause;

    @Column(name = "details", length = 90)
    private String details;

    @OneToMany(mappedBy = "event", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<EventParticipant> participants;

    @Override
    public String getOwnerUsername() { //todo move to domain class #48?
        return owner.getUsername();
    }
}
