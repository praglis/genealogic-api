package genealogicapi.domain.event;

import genealogicapi.domain.abstraction.AbstractRepository;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.tree.Tree;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface EventRepository extends AbstractRepository<Event> {
    @Query("select distinct e from Event e, EventParticipant ep where e = ep.event and ep.person.tree = ?1")
    Page<Event> findByTree(Tree tree, Pageable pageable);

    @Query("select e from Event e, EventParticipant ep where e = ep.event and ep.person = ?1")
    Page<Event> findByPerson(PersonEntity person, Pageable pageable);

    @Query("select e from Event e, EventParticipant ep where e = ep.event and ep.person = ?1 and e.type = ?2")
    Optional<Event> findByPersonAndByType(PersonEntity person, EventType type);

    @Query(value = "select * from event e left join date_entity de on e.date_start = de.id " +
            "where exists(select e.id from person p, event_participant ep " +
            "    where e.id = ep.event and ep.person = p.id and p.tree = ?1) " +
            "order by de.value", nativeQuery = true)
    Page<Event> findByTreeOrderByDate(Tree tree, Pageable pageable);

    @Query(value = "select * from event e left join date_entity de on de.id = e.date_start, event_participant ep " +
            "where e.id = ep.event and ep.person = ?1 order by de.value", nativeQuery = true)
    Page<Event> findByPersonOrderByDate(PersonEntity person, Pageable pageable);
}
