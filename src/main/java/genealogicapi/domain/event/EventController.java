package genealogicapi.domain.event;

import genealogicapi.domain.abstraction.AbstractController;
import genealogicapi.domain.event.dto.BasicEventsDto;
import genealogicapi.domain.event.dto.EventDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Event", description = "Operations related to individual (e. g. birth) and family (e. g. marriage) events.")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequestMapping("/events")
public class EventController extends AbstractController<EventDto> {

    private final EventService eventService;

    public EventController(EventService eventService) {
        super(eventService);
        this.eventService = eventService;
    }

    //    @Hidden
    @Operation(summary = "Returns list of events in which people of the requested tree participated.")
    @GetMapping("/tree/{id}")
    public Page<EventDto> getEventsByTree(@PathVariable Long id, Pageable pageable) {
        return eventService.getEventsByTree(id, pageable);
    }

    //    @Hidden
    @Operation(summary = "Returns list of events in which the requested person participated.")
    @GetMapping("/person/{id}")
    public Page<EventDto> getEventsByPerson(@PathVariable Long id, Pageable pageable) {
        return eventService.getPersonEvents(id, pageable);
    }

    @Operation(summary = "Returns lifespan - array containing birth and death events (only birth if the person is alive).")
    @GetMapping("/lifespans/person/{id}")
    public EventDto[] getPersonLifespan(@PathVariable Long id) {
        return eventService.getPersonLifespan(id);
    }

    @Operation(summary = "Returns page containing lifespans of people belonging to the requested tree.",
            description = "Lifespan means array containing birth and (if exists) death event(s) of a certain person.")
    @GetMapping("/lifespans/tree/{id}")
    public Page<EventDto[]> getTreeLifespan(@PathVariable Long id, Pageable pageable) {
        return eventService.getLifespansByTree(id, pageable);
    }

    @Operation(summary = "Returns birth, death and burial events for requested person (only birth if the person is alive).",
            description = "Birth, death and burial events are considered as 'basic' events.")
    @GetMapping("/basic/person/{personId}")
    public BasicEventsDto getBasicEventsByPerson(@PathVariable Long personId) {
        return eventService.getBasicEventsByPerson(personId);
    }
}
