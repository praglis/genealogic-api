package genealogicapi.domain.event.dto;

import genealogicapi.commons.Ownable;
import genealogicapi.commons.validation.ValidId;
import genealogicapi.domain.abstraction.AbstractDto;
import genealogicapi.domain.date.DateDto;
import genealogicapi.domain.event.EventType;
import genealogicapi.domain.event.participant.EventParticipantDto;
import genealogicapi.domain.place.PlaceDto;
import genealogicapi.domain.user.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true, exclude = "participants")
public class EventDto extends AbstractDto implements Ownable {
    @Size(min = 1, max = 90)
    private String name;

    @NotNull
    @Getter(AccessLevel.NONE)
    private Boolean isNameCustom;

    @NotNull
    private EventType type;

    @Size(max = 90)
    private String subtype;

    @ValidId
    private UserDto owner;

    private PlaceDto place;

    private DateDto startDate;

    @Size(min = 1, max = 90)
    private String cause;

    @Size(min = 1, max = 90)
    private String details;

    private Set<EventParticipantDto> participants;

    public Boolean isNameCustom() {
        return isNameCustom;
    }

    @Override
    public String getOwnerUsername() {
        return owner.getUsername();
    }
}
