package genealogicapi.domain.event.dto;

import lombok.Data;

@Data
public class BasicEventsDto {
    private EventDto birth;
    private EventDto death;
    private EventDto burial;
}
