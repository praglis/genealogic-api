package genealogicapi.domain.event;

import genealogicapi.domain.date.DateMapper;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.event.participant.EventParticipantMapper;
import genealogicapi.domain.place.PlaceMapper;
import genealogicapi.gedcom.GedcomRecordMapper;
import genealogicapi.gedcom.structure.EventRecord;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = {PlaceMapper.class, DateMapper.class, EventParticipantMapper.class})
public interface EventMapper extends GedcomRecordMapper<EventDto, Event, EventRecord> {
    @Override
    @Mapping(target = "isNameCustom", source = "nameCustom",
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT)
    @Mapping(target = "participants", ignore = true)
    Event toEntity(EventDto dto);

    @Override
    @Mapping(target = "isNameCustom", constant = "false")
    EventDto toDto(EventRecord eventRecord);
}
