package genealogicapi.domain.event;

import genealogicapi.gedcom.structure.GedcomTag;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public enum EventType {
    BIRT("Birth", true, true),
    CHR("Christening", true, true),
    DEAT("Death", true, false),
    BURI("Burial", true, false),
    ADOP("Adoption", true, true),
    BAPM("Baptism", true, true),
    NATU("Naturalization", true, false),
    EMIG("Emigration", true, false),
    IMMI("Immigration", true, false),
    GRAD("Graduation", true, false),
    RETI("Retirement", true, false),
    CONF("Confirmation", true, false),
    EVEN("Unspecified event", true, true),

    DIV("Divorce", false, true),
    ENGA("Engagement", false, true),
    MARR("Marriage", false, true);

    @Getter
    private final String value;
    @Getter
    private final boolean isIndividual;
    private final boolean mayHaveMultipleParticipants;

    public boolean mayHaveMultipleParticipants() {
        return mayHaveMultipleParticipants;
    }

    public static boolean contains(GedcomTag tag) {
        for (EventType gt : EventType.values()) {
            if (gt.name().equals(tag.name())) {
                return true;
            }
        }
        return false;
    }
}
