package genealogicapi.domain.event.role;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractMapper;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Mapper(componentModel = "spring")
public abstract class EventRoleMapper implements AbstractMapper<EventRoleDto, EventRole> {
    @Autowired
    private EventRoleRepository eventRoleRepository;

    @Override
    @Transactional
    public EventRole toEntity(EventRoleDto dto) {
        if (eventRoleRepository.existsByDescriptor(dto.getDescriptor())) {
            return eventRoleRepository.findByDescriptor(dto.getDescriptor()).orElseThrow(
                    () -> new EntityNotFoundException("Role not found"));
        } else {
            return eventRoleRepository.saveAndFlush(new EventRole(dto.getDescriptor()));
        }
    }
}
