package genealogicapi.domain.event.role;

import genealogicapi.domain.abstraction.AbstractController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Event role", description = "Operations related to roles of people that participate in certain events.")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequestMapping("/event-roles")
public class EventRoleController extends AbstractController<EventRoleDto> {

    private final EventRoleService eventRoleService;

    public EventRoleController(EventRoleService eventRoleService) {
        super(eventRoleService);
        this.eventRoleService = eventRoleService;
    }

    //    @Hidden
    @Operation(summary = "Returns page containing all event roles of the requested person.")
    @GetMapping("/person/{id}")
    public Page<EventRoleDto> getEventRolesByPerson(@PathVariable Long id, Pageable pageable) {
        return eventRoleService.getByPerson(id, pageable);
    }
}
