package genealogicapi.domain.event.role;

import genealogicapi.domain.abstraction.AbstractRepository;
import genealogicapi.domain.person.PersonEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface EventRoleRepository extends AbstractRepository<EventRole> {
    @Query("select er from EventRole er, EventParticipant ep where er = ep.role and ep.person = ?1")
    Page<EventRole> findByPerson(PersonEntity person, Pageable pageable);

    Optional<EventRole> findByDescriptor(String descriptor);

    boolean existsByDescriptor(String descriptor);
}
