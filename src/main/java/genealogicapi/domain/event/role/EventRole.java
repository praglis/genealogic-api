package genealogicapi.domain.event.role;

import genealogicapi.domain.abstraction.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)  // todo #89
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class EventRole extends AbstractEntity implements Serializable {
    @Column(name = "descriptor", nullable = false, length = 25)
    private String descriptor;
}
