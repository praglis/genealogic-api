package genealogicapi.domain.event.role;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractServiceImpl;
import genealogicapi.domain.event.EventType;
import genealogicapi.domain.person.PersonDto;
import genealogicapi.domain.person.PersonService;
import genealogicapi.domain.person.mapping.PersonMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EventRoleServiceImpl extends AbstractServiceImpl<EventRoleDto, EventRole> implements EventRoleService {

    private final EventRoleRepository eventRoleRepository;
    private final EventRoleMapper eventRoleMapper;
    private final PersonService personService;
    private final PersonMapper personMapper;

    public EventRoleServiceImpl(EventRoleRepository eventRoleRepository,
                                EventRoleMapper eventRoleMapper,
                                PersonService personService,
                                PersonMapper personMapper) {
        super(eventRoleRepository, eventRoleMapper);
        this.eventRoleRepository = eventRoleRepository;
        this.eventRoleMapper = eventRoleMapper;
        this.personService = personService;
        this.personMapper = personMapper;
    }

    @Override
    @Transactional
    public Page<EventRoleDto> getByPerson(Long personId, Pageable pageable) {
        PersonDto personDto = personService.getById(personId);
        return eventRoleRepository.findByPerson(personMapper.toEntity(personDto), pageable)
                .map(eventRoleMapper::toDto);
    }

    @Override
    @Transactional
    public EventRoleDto getByDescriptor(String descriptor) {
        EventRole eventRole = eventRoleRepository.findByDescriptor(descriptor).orElseThrow(
                () -> new EntityNotFoundException("Entity (role) was not found.")
        );
        return eventRoleMapper.toDto(eventRole);
    }

    @Override
    @Transactional
    public EventRoleDto getMainByEventType(EventType eventType) {
        String descriptor;
        if (eventType != null) {
            switch (eventType) { //todo refactor this switch #67
                case BIRT:
                case BAPM:
                case ADOP:
                    descriptor = "MAIN";
                    break;
                case ENGA:
                case MARR:
                case DIV:
                    descriptor = "SPOU";
                    break;
                case GRAD:
                case RETI:
                case DEAT:
                case BURI:
                case IMMI:
                case EMIG:
                case NATU:
                default:
                    descriptor = "MAIN";
                    break;
            }
        } else descriptor = "MAIN";

        if (!eventRoleRepository.existsByDescriptor(descriptor)) {
            return create(new EventRoleDto(descriptor));
        } else return getByDescriptor(descriptor);
    }
}
