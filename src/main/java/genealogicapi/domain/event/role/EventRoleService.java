package genealogicapi.domain.event.role;

import genealogicapi.domain.abstraction.AbstractService;
import genealogicapi.domain.event.EventType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface EventRoleService extends AbstractService<EventRoleDto> {
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    EventRoleDto create(EventRoleDto dto);

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    EventRoleDto getById(Long id);

    @PreAuthorize("hasRole('USER')")
    Page<EventRoleDto> getByPerson(Long personId, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    EventRoleDto getByDescriptor(String descriptor);

    @PreAuthorize("hasRole('USER')")
    EventRoleDto getMainByEventType(EventType eventType);

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    EventRoleDto update(EventRoleDto dto);

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    EventRoleDto delete(Long id);
}
