package genealogicapi.domain.event.role;

import genealogicapi.domain.abstraction.AbstractDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class EventRoleDto extends AbstractDto {
    @NotBlank
    @Size(min = 1, max = 25)
    private String descriptor;
}
