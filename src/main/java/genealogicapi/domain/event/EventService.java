package genealogicapi.domain.event;

import genealogicapi.domain.event.dto.BasicEventsDto;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.person.PersonDto;
import genealogicapi.domain.tree.TreeDto;
import genealogicapi.gedcom.GedcomConvertible;
import genealogicapi.gedcom.structure.EventRecord;
import genealogicapi.gedcom.structure.IndividualRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Set;

public interface EventService extends GedcomConvertible<EventRecord, EventDto> {
    @PreAuthorize("hasRole('USER')")
    Page<EventDto> getEventsByTree(Long id, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    Page<EventDto> getEventsByTreeOrderByDate(TreeDto treeDto, Pageable pageable);

    String generateEventName(EventDto event);

    @PreAuthorize("hasRole('USER')")
    Page<EventDto> getPersonEvents(Long personId, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    Page<EventDto> getEventsByPersonOrderByDate(PersonDto personDto, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    EventDto[] getPersonLifespan(Long personId);

    @PreAuthorize("hasRole('USER')")
    BasicEventsDto getBasicEventsByPerson(Long personId);

    @PreAuthorize("hasRole('USER')")
    Page<EventDto[]> getLifespansByTree(Long id, Pageable pageable);

    @Override
    @PreAuthorize("hasRole('USER')")
    EventDto create(EventDto dto);

    @PreAuthorize("hasRole('USER')")
    void createWithParticipants(EventRecord eventRecord, Set<IndividualRecord> individualRecord);

    @PreAuthorize("hasRole('USER')")
    void createWithParticipants(EventDto createdEvent, Set<IndividualRecord> individualRecords);


    @PreAuthorize("hasRole('USER')")
    String generateAndUpdateEventNameIfNeeded(EventDto eventToCreate);
}
