package genealogicapi.domain.abstraction;

public interface AbstractMapper<D extends AbstractDto, E extends AbstractEntity> {
    D toDto(E entity);

    E toEntity(D dto);
}
