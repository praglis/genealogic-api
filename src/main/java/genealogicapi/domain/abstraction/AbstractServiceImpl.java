package genealogicapi.domain.abstraction;

import genealogicapi.commons.exception.EntityNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RequiredArgsConstructor
public abstract class AbstractServiceImpl<D extends AbstractDto, E extends AbstractEntity> implements AbstractService<D> {

    protected final AbstractRepository<E> repository;
    protected final AbstractMapper<D, E> mapper;

    @Override
    public Page<D> getAll(Pageable pageable) {
        return repository.findAll(pageable).map(mapper::toDto);
    }

    @Override
    public D getById(@NonNull final Long id) {
        return mapper.toDto(findById(id));
    }

    public E findById(@NonNull final Long id) {
        Optional<E> optionalEntity = repository.findById(id);
        return optionalEntity.orElseThrow(() -> new EntityNotFoundException("Entity was not found"));
    }

    @Override
    @Transactional
    public D create(D dto) {
        E entity = mapper.toEntity(dto);
        entity = repository.saveAndFlush(entity);
        return mapper.toDto(entity);
    }

    @Override
    @Transactional
    public D update(D dto) {
        findById(dto.getId());
        E updatedEntity = mapper.toEntity(dto);
        return mapper.toDto(repository.saveAndFlush(updatedEntity));
    }

    @Override
    @Transactional
    public D delete(Long id) {
        E entityToDelete = findById(id);
        return deleteEntity(entityToDelete);
    }

    @Transactional
    public D deleteEntity(E entityToDelete) {
        repository.delete(entityToDelete);
        return mapper.toDto(entityToDelete);
    }
}
