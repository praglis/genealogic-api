package genealogicapi.domain.abstraction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface AbstractService<D extends AbstractDto> {
    D create(D dto);

    @PreAuthorize("hasRole('ADMIN')")
    Page<D> getAll(Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    D getById(Long id);

    @PreAuthorize("hasRole('USER')")
    D update(D dto);

    @PreAuthorize("hasRole('USER')")
    D delete(Long id);
}
