package genealogicapi.domain.abstraction;

import genealogicapi.commons.StringMapper;
import genealogicapi.commons.exception.IdInconsistencyException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;

@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequiredArgsConstructor
public abstract class AbstractController<D extends AbstractDto> {

    protected final AbstractService<D> service;

    @Operation(summary = "Creates object in this domain.")
    @PostMapping
    public ResponseEntity<D> create(@Valid @RequestBody D dto) {
        D createdDto = service.create(dto);
        return ResponseEntity.created(URI.create(
                        String.format("/%ss/%s",
                                StringMapper.dtoClassToResourceName(dto.getClass()),
                                createdDto.getId())))
                .body(createdDto);
    }

    @Operation(summary = "Returns list of all objects in this domain. Requires being authenticated as an administrator.")
    @GetMapping
    public Page<D> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @Operation(summary = "Returns object in this domain having requested ID.")
    @ApiResponse(responseCode = "404", description = "Returned when object with requested ID doesn't exist.",
            content = @Content)
    @GetMapping("/{id}")
    public ResponseEntity<D> getById(@PathVariable Long id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @Operation(summary = "Updates object in this domain having requested ID.",
            description = "ID passed in path and in request body must be identical.")
    @ApiResponse(responseCode = "400", description = "Returned when IDs passed in path and in request body aren't identical.",
            content = @Content)
    @PutMapping("/{id}")
    public ResponseEntity<D> update(@Valid @RequestBody D dto, @NotNull @PathVariable Long id) {
        if (!id.equals(dto.getId()))
            throw new IdInconsistencyException(dto.getId(), id);

        return ResponseEntity.ok(service.update(dto));
    }

    @Operation(summary = "Deletes object in this domain having requested ID.")
    @ApiResponse(responseCode = "404", description = "Returned when object with requested ID doesn't exist.",
            content = @Content)
    @DeleteMapping("/{id}")
    public ResponseEntity<D> delete(@PathVariable Long id) {
        return ResponseEntity.ok(service.delete(id));
    }
}

