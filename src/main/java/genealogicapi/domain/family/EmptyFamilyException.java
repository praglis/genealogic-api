package genealogicapi.domain.family;

public class EmptyFamilyException extends RuntimeException {
    public EmptyFamilyException(String context) {
        super(String.format("Encountered empty family while: %s. Any family must contain at least 1 person.", context));
    }
}
