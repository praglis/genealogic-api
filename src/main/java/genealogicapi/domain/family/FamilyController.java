package genealogicapi.domain.family;

import genealogicapi.domain.abstraction.AbstractController;
import genealogicapi.domain.abstraction.AbstractService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Family",
        description = "Operations related to families, where family is defined as parents and their children.")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequestMapping("/families")
public class FamilyController extends AbstractController<FamilyDto> {

    private final FamilyService familyService;

    public FamilyController(AbstractService<FamilyDto> service, FamilyService familyService) {
        super(service);
        this.familyService = familyService;
    }

    @Operation(summary = "Returns page containing families in which the requested person is child/sibling.")
    @GetMapping("/child/{childId}")
    public Page<FamilyDto> getFamiliesContainingChild(@PathVariable Long childId, Pageable pageable) {
        return familyService.getFamiliesContainingChild(childId, pageable);
    }

    @Operation(summary = "Returns page containing families in which the requested person is spouse/parent.")
    @GetMapping("/spouse/{spouseId}")
    public Page<FamilyDto> getFamiliesContainingSpouse(@PathVariable Long spouseId, Pageable pageable) {
        return familyService.getFamiliesContainingSpouse(spouseId, pageable);
    }

    @Operation(summary = "Returns page containing all families whose members belong to the tree having requested ID.")
    @GetMapping("/tree/{treeId}")
    public Page<FamilyDto> getTreeFamilies(@PathVariable Long treeId, Pageable pageable) {
        return familyService.getFamiliesByTree(treeId, pageable);
    }
}
