package genealogicapi.domain.family;

import com.fasterxml.jackson.annotation.JsonProperty;
import genealogicapi.commons.Ownable;
import genealogicapi.domain.abstraction.AbstractDto;
import genealogicapi.domain.person.PersonDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
public class FamilyDto extends AbstractDto implements Family<PersonDto>, Ownable {
    private PersonDto husband;
    private PersonDto wife;
    private List<PersonDto> children;
    private String details;

    @Override
    public Set<PersonDto> getSpouses() {
        Set<PersonDto> spouses = new HashSet<>();
        if (husband != null) spouses.add(husband);
        if (wife != null) spouses.add(wife);
        return spouses;
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Override
    public String getOwnerUsername() {
        PersonDto anyFamilyMember;
        if (husband != null) anyFamilyMember = husband;
        else if (wife != null) anyFamilyMember = wife;
        else if (!children.isEmpty()) anyFamilyMember = children.get(0);
        else throw new EmptyFamilyException("retrieving any family member in FamilyDto");
        return anyFamilyMember.getOwnerUsername();
    }

    @Override
    public boolean spousesContainId(@NonNull Long id) {
        return getSpouses().stream()
                .anyMatch(individualRecord -> individualRecord.getId().equals(id));
    }

    @Override
    public boolean hasHusband() {
        return this.husband != null;
    }

    @Override
    public boolean hasWife() {
        return this.wife != null;
    }

    @Override
    public boolean hasAtLeastOneChild() {
        return this.children != null && !this.children.isEmpty();
    }

    public void addChild(PersonDto child) {
        children.add(child);
    }

    public int getSize() {
        return (children == null ? 0 : children.size()) + (husband == null ? 0 : 1) + (wife == null ? 0 : 1);
    }
}
