package genealogicapi.domain.family;

public enum Relationship {
    FATHER,
    MOTHER,
    CHILD,
    SPOUSE
}
