package genealogicapi.domain.family;

import genealogicapi.gedcom.GedcomConvertible;
import genealogicapi.gedcom.structure.FamilyRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface FamilyService extends GedcomConvertible<FamilyRecord, FamilyDto> {

    @PreAuthorize("hasRole('USER')")
    Page<FamilyDto> getFamiliesByTree(Long treeId, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    Page<FamilyDto> getFamiliesContainingPerson(Long personId, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    Page<FamilyDto> getFamiliesContainingChild(Long childId, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    Page<FamilyDto> getFamiliesContainingSpouse(Long spouseId, Pageable pageable);

    @Override
    @PreAuthorize("hasRole('USER')")
    FamilyDto create(FamilyDto dto);

    @PreAuthorize("hasRole('USER')")
    List<FamilyDto> deleteContainingPerson(Long personId);
}
