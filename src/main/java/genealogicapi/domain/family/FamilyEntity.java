package genealogicapi.domain.family;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import genealogicapi.commons.Ownable;
import genealogicapi.domain.abstraction.AbstractEntity;
import genealogicapi.domain.person.PersonEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true) // todo #89
@NoArgsConstructor
@SuperBuilder

@Entity
@Table(name = "family")
public class FamilyEntity extends AbstractEntity implements Serializable, Ownable {
    @JsonBackReference
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "husband")
    private PersonEntity husband;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "wife")
    private PersonEntity wife;

    @JsonBackReference
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "family_child",
            joinColumns = @JoinColumn(name = "family_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "person_id", referencedColumnName = "id")
    )
    @ToString.Exclude
    private List<PersonEntity> children;

    @Column(name = "details")
    private String details;

    @JsonIgnore
    public String getOwnerUsername() {
        PersonEntity anyFamilyMember;
        if (husband != null) anyFamilyMember = husband;
        else if (wife != null) anyFamilyMember = wife;
        else if (!children.isEmpty()) anyFamilyMember = children.get(0);
        else throw new EmptyFamilyException("retrieving any family member in FamilyEntity");
        return anyFamilyMember.getOwnerUsername();
    }
}
