package genealogicapi.domain.family;

import genealogicapi.domain.person.Person;

import java.util.Set;

public interface Family<P extends Person> {
    Set<P> getSpouses();

    boolean spousesContainId(Long id);

    boolean hasHusband();

    boolean hasWife();

    boolean hasAtLeastOneChild();
}
