package genealogicapi.domain.family;

import genealogicapi.domain.abstraction.AbstractServiceImpl;
import genealogicapi.domain.person.PersonDto;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.person.PersonService;
import genealogicapi.domain.person.mapping.PersonMapper;
import genealogicapi.domain.tree.TreeMapper;
import genealogicapi.domain.tree.TreeService;
import genealogicapi.gedcom.structure.FamilyRecord;
import genealogicapi.security.SecurityService;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FamilyServiceImpl extends AbstractServiceImpl<FamilyDto, FamilyEntity> implements FamilyService {

    private final FamilyRepository familyRepository;
    private final FamilyMapper familyMapper;
    private final SecurityService securityService;
    private final PersonService personService;
    private final PersonMapper personMapper;
    private final TreeService treeService;
    private final TreeMapper treeMapper;

    public FamilyServiceImpl(FamilyRepository familyRepository,
                             FamilyMapper familyMapper,
                             SecurityService securityService,
                             PersonService personService,
                             PersonMapper personMapper,
                             @Lazy TreeService treeService, TreeMapper treeMapper) {
        super(familyRepository, familyMapper);
        this.familyRepository = familyRepository;
        this.securityService = securityService;
        this.personService = personService;
        this.familyMapper = familyMapper;
        this.personMapper = personMapper;
        this.treeService = treeService;
        this.treeMapper = treeMapper;
    }

    @Override
    @Transactional
    public Page<FamilyDto> getFamiliesByTree(Long treeId, Pageable pageable) {
        return familyRepository.findAllByTree(treeMapper.toEntity(treeService.getById(treeId)), pageable)
                .map(familyMapper::toDto);
    }

    @Override
    @Transactional
    public List<FamilyRecord> getAllRecordsByTree(Long treeId) {
        return getFamiliesByTree(treeId, Pageable.unpaged()).map(familyMapper::toRecord).getContent();
    }

    @Override
    @Transactional
    public Page<FamilyDto> getFamiliesContainingPerson(Long personId, Pageable pageable) {
        PersonDto personDto = personService.getById(personId);
        PersonEntity personEntity = personMapper.toEntity(personDto);
        return familyRepository.findDistinctByHusbandOrWifeOrChildrenContaining(
                personEntity, personEntity, personEntity, pageable).map(familyMapper::toDto);
    }

    @Override
    @Transactional
    public Page<FamilyDto> getFamiliesContainingChild(Long childId, Pageable pageable) {
        PersonDto child = personService.getById(childId);
        return familyRepository.findAllByChildrenContaining(personMapper.toEntity(child), pageable).map(familyMapper::toDto);
    }

    @Override
    @Transactional
    public Page<FamilyDto> getFamiliesContainingSpouse(Long spouseId, Pageable pageable) {
        PersonDto spouseDto = personService.getById(spouseId);
        PersonEntity spouseEntity = personMapper.toEntity(spouseDto);
        return familyRepository.findAllByHusbandOrWife(spouseEntity, spouseEntity, pageable).map(familyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public FamilyDto getById(Long id) {
        FamilyDto familyDto = super.getById(id);
        securityService.checkIfUserIsLoggedIn(familyDto.getOwnerUsername());
        return familyDto;
    }

    @Override
    @Transactional
    public FamilyDto create(FamilyDto newFamily) {
        return super.create(getFamilyPersons(newFamily));
    }

    private FamilyDto getFamilyPersons(FamilyDto newFamily) {
        PersonDto husband;
        PersonDto wife;
        if (newFamily.getSize() < 1) throw new EmptyFamilyException("creating new family");
        if (newFamily.hasHusband()) {
            husband = personService.getById(newFamily.getHusband().getId());
            newFamily.setHusband(husband);
        }

        if (newFamily.hasWife()) {
            wife = personService.getById(newFamily.getWife().getId());
            newFamily.setWife(wife);
        }

        List<PersonDto> children = new ArrayList<>();
        if (newFamily.hasAtLeastOneChild()) {
            for (PersonDto child : newFamily.getChildren()) {
                children.add(personService.getById(child.getId()));
            }
            newFamily.setChildren(children);
        }
        return newFamily;
    }

    @Override
    @Transactional
    public FamilyRecord create(FamilyRecord familyRecord) {
        return familyMapper.toRecord(create(familyMapper.toDto(familyRecord)));
    }

    @Override
    @Transactional
    public FamilyDto update(FamilyDto family) {
        getById(family.getId());
        return super.update(getFamilyPersons(family));
    }

    @Override
    @Transactional
    public FamilyDto delete(Long id) {
        FamilyEntity family = findById(id);
        securityService.checkIfUserIsLoggedIn(family.getOwnerUsername());
        return deleteEntity(family);
    }

    @Override
    @Transactional
    public List<FamilyDto> deleteContainingPerson(Long personId) {
        PersonDto personDto = personService.getById(personId);
        PersonEntity personEntity = personMapper.toEntity(personDto);
        return familyRepository.deleteAllByHusbandOrWifeOrChildrenContaining(personEntity, personEntity, personEntity)
                .stream()
                .map(familyMapper::toDto).collect(Collectors.toList());
    }

}
