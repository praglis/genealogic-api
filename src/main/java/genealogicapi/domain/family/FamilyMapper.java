package genealogicapi.domain.family;

import genealogicapi.domain.person.mapping.PersonMapper;
import genealogicapi.gedcom.GedcomRecordMapper;
import genealogicapi.gedcom.structure.FamilyRecord;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;

@Mapper(componentModel = "spring", uses = PersonMapper.class, imports = ArrayList.class)
public interface FamilyMapper extends GedcomRecordMapper<FamilyDto, FamilyEntity, FamilyRecord> {
    @Override
    @Mapping(target = "familyEvents", expression = "java(new ArrayList<>())")
    FamilyRecord toRecord(FamilyDto dto);
}
