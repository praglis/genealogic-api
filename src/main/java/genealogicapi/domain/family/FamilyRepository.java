package genealogicapi.domain.family;

import genealogicapi.domain.abstraction.AbstractRepository;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.tree.Tree;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FamilyRepository extends AbstractRepository<FamilyEntity> {
    Page<FamilyEntity> findAllByHusbandOrWife(PersonEntity husband, PersonEntity wife, Pageable pageable);

    Page<FamilyEntity> findDistinctByHusbandOrWifeOrChildrenContaining(PersonEntity husband, PersonEntity wife, PersonEntity child, Pageable pageable);

    List<FamilyEntity> deleteAllByHusbandOrWifeOrChildrenContaining(PersonEntity husband, PersonEntity wife, PersonEntity child);

    Page<FamilyEntity> findAllByChildrenContaining(PersonEntity child, Pageable pageable);

    @Query(value = "select distinct on (f.id) * from family f left join family_child fc on f.id = fc.family_id " +
            "where exists(select * from person p where p.tree = ?1 " +
            "and (p.id = f.husband or p.id = f.wife or p.id = fc.person_id))",
            nativeQuery = true)
    Page<FamilyEntity> findAllByTree(Tree tree, Pageable pageable);
}
