package genealogicapi.domain.tree;

public class PersonNotFoundInTreeException extends RuntimeException {
    public PersonNotFoundInTreeException(String message) {
        super(message);
    }
}
