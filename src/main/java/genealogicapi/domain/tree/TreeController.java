package genealogicapi.domain.tree;

import genealogicapi.domain.abstraction.AbstractController;
import genealogicapi.domain.event.dto.EventDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "Tree", description = "Contains operations related to genealogical trees.")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequestMapping("/trees")
public class TreeController extends AbstractController<TreeDto> {

    private final TreeService treeService;

    public TreeController(TreeService treeService) {
        super(treeService);
        this.treeService = treeService;
    }

    @Operation(summary = "Returns trees belonging to user with requested ID")
    @GetMapping("/owner/{id}")
    public Page<TreeDto> getTreesByOwner(
            @Parameter(description = "owner ID") @PathVariable Long id,
            Pageable pageable) {
        return treeService.getTreesByOwner(id, pageable);
    }

    @Operation(summary = "Returns tree timeline - list of chronologically sorted events in which people " +
            "from a certain tree participated.", description = "Only events with not null dates and having at least one " +
            "participant belonging to tree are considered.")
    @GetMapping("/{id}/timeline")
    public Page<EventDto> getTimelineEvents(
            @Parameter(description = "tree ID") @PathVariable Long id, Pageable pageable) {
        return treeService.getTimelineEvents(id, pageable);
    }

    @Operation(summary = "Sets the proband of a tree.",
            description = "Allows setting a person with specific ID as the proband of a tree with specific ID. " +
                    "Clients should build pedigrees starting from this person.")
    @PatchMapping("/{id}/proband-id")
    public ResponseEntity<TreeDto> modifyProbandId(@Parameter(description = "tree ID") @PathVariable Long id,
                                                   @io.swagger.v3.oas.annotations.parameters.RequestBody(
                                                           description = "new proband ID",
                                                           content = @Content(schema = @Schema(type = "Long", example = "0")))
                                                   @RequestBody Long probandId) {
        return ResponseEntity.ok(treeService.modifyProbandId(id, probandId));
    }
}
