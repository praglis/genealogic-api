package genealogicapi.domain.tree;

import genealogicapi.domain.abstraction.AbstractServiceImpl;
import genealogicapi.domain.event.EventService;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.family.FamilyService;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.person.PersonService;
import genealogicapi.domain.user.UserDto;
import genealogicapi.domain.user.UserEntity;
import genealogicapi.domain.user.UserService;
import genealogicapi.domain.user.mapping.UserMapper;
import genealogicapi.security.SecurityService;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TreeServiceImpl extends AbstractServiceImpl<TreeDto, Tree> implements TreeService {

    private final TreeRepository treeRepository;
    private final UserService userService;
    private final TreeMapper treeMapper;
    private final UserMapper userMapper;
    private final SecurityService securityService;
    private final PersonService personService;
    private final FamilyService familyService;
    private final EventService eventService;

    public TreeServiceImpl(TreeRepository treeRepository, //todo #66
                           UserService userService,
                           TreeMapper treeMapper,
                           UserMapper userMapper,
                           SecurityService securityService,
                           @Lazy PersonService personService,
                           FamilyService familyService,
                           @Lazy EventService eventService) {
        super(treeRepository, treeMapper);
        this.treeRepository = treeRepository;
        this.userService = userService;
        this.treeMapper = treeMapper;
        this.userMapper = userMapper;
        this.securityService = securityService;
        this.personService = personService;
        this.familyService = familyService;
        this.eventService = eventService;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TreeDto> getTreesByOwner(Long ownerId, Pageable pageable) {
        securityService.checkIfUserIsLoggedIn(ownerId);
        UserEntity user = userMapper.toEntity(userService.getById(ownerId));
        return treeRepository.findByOwner(user, pageable).map(treeMapper::toDto);
    }

    @Override
    @Transactional
    public List<TreeDto> deleteAllByOwner(Long ownerId) {
        UserEntity user = userMapper.toEntity(userService.getById(ownerId));
        return treeRepository.findByOwner(user, Pageable.unpaged())
                .stream().map(treeMapper::toDto)
                .map(tree -> delete(tree.getId()))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public TreeDto getById(Long id) {
        TreeDto treeDto = super.getById(id);
        securityService.checkIfUserIsLoggedIn(treeDto.getOwnerUsername());
        return treeDto;
    }


    @Override
    @Transactional
    public TreeDto update(TreeDto dto) {
        securityService.checkIfUserIsLoggedIn(dto.getOwner().getId());
        TreeDto updatedCounts = updateCounts(dto.getId());
        dto.setPersonsCount(updatedCounts.getPersonsCount());
        return super.update(dto);
    }

    @Override
    @Transactional
    public TreeDto delete(Long id) {
        Tree tree = findById(id);
        securityService.checkIfUserIsLoggedIn(tree.getOwnerUsername());
        if (tree.getPersons() != null) {
            for (PersonEntity p : tree.getPersons()) {
                familyService.deleteContainingPerson(p.getId());
                personService.delete(p.getId());
            }
        }
        return deleteEntity(tree);
    }

    @Override
    @Transactional
    public Page<EventDto> getTimelineEvents(Long id, Pageable pageable) {
        return eventService.getEventsByTreeOrderByDate(getById(id), pageable);
    }

    @Override
    @Transactional
    public TreeDto modifyProbandId(Long treeId, Long probandId) {
        TreeDto tree = getById(treeId);
        if (!personService.doesPersonBelongToTree(probandId, treeId)) throw new PersonNotFoundInTreeException(
                "Requested person cannot be set as proband of '" + tree.getName() + "' tree");
        tree.setProbandId(probandId);
        return update(tree);
    }

    @Override
    @Transactional
    public TreeDto create(TreeDto newTree) {
        Long ownerId = newTree.getOwner().getId();
        securityService.checkIfUserIsLoggedIn(ownerId);

        UserDto user = userService.getById(ownerId);
        newTree.setOwner(user);
        newTree.setPersonsCount(0L);
        return super.create(newTree);
    }

    @Override
    @Transactional
    public TreeDto updateCounts(Long id) {
        TreeDto tree = getById(id);
        tree.setPersonsCount(personService.countByTree(tree));
        return super.update(tree);
    }
}
