package genealogicapi.domain.tree;

import genealogicapi.domain.abstraction.AbstractRepository;
import genealogicapi.domain.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TreeRepository extends AbstractRepository<Tree> {
    Page<Tree> findByOwner(UserEntity owner, Pageable pageable);

    List<Tree> deleteAllByOwner(UserEntity owner);
}
