package genealogicapi.domain.tree;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import genealogicapi.commons.Ownable;
import genealogicapi.domain.abstraction.AbstractEntity;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.user.UserEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = "persons") // todo #89
@ToString(exclude = "persons")
@SuperBuilder

@Entity
@Table
public class Tree extends AbstractEntity implements Serializable, Ownable {
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "owner", nullable = false)
    private UserEntity owner;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "proband_id")
    private Long probandId;

    @Column(name = "persons_count")
    private Long personsCount;

    @JsonManagedReference
    @OneToMany(mappedBy = "tree", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Column(name = "persons")
    private Set<PersonEntity> persons;

    @Override
    public String getOwnerUsername() {
        return owner.getUsername();
    }
}
