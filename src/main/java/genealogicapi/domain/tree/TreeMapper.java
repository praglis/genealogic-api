package genealogicapi.domain.tree;

import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.user.mapping.UserMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = UserMapper.class)
public interface TreeMapper extends AbstractMapper<TreeDto, Tree> {
    @Override
    @Mapping(target = "personsCount", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT)
    Tree toEntity(TreeDto dto);
}
