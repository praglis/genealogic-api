package genealogicapi.domain.tree;

import genealogicapi.domain.abstraction.AbstractService;
import genealogicapi.domain.event.dto.EventDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TreeService extends AbstractService<TreeDto> {
    @Override
    TreeDto create(TreeDto dto);


    Page<TreeDto> getTreesByOwner(Long ownerId, Pageable pageable);


    TreeDto updateCounts(Long id);


    List<TreeDto> deleteAllByOwner(Long ownerId);


    Page<EventDto> getTimelineEvents(Long id, Pageable pageable);


    TreeDto modifyProbandId(Long treeId, Long probandId);
}
