package genealogicapi.domain.tree;

import com.fasterxml.jackson.annotation.JsonProperty;
import genealogicapi.commons.Ownable;
import genealogicapi.commons.validation.ValidId;
import genealogicapi.domain.abstraction.AbstractDto;
import genealogicapi.domain.user.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class TreeDto extends AbstractDto implements Ownable {

    @NotBlank
    @Size(min = 1, max = 100)
    private String name;

    @ValidId
    private UserDto owner;

    private Long probandId;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long personsCount;

    @Override
    public String getOwnerUsername() {
        return owner.getUsername();
    }
}
