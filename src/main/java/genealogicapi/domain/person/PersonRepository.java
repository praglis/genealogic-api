package genealogicapi.domain.person;

import genealogicapi.domain.abstraction.AbstractRepository;
import genealogicapi.domain.tree.Tree;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PersonRepository extends AbstractRepository<PersonEntity> {
    Page<PersonEntity> findByTreeOrderById(Tree tree, Pageable pageable);

    Long countByTree(Tree tree);

    boolean existsByIdAndTreeId(Long id, Long treeId);
}
