package genealogicapi.domain.person;

public interface Person {
    String getFullName();
}
