package genealogicapi.domain.person;

import genealogicapi.commons.StringMapper;
import genealogicapi.domain.abstraction.AbstractController;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.family.Relationship;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Tag(name = "Person", description = "Operations related people - individuals that are part of a genealogical tree and" +
        " therefore subjects of genealogical research.")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequestMapping("/persons")
public class PersonController extends AbstractController<PersonDto> {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        super(personService);
        this.personService = personService;
    }

    @Override
    @ApiResponse(responseCode = "409", description = "Returned when deleting person having requested ID could break the tree.",
            content = @Content)
    @DeleteMapping("/{id}")
    public ResponseEntity<PersonDto> delete(@PathVariable Long id) {
        return super.delete(id);
    }

    @Operation(summary = "Returns a list of people belonging to a tree having requested ID.")
    @GetMapping("/tree/{id}")
    public Page<PersonDto> getPersonsByTree(@PathVariable Long id, Pageable pageable) {
        return personService.getPersonsByTree(id, pageable);
    }

    @Operation(summary = "Creates new person who is relative in certain way to existing person and creates new family for these two people.")
    @ApiResponse(responseCode = "409", description = "Returned when:\n" +
            "1. Spouse is added anf neither existing spouse or new spouse have specified sex.\n" +
            "2. Child is added and existing parent doesn't have specified sex.", content = @Content)
    @PostMapping("/{relativeId}/{relationship}")
    public ResponseEntity<PersonDto> createRelativeAndFamily(@Valid @RequestBody PersonDto dto,
                                                             @Parameter(description = "Existing relative ID")
                                                             @PathVariable Long relativeId,
                                                             @Parameter(description = "Describes who the new person is for the existing one.")
                                                             @PathVariable Relationship relationship) {
        PersonDto createdDto = personService.createRelativeAndFamily(dto, relativeId, relationship);
        return ResponseEntity.created(URI.create(
                        String.format("/%ss/%s",
                                StringMapper.dtoClassToResourceName(dto.getClass()),
                                createdDto.getId())))
                .body(createdDto);
    }

    @Operation(summary = "Creates new person who is relative in certain way to existing person in existing family.")
    @ApiResponse(responseCode = "409", description = "Returned when in requested family both parents/spouses already exist.",
            content = @Content)
    @PostMapping("/{relativeId}/family/{familyId}/{relationship}")
    public ResponseEntity<PersonDto> createRelativeInExistingFamily(@Valid @RequestBody PersonDto dto,
                                                                    @Parameter(description = "Existing relative ID")
                                                                    @PathVariable Long relativeId,
                                                                    @Parameter(description = "Describes who the new person is for the existing one.")
                                                                    @PathVariable Relationship relationship,
                                                                    @Parameter(description = "Specifies existing family containing existing person.")
                                                                    @PathVariable Long familyId) {
        PersonDto createdDto = personService.createRelativeInExistingFamily(dto, relativeId, relationship, familyId);
        return ResponseEntity.created(URI.create(
                        String.format("/%ss/%s",
                                StringMapper.dtoClassToResourceName(dto.getClass()),
                                createdDto.getId())))
                .body(createdDto);
    }

    @Operation(summary = "Returns person timeline - a list of chronologically sorted events in which a certain person is participant.")
    @GetMapping("/{id}/timeline")
    public Page<EventDto> getTimelineEvents(@PathVariable Long id, Pageable pageable) {
        return personService.getTimelineEvents(id, pageable);
    }
}
