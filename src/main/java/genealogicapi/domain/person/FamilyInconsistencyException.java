package genealogicapi.domain.person;

public class FamilyInconsistencyException extends RuntimeException {
    public FamilyInconsistencyException(String message) {
        super(message);
    }
}
