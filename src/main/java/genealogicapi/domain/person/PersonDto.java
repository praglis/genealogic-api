package genealogicapi.domain.person;

import com.fasterxml.jackson.annotation.JsonProperty;
import genealogicapi.commons.Ownable;
import genealogicapi.commons.file.EncodedFileDto;
import genealogicapi.domain.abstraction.AbstractDto;
import genealogicapi.domain.family.FamilyEntity;
import genealogicapi.domain.place.PlaceDto;
import genealogicapi.domain.tree.TreeDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "parentFamilies")
@SuperBuilder
public class PersonDto extends AbstractDto implements Person, Ownable {

    @NotNull
    private TreeDto tree;//todo po co całe drzewo? #50

    @Size(min = 1, max = 30)
    private String namePrefix;

    @Size(min = 1, max = 60)
    private String firstName;

    @Size(min = 1, max = 60)
    private String middleName;

    @Size(min = 1, max = 30)
    private String nickname;

    @Size(min = 1, max = 30)
    private String surnamePrefix;

    @NotBlank
    @Size(min = 1, max = 60)
    private String lastName;

    @Size(min = 1, max = 60)
    private String maidenName;

    @Size(min = 1, max = 30)
    private String nameSuffix;

    @NotNull
    private Sex sex;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Set<FamilyEntity> parentFamilies;

    private String occupation;

    private PlaceDto residency;

    private String bio;

    private EncodedFileDto photo;

    @Override
    public String getFullName() {
        return StringUtils.chop(String.format("%s%s%s%s%s%s%s%s",
                namePrefix != null ? namePrefix + " " : "",
                firstName != null ? firstName + " " : "",
                middleName != null ? middleName + " " : "",
                nickname != null ? "'" + nickname + "' " : "",
                surnamePrefix != null ? surnamePrefix + " " : "",
                lastName != null ? lastName + " " : "",
                maidenName != null ? maidenName + " " : "",
                nameSuffix != null ? nameSuffix + " " : ""));
    }

    public String getShortName() {
        return StringUtils.chop(String.format("%s%s",
                firstName != null ? firstName + " " : "",
                lastName != null ? lastName + " " : ""
        ));
    }

    public String getOwnerUsername() {
        return tree.getOwnerUsername();
    }
}
