package genealogicapi.domain.person;

import genealogicapi.domain.abstraction.AbstractServiceImpl;
import genealogicapi.domain.event.EventService;
import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.event.participant.EventParticipantService;
import genealogicapi.domain.family.FamilyDto;
import genealogicapi.domain.family.FamilyService;
import genealogicapi.domain.family.Relationship;
import genealogicapi.domain.person.mapping.PersonMapper;
import genealogicapi.domain.tree.Tree;
import genealogicapi.domain.tree.TreeDto;
import genealogicapi.domain.tree.TreeMapper;
import genealogicapi.domain.tree.TreeService;
import genealogicapi.gedcom.structure.IndividualRecord;
import genealogicapi.security.SecurityService;
import lombok.NonNull;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonServiceImpl extends AbstractServiceImpl<PersonDto, PersonEntity> implements PersonService {

    private final PersonRepository personRepository;
    private final TreeService treeService;
    private final TreeMapper treeMapper;
    private final PersonMapper personMapper;
    private final SecurityService securityService;
    private final FamilyService familyService;
    private final EventParticipantService eventParticipantService;
    private final EventService eventService;

    public PersonServiceImpl(PersonRepository personRepository, //todo #66 (this service needs som refactor)
                             @Lazy TreeService treeService,
                             TreeMapper treeMapper,
                             PersonMapper personMapper,
                             SecurityService securityService,
                             @Lazy FamilyService familyService,
                             @Lazy EventParticipantService eventParticipantService,
                             @Lazy EventService eventService) {
        super(personRepository, personMapper);
        this.personRepository = personRepository;
        this.personMapper = personMapper;
        this.treeService = treeService;
        this.securityService = securityService;
        this.familyService = familyService;
        this.eventParticipantService = eventParticipantService;
        this.treeMapper = treeMapper;
        this.eventService = eventService;
    }

    @Override
    @Transactional
    public IndividualRecord create(IndividualRecord individualRecord) {
        return personMapper.toRecord(create(personMapper.toDto(individualRecord)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<IndividualRecord> getAllRecordsByTree(Long treeId) {
        return getPersonsByTree(treeId, Pageable.unpaged()).map(personMapper::toRecord).getContent();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PersonDto> getPersonsByTree(Long treeId, Pageable pageable) {
        Tree tree = treeMapper.toEntity(treeService.getById(treeId));
        return personRepository.findByTreeOrderById(tree, pageable).map(personMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Long countByTree(TreeDto tree) {
        return personRepository.countByTree(treeMapper.toEntity(tree));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventDto> getTimelineEvents(Long id, Pageable pageable) {
        return eventService.getEventsByPersonOrderByDate(getById(id), pageable);
    }

    @Override
    @Transactional
    public PersonDto create(PersonDto personToCreate) {
        TreeDto treeDto = treeService.getById(personToCreate.getTree().getId());
        personToCreate.setTree(treeDto);

        PersonDto createdPerson = super.create(personToCreate);
        treeService.updateCounts(treeDto.getId());
        return createdPerson;
    }

    @Override
    @Transactional
    public PersonDto update(PersonDto dto) {
        treeService.getById(dto.getTree().getId());
        PersonDto personDto = super.update(dto);
        treeService.updateCounts(dto.getTree().getId());
        return personDto;
    }

    @Override
    @Transactional
    public PersonDto createRelativeAndFamily(PersonDto newPersonToCreate, Long existingRelativeId, Relationship relationship) {
        PersonDto existingRelative = getById(existingRelativeId);
        PersonDto createdNewPerson = create(newPersonToCreate);

        FamilyDto family = new FamilyDto();
        switch (relationship) {
            case FATHER:
                family.setChildren(List.of(existingRelative));
                family.setHusband(createdNewPerson);
                break;
            case MOTHER:
                family.setChildren(List.of(existingRelative));
                family.setWife(createdNewPerson);
                break;
            case CHILD:
                addChildToFamily(existingRelative, createdNewPerson, family);
                break;
            case SPOUSE:
                addSpouseToFamily(existingRelative, createdNewPerson, family);
                break;
        }
        familyService.create(family);

        return createdNewPerson;
    }

    @Override
    @Transactional(readOnly = true)
    public PersonDto getById(Long id) {
        PersonDto personDto = super.getById(id);
        securityService.checkIfUserIsLoggedIn(personDto.getOwnerUsername());
        return personDto;
    }

    @Override
    public boolean doesPersonBelongToTree(Long personId, Long treeId) {
        return personRepository.existsByIdAndTreeId(personId, treeId);
    }

    @Override
    @Transactional
    public PersonDto delete(Long id) {
        PersonDto person = getById(id);

        eventParticipantService.deleteAllByPerson(person);

        Page<FamilyDto> familiesContainingPerson = familyService.getFamiliesContainingPerson(
                id, PageRequest.of(0, 3));
        if (familiesContainingPerson.getTotalElements() > 1)
            throw new FamilyInconsistencyException("Deleting " + person.getFullName() +
                    " would break tree. Delete their parents or/and spouse or/and children first");

        if (!familiesContainingPerson.isEmpty()) {
            FamilyDto family = familiesContainingPerson.getContent().get(0);
            if (family.getSize() <= 2) familyService.delete(family.getId());
            else {
                if (family.hasHusband() && family.getHusband().equals(person)) family.setHusband(null);
                else if (family.hasWife() && family.getWife().equals(person)) family.setWife(null);
                else if (!family.hasAtLeastOneChild()) family.getChildren().remove(person);
                familyService.update(family);
            }
        }
        PersonDto delete = super.delete(id);
        treeService.updateCounts(person.getTree().getId());
        return delete;
    }

    private void addChildToFamily(PersonDto existingRelative, PersonDto createdNewPerson, FamilyDto family) {
        family.setChildren(List.of(createdNewPerson));
        if (existingRelative.getSex() == Sex.M) family.setHusband(existingRelative);
        else if (existingRelative.getSex() == Sex.F) family.setWife(existingRelative);
        else {
            delete(createdNewPerson.getId());
            throw new FamilyInconsistencyException(
                    "Parent (" + existingRelative.getFullName() + ") can not have undetermined sex");
        }
    }

    private void addSpouseToFamily(PersonDto existingRelative, PersonDto createdNewPerson, FamilyDto family) {
        if (existingRelative.getSex() == Sex.M || createdNewPerson.getSex() == Sex.F) {
            family.setHusband(existingRelative);
            family.setWife(createdNewPerson);
        } else if (existingRelative.getSex() == Sex.F || createdNewPerson.getSex() == Sex.M) {
            family.setWife(existingRelative);
            family.setHusband(createdNewPerson);
        } else {
            delete(createdNewPerson.getId());
            throw new FamilyInconsistencyException(String.format(
                    "At least one of spouses (%s or %s) must have determined sex.",
                    existingRelative.getFullName(), createdNewPerson.getFullName())
            );
        }
    }

    @Override
    @Transactional
    public PersonDto createRelativeInExistingFamily(PersonDto newPersonToCreate, @NonNull Long existingRelativeId,
                                                    Relationship relationship,
                                                    Long familyId) {
        PersonDto existingRelative = getById(existingRelativeId);
        PersonDto createdNewPerson = create(newPersonToCreate);
        FamilyDto family = familyService.getById(familyId);

        switch (relationship) {
            case FATHER:
                if (!family.hasHusband()) family.setHusband(createdNewPerson);
                else
                    clearAndThrowFamilyInconsistencyException(existingRelative, Relationship.FATHER, family.getHusband());
                break;
            case MOTHER:
                if (!family.hasWife()) family.setWife(createdNewPerson);
                else
                    clearAndThrowFamilyInconsistencyException(existingRelative, Relationship.MOTHER, family.getWife());
                break;
            case CHILD:
                family.addChild(createdNewPerson);
                break;
            case SPOUSE:
                if (!family.hasHusband()) family.setHusband(createdNewPerson);
                else if (!family.hasWife()) family.setWife(createdNewPerson);
                else {
                    PersonDto currentSpouse;
                    if (family.getHusband().getId().equals(existingRelativeId)) currentSpouse = family.getWife();
                    else currentSpouse = family.getHusband();
                    clearAndThrowFamilyInconsistencyException(existingRelative, Relationship.SPOUSE, currentSpouse);
                }
                break;
        }
        familyService.update(family);
        return createdNewPerson;
    }

    private void clearAndThrowFamilyInconsistencyException(PersonDto person, Relationship relationshipName,
                                                           PersonDto existingRelative) {
        delete(existingRelative.getId());
        throw new FamilyInconsistencyException(String.format("%s already has a %s (%s).",
                person.getFullName(), relationshipName.name().toLowerCase(), existingRelative.getFullName()));
    }

}
