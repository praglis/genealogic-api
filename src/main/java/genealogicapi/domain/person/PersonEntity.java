package genealogicapi.domain.person;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import genealogicapi.commons.Ownable;
import genealogicapi.domain.abstraction.AbstractEntity;
import genealogicapi.domain.event.participant.EventParticipant;
import genealogicapi.domain.family.FamilyEntity;
import genealogicapi.domain.place.Place;
import genealogicapi.domain.tree.Tree;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true,
        exclude = {"tree", "participants", "parentFamilies", "husbandFamilies", "wifeFamilies"}) // todo #89
@ToString(callSuper = true,
        exclude = {"tree", "participants", "parentFamilies", "husbandFamilies", "wifeFamilies"})
@NoArgsConstructor
@SuperBuilder

@Entity
@Table(name = "person")
public class PersonEntity extends AbstractEntity implements Serializable, Ownable {
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tree")
    private Tree tree;

    @Column(name = "name_prefix", length = 30)
    private String namePrefix;

    @Column(name = "name_first", length = 60)
    private String firstName;

    @Column(name = "name_middle", length = 60)
    private String middleName;

    @Column(name = "nickname", length = 30)
    private String nickname;

    @Column(name = "surname_prefix", length = 30)
    private String surnamePrefix;

    @Column(name = "name_last", nullable = false, length = 60)
    private String lastName;

    @Column(name = "name_maiden", length = 60)
    private String maidenName;

    @Column(name = "name_suffix", length = 30)
    private String nameSuffix;

    @Column(name = "sex", nullable = false)
    private Sex sex;

    @JsonManagedReference
    @ManyToMany(mappedBy = "children")
    private Set<FamilyEntity> parentFamilies;

    @JsonManagedReference
    @OneToMany(mappedBy = "husband")
    private Set<FamilyEntity> wifeFamilies;

    @JsonManagedReference
    @OneToMany(mappedBy = "wife")
    private Set<FamilyEntity> husbandFamilies;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "residency")
    private Place residency;

    @Column(name = "occupation", length = 90)
    private String occupation;

    @Column(name = "bio", length = 10000)
    private String bio;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    private Set<EventParticipant> participants;

    @Lob
    @Column(name = "photo")
    @Type(type = "org.hibernate.type.BinaryType")
    private byte[] photo;

    @Override
    public String getOwnerUsername() {
        return tree.getOwnerUsername();
    }
}
