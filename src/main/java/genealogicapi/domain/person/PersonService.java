package genealogicapi.domain.person;

import genealogicapi.domain.event.dto.EventDto;
import genealogicapi.domain.family.Relationship;
import genealogicapi.domain.tree.TreeDto;
import genealogicapi.gedcom.GedcomConvertible;
import genealogicapi.gedcom.structure.IndividualRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface PersonService extends GedcomConvertible<IndividualRecord, PersonDto> {
    @PreAuthorize("hasRole('USER')")
    Page<PersonDto> getPersonsByTree(Long treeId, Pageable pageable);

    @PreAuthorize("hasRole('USER')")
    Long countByTree(TreeDto tree);

    @PreAuthorize("hasRole('USER')")
    Page<EventDto> getTimelineEvents(Long id, Pageable pageable);

    @Override
    @PreAuthorize("hasRole('USER')")
    PersonDto create(PersonDto dto);

    @PreAuthorize("hasRole('USER')")
    PersonDto createRelativeAndFamily(PersonDto dto, Long relativeId, Relationship relationship);

    @PreAuthorize("hasRole('USER')")
    PersonDto createRelativeInExistingFamily(PersonDto dto, Long relativeId, Relationship relationship, Long familyId);

    @PreAuthorize("hasRole('USER')")
    boolean doesPersonBelongToTree(Long probandId, Long treeId);

}
