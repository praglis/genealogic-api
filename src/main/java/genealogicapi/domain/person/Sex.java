package genealogicapi.domain.person;

public enum Sex {
    M("MALE"),
    F("FEMALE"),
    U("UNDETERMINED");

    private final String value;

    Sex(String value) {
        this.value = value;
    }
}
