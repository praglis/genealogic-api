package genealogicapi.domain.person.mapping;

import genealogicapi.commons.file.EncodedFileDto;
import genealogicapi.domain.person.PersonDto;
import genealogicapi.domain.person.PersonEntity;
import genealogicapi.domain.place.PlaceMapper;
import genealogicapi.domain.tree.TreeMapper;
import genealogicapi.gedcom.GedcomRecordMapper;
import genealogicapi.gedcom.structure.IndividualRecord;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;

@Mapper(componentModel = "spring",
        uses = {TreeMapper.class, PlaceMapper.class},
        imports = {ArrayList.class, HashMap.class, EncodedFileDto.class, Base64.class})
public interface PersonMapper extends GedcomRecordMapper<PersonDto, PersonEntity, IndividualRecord> {
    @Override
    @Mapping(target = "individualEvents", expression = "java(new ArrayList<>())")
    @Mapping(target = "familyPointers", expression = "java(new HashMap<>())")
    IndividualRecord toRecord(PersonDto dto);

    @Override
    @Mapping(target = "photo", qualifiedBy = ImageMapping.class,
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    PersonEntity toEntity(PersonDto dto);

    @Override
    @Mapping(target = "photo",
            expression = "java(entity.getPhoto() != null ? " +
                    "new EncodedFileDto(entity.getId() + \"-photo\", Base64.getEncoder().encodeToString(entity.getPhoto()))" +
                    " : null)")
    PersonDto toDto(PersonEntity entity);

    @Override
    @Mapping(target = "photo", ignore = true)
    PersonDto toDto(IndividualRecord individualRecord);

    @ImageMapping
    default byte[] map(EncodedFileDto value) {
        if (value != null && value.getEncodedBytes() != null && value.getEncodedBytes().length() > 0)
            return Base64.getDecoder().decode(value.getEncodedBytes());
        else return new byte[0];
    }
}
