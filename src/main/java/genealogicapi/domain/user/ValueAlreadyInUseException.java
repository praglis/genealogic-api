package genealogicapi.domain.user;

public class ValueAlreadyInUseException extends RuntimeException {
    public ValueAlreadyInUseException(String message) {
        super(message);
    }

    public static ValueAlreadyInUseException of(String property) {
        return new ValueAlreadyInUseException("Given " + property + " is already in use.");
    }
}
