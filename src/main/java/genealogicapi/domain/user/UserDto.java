package genealogicapi.domain.user;

import genealogicapi.domain.abstraction.AbstractDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
public class UserDto extends AbstractDto {
    @NotBlank
    @Size(min = 3, max = 48)
    private String username;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Size(min = 8, max = 80)
    private String password;

    @Size(max = 48)
    private String firstName;

    @Size(max = 48)
    private String lastName;

    @NotNull
    private AppUserRole role;
}
