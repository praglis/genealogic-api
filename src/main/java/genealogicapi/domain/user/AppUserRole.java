package genealogicapi.domain.user;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum AppUserRole {
    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN", "ROLE_USER");

    private final Set<String> values;

    AppUserRole(String... values) {
        this.values = new HashSet<>(Arrays.asList(values));
    }

    public static boolean contains(String name) {

        for (AppUserRole re : AppUserRole.values()) {
            if (re.name().equals(name)) {
                return true;
            }
        }

        return false;
    }

    public Set<String> getValues() {
        return values;
    }
}
