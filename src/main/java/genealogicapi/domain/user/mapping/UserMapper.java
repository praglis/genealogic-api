package genealogicapi.domain.user.mapping;

import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.user.UserDto;
import genealogicapi.domain.user.UserEntity;
import genealogicapi.security.payload.LoginRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public abstract class UserMapper implements AbstractMapper<UserDto, UserEntity> {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @EncodedMapping
    public String encode(String value) {
        if (value != null) return passwordEncoder.encode(value);
        else return value;
    }

    @Override
    @Mapping(target = "password", qualifiedBy = EncodedMapping.class,
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract UserEntity toEntity(UserDto dto);

    public abstract LoginRequest toLoginRequest(UserDto dto);
}
