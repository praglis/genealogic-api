package genealogicapi.domain.user;

import genealogicapi.domain.abstraction.AbstractService;
import genealogicapi.security.payload.JwtResponse;
import genealogicapi.security.payload.LoginRequest;

public interface UserService extends AbstractService<UserDto> {
    JwtResponse authenticateUser(LoginRequest loginRequest);

    UserDto getByUsername(String username);
}
