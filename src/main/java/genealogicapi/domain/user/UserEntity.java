package genealogicapi.domain.user;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import genealogicapi.domain.abstraction.AbstractEntity;
import genealogicapi.domain.event.Event;
import genealogicapi.domain.tree.Tree;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"trees", "events"}) // todo #89
@ToString(exclude = {"trees", "events"})
@SuperBuilder

@Entity
@Table(name = "user_gl")
public class UserEntity extends AbstractEntity implements Serializable {
    @Column(name = "username", unique = true, nullable = false, length = 48)
    private String username;

    @Column(name = "email", unique = true, nullable = false, length = 100)
    private String email;

    @Column(name = "password", nullable = false, length = 80)
    private String password;

    @Column(name = "name_first", length = 48)
    private String firstName;

    @Column(name = "name_last", length = 48)
    private String lastName;

    @Column(name = "role")
    private AppUserRole role;

    @JsonManagedReference
    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private Set<Tree> trees;

    @JsonManagedReference
    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private Set<Event> events;
}
