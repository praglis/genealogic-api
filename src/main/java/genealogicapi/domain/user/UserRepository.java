package genealogicapi.domain.user;

import genealogicapi.domain.abstraction.AbstractRepository;

import java.util.Optional;

public interface UserRepository extends AbstractRepository<UserEntity> {
    Optional<UserEntity> findByUsername(String username);

    Optional<UserEntity> findByEmail(String email);

    boolean existsByEmailLike(String email);

    boolean existsByEmailLikeAndIdNot(String email, Long id);

    boolean existsByUsernameLike(String username);

    boolean existsByUsernameLikeAndIdNot(String username, Long id);
}
