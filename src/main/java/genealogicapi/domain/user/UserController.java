package genealogicapi.domain.user;

import genealogicapi.domain.abstraction.AbstractController;
import genealogicapi.security.payload.JwtResponse;
import genealogicapi.security.payload.LoginRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "User", description = "Operations related to app users. Includes JWT authorization.")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
@RequestMapping("/users")
public class UserController extends AbstractController<UserDto> {

    private final UserService userService;

    public UserController(UserService service) {
        super(service);
        userService = service;
    }

    @ApiResponse(responseCode = "409", description = "Returned when username or email is already in use.",
            content = @Content)
    @Override
    public ResponseEntity<UserDto> create(@Valid @RequestBody UserDto dto) {
        return super.create(dto);
    }

    @ApiResponse(responseCode = "409", description = "Returned when username or email is already in use.",
            content = @Content)
    @Override
    @PutMapping("/{id}")

    public ResponseEntity<UserDto> update(@Valid @RequestBody UserDto dto, @PathVariable Long id) {
        return super.update(dto, id);
    }

    @Operation(summary = "Allows authentication.", description = "Successful authentication will return bearer token " +
            "which must be included in 'Authorization' header for requests that need authentication.")
    @PostMapping("/auth")
    public ResponseEntity<JwtResponse> authenticateUser(@RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(userService.authenticateUser(loginRequest));
    }
}
