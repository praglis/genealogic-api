package genealogicapi.domain.user;

import genealogicapi.commons.exception.EntityNotFoundException;
import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.abstraction.AbstractRepository;
import genealogicapi.domain.abstraction.AbstractServiceImpl;
import genealogicapi.domain.tree.TreeService;
import genealogicapi.domain.user.mapping.UserMapper;
import genealogicapi.security.UserDetailsImpl;
import genealogicapi.security.jwt.JwtUtils;
import genealogicapi.security.payload.JwtResponse;
import genealogicapi.security.payload.LoginRequest;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl extends AbstractServiceImpl<UserDto, UserEntity> implements UserService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final UserMapper userMapper;
    private final TreeService treeService;

    public UserServiceImpl(AbstractRepository<UserEntity> repository,
                           AbstractMapper<UserDto, UserEntity> mapper,
                           UserRepository userRepository,
                           AuthenticationManager authenticationManager,
                           JwtUtils jwtUtils,
                           UserMapper userMapper, @Lazy TreeService treeService) {
        super(repository, mapper);
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.userMapper = userMapper;
        this.treeService = treeService;
    }

    @Override
    @Transactional
    public UserDto create(UserDto userDto) {
        if (userRepository.existsByUsernameLike(userDto.getUsername())) {
            throw ValueAlreadyInUseException.of("username");
        }
        if (userRepository.existsByEmailLike(userDto.getEmail())) {
            throw ValueAlreadyInUseException.of("mail address");
        }

        return super.create(userDto);
    }

    @Override
    @Transactional
    public UserDto update(UserDto dto) {
        if (userRepository.existsByEmailLikeAndIdNot(dto.getEmail(), dto.getId())) {
            throw ValueAlreadyInUseException.of("mail address");
        }
        if (userRepository.existsByUsernameLikeAndIdNot(dto.getUsername(), dto.getId())) {
            throw ValueAlreadyInUseException.of("username");
        }

        return super.update(dto);
    }

    @Override
    public JwtResponse authenticateUser(LoginRequest loginRequest) {
        UserEntity user = userRepository.findByEmail(loginRequest.getEmail()).orElseThrow(
                () -> new EntityNotFoundException("User with mail " + loginRequest.getEmail() + " not found."));

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getEmail(),
                userDetails.getUsername(),
                userDetails.getFirstName(),
                userDetails.getLastName(),
                roles);
    }

    @Override
    @Transactional
    public UserDto getByUsername(String username) {
        UserEntity user = userRepository.findByUsername(username).orElseThrow(
                () -> new EntityNotFoundException("User with username (" + username + ") does not exist.")
        );
        return userMapper.toDto(user);
    }

    @Override
    @Transactional
    public UserDto delete(Long id) {
        treeService.deleteAllByOwner(id);
        return super.delete(id);
    }
}
