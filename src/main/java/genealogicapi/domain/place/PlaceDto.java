package genealogicapi.domain.place;

import genealogicapi.domain.abstraction.AbstractDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = true)
@Data
@Valid
public class PlaceDto extends AbstractDto {
    @NotBlank
    @Size(max = 120)
    private String name;

    @Size(max = 180)
    private String address;

    @Size(max = 60)
    private String city;

    @Size(max = 60)
    private String state;

    @Size(max = 10)
    private String postalCode;

    @Size(max = 60)
    private String country;

    public boolean hasAnyAddress() {
        return address != null || city != null || state != null || postalCode != null || country != null;
    }
}
