package genealogicapi.domain.place;

import genealogicapi.domain.abstraction.AbstractRepository;

public interface PlaceRepository extends AbstractRepository<Place> {
}
