package genealogicapi.domain.place;

import genealogicapi.domain.abstraction.AbstractMapper;
import genealogicapi.domain.abstraction.AbstractRepository;
import genealogicapi.domain.abstraction.AbstractServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class PlaceServiceImpl extends AbstractServiceImpl<PlaceDto, Place> implements PlaceService {
    public PlaceServiceImpl(AbstractRepository<Place> repository, AbstractMapper<PlaceDto, Place> mapper) {
        super(repository, mapper);
    }
}
