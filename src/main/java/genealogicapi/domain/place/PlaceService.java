package genealogicapi.domain.place;

import genealogicapi.domain.abstraction.AbstractService;
import org.springframework.security.access.prepost.PreAuthorize;

public interface PlaceService extends AbstractService<PlaceDto> {
    @Override
    @PreAuthorize("hasRole('USER')")
    PlaceDto create(PlaceDto dto);
}
