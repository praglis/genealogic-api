package genealogicapi.domain.place;

import genealogicapi.domain.abstraction.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true) // todo #89
@Entity
@Table
public class Place extends AbstractEntity implements Serializable {
    @Column(name = "name", length = 120, nullable = false)
    private String name;

    @Column(name = "address", length = 180)
    private String address;

    @Column(name = "city", length = 60)
    private String city;

    @Column(name = "state", length = 60)
    private String state;

    @Column(name = "postal", length = 10)
    private String postalCode;

    @Column(name = "country", length = 60)
    private String country;

}
