package genealogicapi.domain.place;

import genealogicapi.gedcom.GedcomRecordMapper;
import genealogicapi.gedcom.structure.EventRecord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PlaceMapper extends GedcomRecordMapper<PlaceDto, Place, EventRecord> {
}
