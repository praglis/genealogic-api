package genealogicapi.domain.date;

import genealogicapi.domain.abstraction.AbstractMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DateMapper extends AbstractMapper<DateDto, DateEntity> {
}
