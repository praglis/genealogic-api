package genealogicapi.domain.date;

import genealogicapi.gedcom.structure.EventRecord;

public enum DateEstimation {
    EXACT(""),
    APPROXIMATED("ABT "),
    BEFORE("BEF "),
    AFTER("AFT "),
    BETWEEN("BET "),
    UNKNOWN("");

    private final String value;

    DateEstimation(String value) {
        this.value = value;
    }

    public static DateEstimation of(EventRecord.RecordDateEstimation rde) {
        switch (rde) {
            case ABT:
            case CAL:
            case EST:
                return APPROXIMATED;
            case BEF:
                return BEFORE;
            case AFT:
                return AFTER;
            case BET:
                return BETWEEN;
            case EXACT:
            default:
                return EXACT;
        }
    }

    public String getValue() {
        return value;
    }
}
