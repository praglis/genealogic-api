package genealogicapi.domain.date;

public enum DateAccuracy {
    YEAR,
    MONTH,
    DAY
}
