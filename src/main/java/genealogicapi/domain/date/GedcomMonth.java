package genealogicapi.domain.date;

import java.time.Month;

public enum GedcomMonth {
    JAN, //January
    FEB, //February
    MAR, //March
    APR, //April
    MAY, //May
    JUN, //June
    JUL, //July
    AUG, //August
    SEP, //September
    OCT, //October
    NOV, //November
    DEC; //December

    public static Month toJavaMonth(String monthEnumString) {
        return Month.of(GedcomMonth.valueOf(monthEnumString.toUpperCase()).ordinal() + 1);
    }

    public static GedcomMonth ofJavaMonth(Month month) {
        return GedcomMonth.values()[month.getValue() - 1];
    }

    public static boolean contains(String name) {
        for (GedcomMonth gt : GedcomMonth.values()) {
            if (gt.name().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }
}
