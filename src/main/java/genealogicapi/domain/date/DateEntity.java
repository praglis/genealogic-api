package genealogicapi.domain.date;

import genealogicapi.domain.abstraction.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;


@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true) // todo #89
@Table
public class DateEntity extends AbstractEntity implements Serializable {
    @Column(name = "value")
    private LocalDate value;

    @Column(name = "value_accuracy")
    private DateAccuracy valueAccuracy;

    @Column(name = "value_max")
    private LocalDate maxValue;

    @Column(name = "value_accuracy_max")
    private DateAccuracy maxValueAccuracy;

    @Column(name = "estimation", nullable = false)
    private DateEstimation estimation;

    @Column(name = "phrase", length = 35)
    private String phrase;
}
