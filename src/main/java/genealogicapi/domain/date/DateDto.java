package genealogicapi.domain.date;

import genealogicapi.domain.abstraction.AbstractDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
public class DateDto extends AbstractDto {
    @NotNull
    private LocalDate value;
    @NotNull
    private DateAccuracy valueAccuracy;

    private LocalDate maxValue;
    private DateAccuracy maxValueAccuracy;

    @NotNull
    private DateEstimation estimation;

    @Size(max = 35)
    private String phrase;

    public boolean isEmpty() {
        return value == null && maxValue == null && phrase == null;
    }
}
