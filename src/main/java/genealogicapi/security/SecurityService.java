package genealogicapi.security;

import genealogicapi.domain.user.UserDto;

public interface SecurityService {
    String findLoggedInUsername();

    UserDto findLoggedInUser();

    UserDto checkIfUserIsLoggedIn(Long userId);

    void checkIfUserIsLoggedIn(String username);
}
