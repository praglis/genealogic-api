package genealogicapi.security;

import genealogicapi.domain.user.UserDto;
import genealogicapi.domain.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SecurityServiceImpl implements SecurityService {
    private final UserService userService;

    public SecurityServiceImpl(@Lazy UserService userService) {
        this.userService = userService;
    }

    @Override
    public String findLoggedInUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            Object userDetails = auth.getPrincipal();
            if (userDetails instanceof UserDetails) {
                return ((UserDetails) userDetails).getUsername();
            }
        }

        log.debug("There's no logged in user");
        return null;
    }

    @Override
    public UserDto findLoggedInUser() {
        return userService.getByUsername(findLoggedInUsername());
    }

    @Override
    public void checkIfUserIsLoggedIn(String username) {
        if (!username.equals(findLoggedInUsername())) {
            throw new NoOwnershipException(
                    "Wrong user request, you are currently logged as (" + findLoggedInUsername() + ")."
            );
        }
    }

    @Override
    public UserDto checkIfUserIsLoggedIn(Long userId) {
        UserDto userDto = userService.getById(userId);
        checkIfUserIsLoggedIn(userDto.getUsername());
        return userDto;
    }
}
