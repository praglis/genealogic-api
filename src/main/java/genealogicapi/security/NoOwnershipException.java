package genealogicapi.security;

public class NoOwnershipException extends RuntimeException {
    public NoOwnershipException(String message) {
        super(message);
    }
}
