package genealogicapi.commons.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target(ElementType.FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = {NonNullIdValidator.class})
public @interface ValidId {

    String message() default "id can't be empty";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
