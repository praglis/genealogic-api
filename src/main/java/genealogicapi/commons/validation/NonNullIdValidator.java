package genealogicapi.commons.validation;

import genealogicapi.domain.abstraction.AbstractDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NonNullIdValidator implements ConstraintValidator<ValidId, AbstractDto> {
    @Override
    public boolean isValid(AbstractDto dto, ConstraintValidatorContext constraintValidatorContext) {
        return dto != null && dto.getId() != null;
    }
}
