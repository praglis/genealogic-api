package genealogicapi.commons.file;

import lombok.EqualsAndHashCode;
import lombok.Getter;


@Getter
@EqualsAndHashCode(callSuper = true)
public class NotEncodedFileDto extends FileDto {
    private final byte[] file;

    public NotEncodedFileDto(String fileName, byte[] file) {
        super(fileName);
        this.file = file;
    }
}
