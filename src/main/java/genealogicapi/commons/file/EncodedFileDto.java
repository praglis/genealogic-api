package genealogicapi.commons.file;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class EncodedFileDto extends FileDto {
    private final String encodedBytes;

    public EncodedFileDto(String fileName, String encodedBytes) {
        super(fileName);
        this.encodedBytes = encodedBytes;
    }
}
