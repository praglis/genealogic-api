package genealogicapi.commons;

public interface StringMapper {
    static String dtoClassToResourceName(Class<?> dtoClass) {
        return dtoClass.getSimpleName().toLowerCase().substring(0, dtoClass.getSimpleName().length() - 3);
    }
}
