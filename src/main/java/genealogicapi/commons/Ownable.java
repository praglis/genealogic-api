package genealogicapi.commons;

public interface Ownable {
    String getOwnerUsername();
}
