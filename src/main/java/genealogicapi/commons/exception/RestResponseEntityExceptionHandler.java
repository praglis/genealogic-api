package genealogicapi.commons.exception;

import genealogicapi.domain.person.FamilyInconsistencyException;
import genealogicapi.domain.user.ValueAlreadyInUseException;
import genealogicapi.gedcom.exporting.exception.GedcomExportException;
import genealogicapi.gedcom.importing.exception.GedcomImportException;
import genealogicapi.security.NoOwnershipException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected String handleConflict(EntityNotFoundException ex) {
        return ex.getMessage();
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleExceptionInternal(ex, ex.getBindingResult()
                .getFieldErrors().stream()
                .map(fieldError -> String.format("%s: %s%n", fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.joining("\n")), headers, status, request);
    }

    @ExceptionHandler(value = {IdInconsistencyException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected String handleBadRequest(IdInconsistencyException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(value = {ValueAlreadyInUseException.class, FamilyInconsistencyException.class, GedcomExportException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.CONFLICT)
    protected String handleValueAlreadyInUseException(RuntimeException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(value = {AuthenticationException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected String handleUnauthorized(AuthenticationException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(value = {AccessDeniedException.class, NoOwnershipException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.FORBIDDEN)
    protected String handleForbidden(RuntimeException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(value = {GedcomImportException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected String handleBadRequest(GedcomImportException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected String handleAnyOtherException(Exception ex) {
        logger.error("{}", ex);
        return ex.getMessage();
    }
}

