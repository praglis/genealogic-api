package genealogicapi.commons.exception;

public class IdInconsistencyException extends RuntimeException {
    public IdInconsistencyException(Long bodyId, Long urlId) {
        super(String.format("Id passed in body (%d) and through URL (%d) must be the same.", bodyId, urlId));
    }
}
